# Meson

```sh
sudo pacman -S meson ninja aarch64-linux-gnu-gcc
```

```sh
rm -rf build
```

```sh
meson setup build/aarch64-generic --buildtype=release --cross-file ./compile/aarch64-generic.ini
meson setup build/x86_64-generic  --buildtype=release --cross-file ./compile/x86_64-generic.ini
meson setup build/x86_64-znver1   --buildtype=release --cross-file ./compile/x86_64-znver1.ini
```

```sh
meson compile -C build/aarch64-generic
meson compile -C build/x86_64-generic
meson compile -C build/x86_64-znver1
```

```sh
time ./build/aarch64-generic/hello
time ./build/x86_64-generic/hello
time ./build/x86_64-znver1/hello
```

# Slides

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Slides](#slides)
    - [Dependencies](#dependencies)
        - [Linux](#linux)
            - [Arch/Manjaro](#archmanjaro)
            - [Alpine](#alpine)
            - [Termux](#termux)
            - [Debian](#debian)
        - [MacOS](#macos)
        - [Windows](#windows)
    - [Setup](#setup)

<!-- markdown-toc end -->

## Dependencies

- curl
- emacs
- make
- sha256sum (or shasum)
- tar
- uname

### Linux
#### Arch/Manjaro
```sh
pacman -Ss curl emacs make
```

#### Alpine
```sh
apk add curl emacs make
```

#### Termux
```sh
pkg install curl emacs make
```

#### Debian
```sh
apt install curl emacs make
```

### MacOS
```sh
brew install curl emacs make
```

### Windows
https://manjaro.org/get-manjaro/

## Setup
```sh
make setup
```

.DEFAULT_GLOBAL := help

OS := $(shell uname)

PACKAGE_NAME    := reveal.tar.gz
PACKAGE_VERSION := 3.6.0
PACKAGE_256_SUM := b4db1f7525b1e436c62ab622107be0cc4304a477e0b69dec1ab6a91ec352a297
PACKAGE_URL     := https://github.com/hakimel/reveal.js/archive/$(PACKAGE_VERSION).tar.gz

SHA_256_SUM_COMMAND :=
ifeq ($(OS), Darwin)
  SHA_256_SUM_COMMAND := shasum -a 256
  OPEN_COMMAND := open
else
  SHA_256_SUM_COMMAND := sha256sum
  OPEN_COMMAND := xdg-open
endif

.PHONY: help
help:
	@echo 'Sets up Org Reveal'

.PHONY: check
check:
	@echo "$(PACKAGE_256_SUM)  $(PACKAGE_NAME)" | $(SHA_256_SUM_COMMAND) -c - > /dev/null 2>&1

.PHONY: clean
clean:
	@rm -rf ./$(PACKAGE_NAME) ./src/reveal.js ./src/*.html
	@rm -rf ./public/theme

.PHONY: compile
compile:
	HOME=. emacs -batch -kill --load ./.emacs.d/init.el

.PHONY: download
download:
	@curl -fsSL $(PACKAGE_URL) -o reveal.tar.gz

.PHONY: extract
extract:
	@mkdir -p ./src/reveal.js
	@tar -xzf reveal.tar.gz -C ./src/reveal.js --strip-components 1

.PHONY: setup
setup: download check extract

.PHONY: open
open:
	@$(OPEN_COMMAND) ./public/index.html

.PHONY: copy
copy:
	@cp -R src/reveal.js src/shared src/*.html public/
	@cp -R public/theme/white-custom.css public/reveal.js/css/theme/
	@cp -R public/theme/fonts public/reveal.js/
	@cp -R public/theme/images public/images/
	@cp -R assets/docker_*.png public/images/

.PHONY: theme
theme:
	@git clone https://github.com/sdwolfz/reveal-js-fc-theme.git public/theme

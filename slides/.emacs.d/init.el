;;; init.el -*- lexical-binding: t; -*-

(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

(let ((default-directory (expand-file-name "elpa" user-emacs-directory)))
  (unless (file-exists-p default-directory)
    (make-directory default-directory))
  (normal-top-level-add-subdirs-to-load-path))

(require 'package)
(package-initialize t)
(setq
  package-enable-at-startup nil
  package-archives
    '(("melpa-stable" . "http://stable.melpa.org/packages/")
      ("melpa"        . "http://melpa.org/packages/")
      ("org"          . "http://orgmode.org/elpa/")
      ("gnu"          . "http://elpa.gnu.org/packages/")))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package org       :ensure t)
(use-package htmlize   :ensure t)
(use-package ox-reveal :ensure t)

(let* ((src-path (concat (file-name-as-directory default-directory) "/src"))
        (files (directory-files src-path t "\.org")))
  (dolist (file files)
    (find-file file)
    (let ((result (org-reveal-export-to-html)))
      (message "%s" result))
    (kill-buffer (current-buffer))))

;;------------------------------------------------------------------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (ox-reveal htmlize use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

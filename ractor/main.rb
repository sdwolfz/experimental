# frozen_string_literal: true

class Worker
  def initialize(&block)
    @block  = block
  end

  def start
    block = Ractor.make_shareable(@block)

    @ractor = Ractor.new(block) do |callable|
      message = receive

      callable.call(message)
    end

    self
  end

  def work(*args)
    @ractor.send(args)

    @ractor.take
  end
end

worker = Ractor.current.instance_eval do
  Worker.new { |args| puts args.inspect }
end

worker.start.work(1, 2, 3)

# Prompt RB

```sh
docker-here ruby:3.2.0-alpine sh
ruby src/main.rb
```

```sh
brew tap kateinoigakukun/wasi-vfs https://github.com/kateinoigakukun/wasi-vfs.git
brew install kateinoigakukun/wasi-vfs/wasi-vfs wasmer wasmtime
```

```sh
curl -LO https://github.com/ruby/ruby.wasm/releases/latest/download/ruby-head-wasm32-unknown-wasi-full.tar.gz
tar xfz ruby-head-wasm32-unknown-wasi-full.tar.gz

mv head-wasm32-unknown-wasi-full/usr/local/bin/ruby ruby.wasm

wasi-vfs pack ruby.wasm --mapdir /src::./src --mapdir /usr::./head-wasm32-unknown-wasi-full/usr -o my-ruby-app.wasm
wasmtime my-ruby-app.wasm -- /src/my_app.rb
```

```sh
cargo install wasmer-cli --features singlepass,cranelift,llvm
rustup default stable
cargo install wapm-cli
```

```sh
export WASMER_DIR=`pwd`/.wasmer

wasmer create-exe my-ruby-app.wasm -o main --llvm
```

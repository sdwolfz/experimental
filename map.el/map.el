(defun {} ()
  "Creates an empty hash table."
  (make-hash-table :test 'equal))

(defvar } (make-symbol "}"))

(defun { (&rest body)
  "Creates a hash table with elements."
  (let ((table (make-hash-table :test 'equal))
        (finished nil)
        (elements body))
    (while (not finished)
      (let ((first  (nth 0 elements))
            (second (nth 1 elements)))
        (progn
          (cond
           ((and (not (eq } first))
              (not (eq } second)))
             (puthash first second table))
           ((not (eq } first))
             (progn
               (puthash first nil table)
               (setq finished t)))
           (t (setq finished t)))
          (setq elements (cddr elements)))))
    table))

(defun << (key table)
  "Look up a key in the hash table."
  (gethash key table))

(defun >> (key value table)
  "Set a key in the hash table to the provided value."
  (puthash key value table))

(message "%S" ({}))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data ())

(message "%S" ({ }))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data ())

(message "%S" ({ :a 1 :b 2 }))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data (:a 1 :b 2))

(message "%S" ({ :a 1 :b :} }))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data (:a 1 :b :}))

(message "%S" ({ :a 1 :b 2 :c }))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data (:a 1 :b 2 :c nil))

(message "%S" ({ :a 1 :b 2 :c 3 }))
;; => #s(hash-table size 65 test equal rehash-size 1.5 rehash-threshold 0.8125 data (:a 1 :b 2 :c 3))

(message "%S" (<< :a ({ :a 1 })))
;; => 1

(message "%S" (>> :a 2 ({ :a 1 })))
;; => 2

#!/usr/bin/env ruby
# frozen_string_literal: true

ENV['GEM_HOME'] = File.join(__dir__, 'vendor', 'ruby')
Gem.clear_paths

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'

  gem 'bigdecimal'
  gem 'distribution'
  gem 'prime'
end

require 'distribution'

def convolve(m1, m2)
  result = Hash.new(0)
  m1.each do |k1, p1|
    m2.each do |k2, p2|
      result[k1 + k2] += p1 * p2
    end
  end
  result
end

def input_distribution
  print "Enter estimated time: "
  estimate = gets.chomp.to_f
  min = estimate * 0.5
  max = estimate * 2.0
  confidence = 0.8

  steps = 20.0
  step_size = (max - min) / steps
  range_prob = confidence / steps
  distribution = Hash.new(0)

  (0..steps).each do |i|
    value = min + (i * step_size)
    distribution[value.round(1)] = range_prob
  end

  tail_prob = 0.2
  sigma = Math.log(2.0) / 2.0
  mu = Math.log(estimate)

  total_tail_prob = 0.0
  (1..10).each do |i|
    value = max + (i * step_size)
    prob = Distribution::LogNormal.pdf(value, mu, sigma)
    total_tail_prob += prob
    distribution[value.round(1)] = prob
  end

  total_prob = distribution.values.sum
  distribution.each { |k, v| distribution[k] = v / total_prob }

  distribution
end

print "What are you estimating: "
what = gets.chomp

print "How many estimations: "
n = gets.chomp.to_i
components = Array.new(n) { input_distribution }

combined_distribution = components.reduce { |acc, comp| convolve(acc, comp) }

mean = combined_distribution.sum { |k, v| k * v }
sorted = combined_distribution.sort_by { |k, _| k }
cumulative = 0.0
percentiles = {}
sorted.each do |k, v|
  cumulative += v
  percentiles[:p50] ||= k if cumulative >= 0.5
  percentiles[:p80] ||= k if cumulative >= 0.8
  percentiles[:p95_lower] ||= k if cumulative >= 0.05
  percentiles[:p95_upper] ||= k if cumulative >= 0.95

  break if cumulative > 1
end

puts "Average is expected to be about #{percentiles[:p50].round(1)} #{what}."
puts "  * likely to be greater than #{percentiles[:p95_lower].round(1)} #{what} (p = 5%)"
puts "  * has an 20% chance of exceeding #{percentiles[:p80].round(1)} #{what} (p = 80%)"
puts "  * likely to be less than #{percentiles[:p95_upper].round(1)} #{what} (p = 95%)"

#!/usr/bin/env sh

download_from_git () {
  NAME=$1
  VERSION=$2
  SHA=$3
  URL=$4

  DIR="$PWD"/vendor/"$NAME"

  if [ -d "$DIR" ]; then
    cd "$DIR"
    if [ ! -d ".git" ]; then exit 1; fi

    git clean -fdx
    git fetch --depth=1 origin "$VERSION"
    git checkout "$SHA"
  else
    git clone "$URL" "$DIR" -b "$VERSION" --depth=1
    cd "$DIR"
  fi

  if [ "$SHA" != "$(git rev-parse HEAD)" ]; then exit 1; fi
}

download_from_github () {
  NAME=$1
  ASSET=$2
  VERSION=$3
  SHA=$4
  BASE=$5

  DIR="$PWD"/vendor/"$NAME"
  URL="$BASE"/releases/download/"$VERSION"/"$ASSET"

  mkdir -p "$DIR"
  cd "$DIR"

  if [ -f "$ASSET" ]; then
    sha256sum "$DIR"/"$ASSET"

    if echo "$SHA  $DIR/$ASSET" | sha256sum -c -; then
      exit 0
    else
      rm -f "$ASSET"
    fi
  fi

  wget "$URL" -O "$DIR"/"$ASSET"

  sha256sum "$DIR"/"$ASSET"
  echo "$SHA  $DIR/$ASSET" | sha256sum -c -
}

download_file () {
  NAME=$1
  ASSET=$2
  SHA=$3
  URL=$4

  DIR="$PWD"/vendor/"$NAME"

  mkdir -p "$DIR"
  cd "$DIR"

  if [ -f "$ASSET" ]; then
    sha256sum "$DIR"/"$ASSET"

    if echo "$SHA  $DIR/$ASSET" | sha256sum -c -; then
      exit 0
    else
      rm -f "$ASSET"
    fi
  fi

  wget "$URL" -O "$DIR"/"$ASSET"

  sha256sum "$DIR"/"$ASSET"
  echo "$SHA  $DIR/$ASSET" | sha256sum -c -
}

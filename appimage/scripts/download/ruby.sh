#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_git                          \
  ruby                                     \
  v2_7_2                                   \
  5445e0435260b449decf2ac16f9d09bae3cafe72 \
  https://github.com/ruby/ruby.git

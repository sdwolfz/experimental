#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_git                          \
  openssl                                  \
  OpenSSL_1_1_1h                           \
  f123043faa15965c34947670ff3d3a7005d6bdb4 \
  https://github.com/openssl/openssl.git

#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_git                          \
  libxcrypt                                \
  v4.4.17                                  \
  6b110bcd4f4caa61fc39c7339d30adc20a7dd177 \
  https://github.com/besser82/libxcrypt.git

#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_git                          \
  coreutils                                \
  v8.32                                    \
  8d13292a73ecf1f265f77731d3ace29866e3d616 \
  https://github.com/coreutils/coreutils.git

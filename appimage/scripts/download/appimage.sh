#!/usr/bin/env sh

set -ex

. ./scripts/download.sh

download_from_github                                               \
  appimagetool                                                     \
  appimagetool-x86_64.AppImage                                     \
  12                                                               \
  d918b4df547b388ef253f3c9e7f6529ca81a885395c31f619d9aaf7030499a13 \
  https://github.com/AppImage/AppImageKit

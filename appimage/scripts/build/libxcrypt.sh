#!/usr/bin/env sh

set -ex

APPDIR="$PWD"/package

cd vendor/libxcrypt

./autogen.sh
./configure \
  --prefix="$APPDIR"/usr \
  --disable-static \
  --enable-hashes=strong,glibc \
  --enable-obsolete-api=no \
  --disable-failure-tokens

make -j`nproc`
make install -j`nproc`

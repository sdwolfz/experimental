#!/usr/bin/env sh

set -ex

APPDIR="$PWD"/package

cd vendor/coreutils

./bootstrap
./configure \
  --prefix="$APPDIR"/usr \
  --libexecdir="$APPDIR"/usr/lib \
  --with-openssl \
  --enable-no-install-program=groups,hostname,kill,uptime

make -j`nproc`
make install -j`nproc`

#!/usr/bin/env sh

set -ex

APPDIR="$PWD"/package

cd vendor/openssl

./config \
  --prefix="$APPDIR"/usr \
  --openssldir="$APPDIR"/etc/ssl \
  --libdir=lib \
  shared

make depend -j`nproc`
make -j`nproc`
make install_sw -j`nproc`

#!/usr/bin/env sh

set -ex

APPDIR="$PWD"/package

unset GEM_HOME
unset GEM_PATH

cd vendor/ruby

autoconf -f
./configure \
  --prefix=/usr \
  --with-destdir="$APPDIR" \
  --enable-shared \
  --disable-install-doc

make -j`nproc`
make install -j`nproc`

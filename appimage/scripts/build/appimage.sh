#!/usr/bin/env sh

set -ex

FILE=./bin/appimagetool

if [ ! -f "$FILE" ]; then
  ln -s ../vendor/appimagetool/appimagetool-x86_64.AppImage "$FILE"
fi

chmod +x "$FILE"

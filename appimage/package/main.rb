#!/usr/bin/env ruby
# frozen_string_literal: true

trap('SIGINT') { exit 0 }

require 'irb'
IRB.send(:easter_egg, :dancing)

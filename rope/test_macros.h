//-------------------------------------------------------------------------------
// Stubbed `malloc` for testing error cases.
//-------------------------------------------------------------------------------

#define WITH_FAILING_MALLOC(EXP)          \
  do {                                    \
    stubbed_malloc_params.call_count = 0; \
    stubbed_malloc_params.fail_at = 1;    \
    stubbed_malloc_params.fail = true;    \
    do { EXP } while(false);              \
    stubbed_malloc_params.fail = false;   \
  } while (false);

#define WITH_NTH_FAILING_MALLOC(N, EXP)   \
  do {                                    \
    stubbed_malloc_params.call_count = 0; \
    stubbed_malloc_params.fail_at = N;    \
    stubbed_malloc_params.fail = true;    \
    do { EXP } while(false);              \
    stubbed_malloc_params.fail = false;   \
  } while (false);

#define WITH_STUBBED_MALLOC(EXP)          \
  do {                                    \
    stubbed_malloc_params.call_count = 0; \
    stubbed_malloc_params.fail = false;   \
    do { EXP } while(false);              \
  } while (false);

#define ASSERT_MALLOC_CALLS(N)                     \
  do {                                             \
    assert(stubbed_malloc_params.call_count == N); \
  } while (false);

#define ASSERT_MALLOC_SIZE(N)                \
  do {                                       \
    assert(stubbed_malloc_params.size == N); \
  } while (false);

//-------------------------------------------------------------------------------
// Stubbed free for spying on calls.
//-------------------------------------------------------------------------------

#define WITH_STUBBED_FREE(EXP)          \
  do {                                  \
    stubbed_free_params.call_count = 0; \
    do { EXP } while(false);            \
  } while (false);

#define ASSERT_FREE_CALLS(N)                     \
  do {                                           \
    assert(stubbed_free_params.call_count == N); \
  } while (false);

//-------------------------------------------------------------------------------
// Custom Assertions
//-------------------------------------------------------------------------------

#define assert_strings_are_equal(RESULT, EXPECTED)           \
  do {                                                       \
    int _result_length   = strlen(RESULT);                   \
    int _expected_length = strlen(EXPECTED);                 \
                                                             \
    assert(_result_length == _expected_length);              \
                                                             \
    int _index;                                              \
    for(_index = 0; _index <= _result_length; _index += 1) { \
      assert(RESULT[_index] == EXPECTED[_index]);            \
    }                                                        \
  } while (false)

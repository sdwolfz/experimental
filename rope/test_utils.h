// Utility functions for tests.

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

//-------------------------------------------------------------------------------
// Stubbed `malloc` for testing error cases.
//-------------------------------------------------------------------------------

#define malloc(x) stubbed_malloc(x)

struct {
  bool fail;
  unsigned int fail_at;
  unsigned int call_count;

  size_t size;
  void *(*real_malloc)(size_t);
} stubbed_malloc_params;

void *
stubbed_malloc(size_t size);

//-------------------------------------------------------------------------------
// Stubbed free for spying on calls.
//-------------------------------------------------------------------------------

#define free(x) stubbed_free(x)

struct {
  unsigned int call_count;
  void (*real_free)(void *);
} stubbed_free_params;

void
stubbed_free(void *ptr);

//-------------------------------------------------------------------------------
// Capture stdout and return it as a string.
//-------------------------------------------------------------------------------

typedef void (*CapturedIOFunction)();

char * temp_file_path();

char * read_file(char *path);

char * capture_stdout(CapturedIOFunction test_case);

//-------------------------------------------------------------------------------
// Common setup
//-------------------------------------------------------------------------------

void common_test_setup();

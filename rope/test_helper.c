//------------------------------------------------------------------------------
// Deep ropes
//------------------------------------------------------------------------------

// Builds the following rope structure:
//
//          .--[R]~
//     .---(A)---.
//  .-(B)-.   .-(C)-.
// "1"   "2" "3"   "4"
//
struct Rope *
test_helper_build_balanced_deep_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_C;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_2;
  node_C->parent = node_A;
  node_C->left   = node_3;
  node_C->right  = node_4;
  node_1->parent = node_B;
  node_2->parent = node_B;
  node_3->parent = node_C;
  node_4->parent = node_C;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);

  node_R->height = 3;
  node_A->height = 2;
  node_B->height = 1;
  node_C->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;

  node_R->length = 4;
  node_A->length = 2;
  node_B->length = 1;
  node_C->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//             .-[R]~
//         .--(A)-.
//     .--(B)-.  "4"
//  .-(C)-.  "3"
// "1"   "2"
//
struct Rope *
test_helper_build_unbalanced_left_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_4;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_3;
  node_C->parent = node_B;
  node_C->left   = node_1;
  node_C->right  = node_2;
  node_1->parent = node_C;
  node_2->parent = node_C;
  node_3->parent = node_B;
  node_4->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 2;
  node_C->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;

  node_R->length = 4;
  node_A->length = 3;
  node_B->length = 2;
  node_C->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//           .--[R]~
//      .---(A)---.
//  .--(B)-.     "4"
// "1"  .-(C)-.
//     "2"   "3"
//
struct Rope *
test_helper_build_unbalanced_left_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_4;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_2;
  node_C->right  = node_3;
  node_1->parent = node_B;
  node_2->parent = node_C;
  node_3->parent = node_C;
  node_4->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 2;
  node_C->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;

  node_R->length = 4;
  node_A->length = 3;
  node_B->length = 1;
  node_C->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//        .--[R]~
//  .----(A)---.
// "1"     .--(B)-.
//      .-(C)-.  "4"
//     "2"   "3"
//
struct Rope *
test_helper_build_unbalanced_right_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_4;
  node_C->parent = node_B;
  node_C->left   = node_2;
  node_C->right  = node_3;
  node_1->parent = node_A;
  node_2->parent = node_C;
  node_3->parent = node_C;
  node_4->parent = node_B;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 2;
  node_C->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;

  node_R->length = 4;
  node_A->length = 1;
  node_B->length = 2;
  node_C->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//      .-[R]~
//  .--(A)--.
// "1"  .--(B)--.
//     "2"   .-(C)-.
//          "3"   "4"
//
struct Rope *
test_helper_build_unbalanced_right_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_2;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_3;
  node_C->right  = node_4;
  node_1->parent = node_A;
  node_2->parent = node_B;
  node_3->parent = node_C;
  node_4->parent = node_C;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 2;
  node_C->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;

  node_R->length = 4;
  node_A->length = 1;
  node_B->length = 1;
  node_C->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;

  return node_R;
}

//------------------------------------------------------------------------------
// Deeper ropes
//------------------------------------------------------------------------------

// Builds the following rope structure:
//
//            .---[R]~
//     .-----(A)-----.
//  .-(B)-.      .--(C)-.
// "1"   "2"  .-(D)-.  "5"
//           "3"   "4"
//
struct Rope *
test_helper_build_balanced_right_heavy_deeper_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_C;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_2;
  node_C->parent = node_A;
  node_C->left   = node_D;
  node_C->right  = node_5;
  node_D->parent = node_C;
  node_D->left   = node_3;
  node_D->right  = node_4;
  node_1->parent = node_B;
  node_2->parent = node_B;
  node_3->parent = node_D;
  node_4->parent = node_D;
  node_5->parent = node_C;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 1;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 2;
  node_B->length = 1;
  node_C->length = 2;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//             .--[R]~
//      .-----(A)----.
//  .--(B)-.      .-(C)-.
// "1"  .-(D)-.  "4"   "5"
//     "2"   "3"
//
struct Rope *
test_helper_build_balanced_left_heavy_deeper_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_C;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_D;
  node_C->parent = node_A;
  node_C->left   = node_4;
  node_C->right  = node_5;
  node_D->parent = node_B;
  node_D->left   = node_2;
  node_D->right  = node_3;
  node_1->parent = node_B;
  node_2->parent = node_D;
  node_3->parent = node_D;
  node_4->parent = node_C;
  node_5->parent = node_C;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 4;
  node_A->height = 3;
  node_B->height = 2;
  node_C->height = 1;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 3;
  node_B->length = 1;
  node_C->length = 1;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//                  .-[R]~
//              .--(A)-.
//          .--(B)-.  "5"
//      .--(C)-.  "4"
//   .-(D)-.  "3"
//  "1"   "2"
//
struct Rope *
test_helper_build_unbalanced_left_left_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_5;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_4;
  node_C->parent = node_B;
  node_C->left   = node_D;
  node_C->right  = node_3;
  node_D->parent = node_C;
  node_D->left   = node_1;
  node_D->right  = node_2;
  node_1->parent = node_D;
  node_2->parent = node_D;
  node_3->parent = node_C;
  node_4->parent = node_B;
  node_5->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 4;
  node_B->length = 3;
  node_C->length = 2;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//               .-[R]~
//          .---(A)---.
//      .---(B)---.  "5"
//  .--(C)-.     "4"
// "1"  .-(D)-.
//     "2"   "3"
//
struct Rope *
test_helper_build_unbalanced_left_left_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_5;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_4;
  node_C->parent = node_B;
  node_C->left   = node_1;
  node_C->right  = node_D;
  node_D->parent = node_C;
  node_D->left   = node_2;
  node_D->right  = node_3;
  node_1->parent = node_D;
  node_2->parent = node_D;
  node_3->parent = node_C;
  node_4->parent = node_B;
  node_5->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 4;
  node_B->length = 3;
  node_C->length = 1;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//              .--[R]~
//        .----(A)----.
//  .----(B)---.     "5"
// "1"     .--(C)-.
//      .-(D)-.  "4"
//     "2"   "3"
//
struct Rope *
test_helper_build_unbalanced_left_right_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_5;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_D;
  node_C->right  = node_4;
  node_D->parent = node_C;
  node_D->left   = node_2;
  node_D->right  = node_3;
  node_1->parent = node_B;
  node_2->parent = node_D;
  node_3->parent = node_D;
  node_4->parent = node_C;
  node_5->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 4;
  node_B->length = 1;
  node_C->length = 2;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//             .---[R]~
//      .-----(A)-----.
//  .--(B)--.        "5"
// "1"  .--(C)-.
//     "2"  .-(D)-.
//         "3"   "4"
//
struct Rope *
test_helper_build_unbalanced_left_right_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_B;
  node_A->right  = node_5;
  node_B->parent = node_A;
  node_B->left   = node_1;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_2;
  node_C->right  = node_D;
  node_D->parent = node_C;
  node_D->left   = node_3;
  node_D->right  = node_4;
  node_1->parent = node_B;
  node_2->parent = node_C;
  node_3->parent = node_D;
  node_4->parent = node_D;
  node_5->parent = node_A;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 4;
  node_B->length = 1;
  node_C->length = 1;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//          .---[R]~
//  .------(A)-----.
// "1"         .--(B)-.
//         .--(C)-.  "5"
//      .-(D)-.  "4"
//     "2"   "3"
//
struct Rope *
test_helper_build_unbalanced_right_left_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_5;
  node_C->parent = node_B;
  node_C->left   = node_D;
  node_C->right  = node_4;
  node_D->parent = node_C;
  node_D->left   = node_2;
  node_D->right  = node_3;
  node_1->parent = node_A;
  node_2->parent = node_D;
  node_3->parent = node_D;
  node_4->parent = node_C;
  node_5->parent = node_B;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 1;
  node_B->length = 3;
  node_C->length = 2;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//         .--[R]~
//  .-----(A)----.
// "1"      .---(B)---.
//      .--(C)-.     "5"
//     "2"  .-(D)-.
//         "3"   "4"
//
struct Rope *
test_helper_build_unbalanced_right_left_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_C;
  node_B->right  = node_5;
  node_C->parent = node_B;
  node_C->left   = node_2;
  node_C->right  = node_D;
  node_D->parent = node_C;
  node_D->left   = node_3;
  node_D->right  = node_4;
  node_1->parent = node_A;
  node_2->parent = node_C;
  node_3->parent = node_D;
  node_4->parent = node_D;
  node_5->parent = node_B;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 1;
  node_B->length = 3;
  node_C->length = 1;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//       .-[R]~
//  .---(A)---.
// "1"  .----(B)---.
//     "2"     .--(C)-.
//          .-(D)-.  "5"
//         "3"   "4"
//
struct Rope *
test_helper_build_unbalanced_right_right_left_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_2;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_D;
  node_C->right  = node_5;
  node_D->parent = node_C;
  node_D->left   = node_3;
  node_D->right  = node_4;
  node_1->parent = node_A;
  node_2->parent = node_B;
  node_3->parent = node_D;
  node_4->parent = node_D;
  node_5->parent = node_C;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 1;
  node_B->length = 1;
  node_C->length = 2;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

// Builds the following rope structure:
//
//      .-[R]~
//  .--(A)--.
// "1"  .--(B)--.
//     "2"  .--(C)-.
//         "3"  .-(D)-.
//             "4"   "5"
//
struct Rope *
test_helper_build_unbalanced_right_right_right_rope() {
  struct Rope *node_R, *node_A, *node_B, *node_C, *node_D,
              *node_1, *node_2, *node_3, *node_4, *node_5;

  node_R = rope__new_empty_node();
  node_A = rope__new_empty_node();
  node_B = rope__new_empty_node();
  node_C = rope__new_empty_node();
  node_D = rope__new_empty_node();
  node_1 = rope__new_empty_node();
  node_2 = rope__new_empty_node();
  node_3 = rope__new_empty_node();
  node_4 = rope__new_empty_node();
  node_5 = rope__new_empty_node();

  node_R->left   = node_A;
  node_A->parent = node_R;
  node_A->left   = node_1;
  node_A->right  = node_B;
  node_B->parent = node_A;
  node_B->left   = node_2;
  node_B->right  = node_C;
  node_C->parent = node_B;
  node_C->left   = node_3;
  node_C->right  = node_D;
  node_D->parent = node_C;
  node_D->left   = node_4;
  node_D->right  = node_5;
  node_1->parent = node_A;
  node_2->parent = node_B;
  node_3->parent = node_C;
  node_4->parent = node_D;
  node_5->parent = node_D;

  node_1->text = malloc(1);
  node_2->text = malloc(1);
  node_3->text = malloc(1);
  node_4->text = malloc(1);
  node_5->text = malloc(1);

  memcpy(node_1->text, "1", 1);
  memcpy(node_2->text, "2", 1);
  memcpy(node_3->text, "3", 1);
  memcpy(node_4->text, "4", 1);
  memcpy(node_5->text, "5", 1);

  node_R->height = 5;
  node_A->height = 4;
  node_B->height = 3;
  node_C->height = 2;
  node_D->height = 1;
  node_1->height = 0;
  node_2->height = 0;
  node_3->height = 0;
  node_4->height = 0;
  node_5->height = 0;

  node_R->length = 5;
  node_A->length = 1;
  node_B->length = 1;
  node_C->length = 1;
  node_D->length = 1;
  node_1->length = 1;
  node_2->length = 1;
  node_3->length = 1;
  node_4->length = 1;
  node_5->length = 1;

  return node_R;
}

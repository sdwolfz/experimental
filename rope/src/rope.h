#ifndef INCLUDED_ROPE_H
#define INCLUDED_ROPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//-------------------------------------------------------------------------------
// Constants
//-------------------------------------------------------------------------------

#define ROPE_NODE_TEXT_MIN_LENGHT 400
#define ROPE_NODE_TEXT_MAX_LENGHT 1200

//-------------------------------------------------------------------------------
// Data Structure
//-------------------------------------------------------------------------------

struct Rope {
  unsigned int height;
  unsigned int length;

  char *text;

  struct Rope *parent;
  struct Rope *left;
  struct Rope *right;
};

//-------------------------------------------------------------------------------
// Allocation and dealocation
//-------------------------------------------------------------------------------

struct Rope *
rope_new();

void
rope_free(struct Rope *rope);

//-------------------------------------------------------------------------------
// String conversions
//-------------------------------------------------------------------------------

struct Rope *
rope_from_string(char *string);

char *
rope_to_string(struct Rope *rope);

//-------------------------------------------------------------------------------
// Comparison
//-------------------------------------------------------------------------------

int
rope_identical(struct Rope *first, struct Rope *second);

int
rope_equivalent(struct Rope *first, struct Rope *second);

int
rope_equal(struct Rope *first, struct Rope *second);

//-------------------------------------------------------------------------------
// Validation
//-------------------------------------------------------------------------------

int
rope_validate(struct Rope *rope);

//-------------------------------------------------------------------------------
// Manipulation
//-------------------------------------------------------------------------------

size_t
rope_length(struct Rope *rope);

int
rope_index(struct Rope *rope, size_t index);

int
rope_clone(struct Rope *rope, struct Rope **clone);

struct Rope *
rope_concatenate(struct Rope *first, struct Rope *second);

int
rope_split(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
);

int
rope_insert_string(struct Rope *rope, char *string, size_t index);

//------------------------------------------------------------------------------
// Support
//------------------------------------------------------------------------------

void
rope_balance(struct Rope *rope);

//-------------------------------------------------------------------------------
// Debugging
//-------------------------------------------------------------------------------

int
rope_inspect(struct Rope *rope);

int
rope_inspect_internal(struct Rope *rope, int indentation);

//-------------------------------------------------------------------------------
// Private functions
//-------------------------------------------------------------------------------

struct Rope *
rope__new_empty_node();

int
rope__equivalent_nodes(struct Rope *first, struct Rope *second);

//------------------------------------------------------------------------------
// Validation

int
rope__validate_intermediary_node(struct Rope *node);

int
rope__validate_leaf(struct Rope *leaf);

//------------------------------------------------------------------------------
// Splitting

int
rope__split_string(
  struct Rope *rope,
  char        *string,
  size_t       start,
  size_t       length
);

void
rope__copy_text_to_string(
  struct Rope *rope,
  char        *string,
  size_t       start,
  size_t       end
);

int
rope__clone_node(struct Rope *rope, struct Rope **clone_address);

int
rope__split_root_node(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
);

int
rope__split_intermediary_node(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
);

int
rope__split_leaf_node(
  struct Rope  *leaf,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
);

void
rope__recalculate_weights(struct Rope *rope);

size_t
rope__sum_of_right_weights(struct Rope *rope);

int
rope__insert_string_at_index(struct Rope *rope, char *string, size_t string_length, size_t index);

int
rope__insert_string_at_leaf(struct Rope *rope, char *string, size_t string_length, size_t index);

void
rope__balance_nodes(struct Rope *rope);

//------------------------------------------------------------------------------
// Rotations

void
rope__balance_with_right_rotation(struct Rope *rope);

void
rope__balance_with_left_right_rotation(struct Rope *rope);

void
rope__balance_with_right_left_rotation(struct Rope *rope);

void
rope__balance_with_left_rotation(struct Rope *rope);

//------------------------------------------------------------------------------
// Inspect

int
rope__inspect_with_direction(
  struct Rope *rope,
  int          indentation,
  struct Rope *parent,
  short int    direction
);

#endif

#include "rope.h"

// # ROPE

//------------------------------------------------------------------------------
// ## Description
//------------------------------------------------------------------------------
//
// A rope is a binary tree with the following structure:
//
// ```
//   .-----{11}---.
//   |      .----[5]----.
//   |   .-[3]-.     .-[2]-.
//   `->(3)<->(2)<->(2)<->(4)
//     "AAA"  "BB" "CC"  "DDDD"
// ````
//
// Where:
// * `{n}` is a root node with:
//   - `left` node pointing to the first leaf node
//   - `right` node pointing to the internal tree strucure
//   - `lenght` is the lenght of the right subtree
//   - a root node has no parent
// * `[n]` is a intermediary node with:
//   - `left` node pointing to the left subtree
//   - `right` node pointing to the right subtree
//   - `lenght` is the lenght of the right subtree
//   - an intermediary node has a parent but no text
// * `(n)` is a leaf node with:
//   - `left` node pointing to the previous leaf
//   - `right` node pointing to the next leaf
//   - `lenght` is the lenght of the text
//   - a leaf node has a parent and text

//------------------------------------------------------------------------------
// ## Allocation and dealocation
//------------------------------------------------------------------------------

// ### Creates an empty rope.
//
// An empty rope is equivalent to the empty string ("").
//
// The structure is as following:
//
// ```
//   {0}
// ```
//
// With:
//   - height: 0
//   - length: 0
//   - text: a block of memory the size of `ROPE_NODE_TEXT_MAX_LENGHT`
//   - parent: NULL
//   - left: NULL
//   - right: NULL
//
// Complexity:
//   - time:  O(1)
//   - space: O(1)
//
// Returns:
//   - `NULL` if memory allocation failed.
//   - a pointer to the new rope node if memory allocation succeeds.
//
// Progress: DONE.
//
struct Rope *
rope_new() {
  struct Rope *rope;
  char        *text;

  rope = malloc(sizeof(struct Rope));
  if (rope == NULL) { return NULL; }

  text = malloc(ROPE_NODE_TEXT_MAX_LENGHT);
  if (text == NULL) {
    free(rope);

    return NULL;
  }

  rope->text   = text;
  rope->height = 0;
  rope->length = 0;
  rope->parent = NULL;
  rope->left   = NULL;
  rope->right  = NULL;

  return rope;
}

// Frees a rope node and all it's children.
//
// Complexity:
//   - time:  O(n)
//   - space: O(log(n))
//
// Progress: TODO.
// This can use O(1) space by traversing via it's `parent` references.
void
rope_free(struct Rope *rope) {
  if (rope == NULL) { return; }

  struct Rope *left, *right;

  left  = rope->left;
  right = rope->right;

  if (left  != NULL) { rope_free(left);  }
  if (right != NULL) { rope_free(right); }

  free(rope->text);
  free(rope);
}

//------------------------------------------------------------------------------
// String conversions
//------------------------------------------------------------------------------

// Creates a rope from a string.
struct Rope *
rope_from_string(char *string) {
  if (string == NULL) { return NULL; }

  size_t length = strlen(string);

  struct Rope *rope = rope__new_empty_node();
  if (rope == NULL) { return NULL; }

  int result = rope__split_string(rope, string, 0, length);
  if (result != 0) {
    rope_free(rope);

    return NULL;
  }

  struct Rope *root = rope__new_empty_node();
  if (root == NULL) {
    rope_free(rope);

    return NULL;
  }

  root->height = rope->height + 1;
  root->length = length;
  root->left   = rope;
  rope->parent = root;

  return root;
}

// Converts a rope into a `NULL` terminated string.
char *
rope_to_string(struct Rope *rope) {
  if (rope == NULL) { return NULL; }

  size_t  length = rope->length;
  char   *string = malloc(length + 1);
  if (string == NULL) { return NULL; }

  if (length > 0) {
    rope__copy_text_to_string(rope->left, string, 0, length);
  }
  string[length] = '\0';

  return string;
}

//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

// Compares two ropes for identity unsing their memory addresses.
//
// Complexity:
//   - time:  O(1)
//   - space: O(1)
//
// Returns:
//   - 1 (true) if the addresses are equal (pointers point to the same memory).
//   - 0 (false) if the addresses are not equal, or any of them in `NULL`.
//
int
rope_identical(struct Rope *first, struct Rope *second) {
  if (first == NULL || second == NULL) { return 0; }

  return first == second;
}

// Compares two ropes vor equivalence node by node.
//
// Returns:
//   - 1 (true) if ropes are equivalent.
//   - 0 (false) if ropes are not equivalent, or any of them is `NULL`.
//
int
rope_equivalent(struct Rope *first, struct Rope *second) {
  if (first == NULL || second == NULL) { return 0; }

  return rope__equivalent_nodes(first, second);
}

// Compares two ropes lexicographically by their text values.
//
// TODO: space complexity can be reduced to O(1).
// Complexity:
//   - time:  O(n)
//   - space: O(n)
//
// Returns:
//   - 1 (true) if ropes are equal.
//   - 0 (false) if ropes are not equal, or any of them is `NULL`.
//
int
rope_equal(struct Rope *first, struct Rope *second) {
  if (first == NULL || second == NULL) { return 0; }

  char *first_string, *second_string;
  int   result;

  first_string  = rope_to_string(first);
  second_string = rope_to_string(second);

  result = strcmp(first_string, second_string);
  if (result == 0) { return 1; }

  free(first_string);
  free(second_string);

  return 0;
}

//------------------------------------------------------------------------------
// Validation
//------------------------------------------------------------------------------

// Validates a rope.
//
// Returns:
//   - `0` if the rope is valid.
//   - `1` if the rope is `NULL`.
//   - `2` if the rope is invalid.
//
// TODO: implement this.
int
rope_validate(struct Rope *rope) {
  if (rope == NULL) { return 1; }

  int result;

  result = rope__validate_intermediary_node(rope);
  if (result != 0) { return 2; }

  return 0;
}

//------------------------------------------------------------------------------
// Manipulation
//------------------------------------------------------------------------------

// Returns the length of a `rope`.
//
// The lenght of a rope if the weight of it's root node.
// In case `NULL` is passed to this function, it returns `0`, since a missing
// rope can be considered to be equivalent to an empty rope, at least concerning
// it's size.
//
// Complexity:
//   - time:  O(1)
//   - space: O(1)
//
// Examples:
// * For a `NULL` rope:
//     ```c
//     rope_lenght(NULL); // => 0
//     ```
//
// * For an empty rope:
//     ~[0]~
//
//    ```c
//    rope_length(rope); // => 0
//    ```
//
// * For a regular rope:
//
//               .--[11]~
//         .----(5)----.
//      .-(3)-.     .-(2)-.
//     (3)   (2)   (2)   (4)
//    "AAA"  "BB" "CC"  "DDDD"
//
//    ```c
//    rope_length(rope); // => 11
//    ```
size_t
rope_length(struct Rope *rope) {
  if (rope == NULL) { return 0; }

  return rope->length;
}

// Returns the character at `index`.
//
// Rope indexing starts at 0.
//
// Complexity:
//   - time:  O(log(n))
//   - space: O(1)
//
// Examples:
// * For a `NULL` rope:
//     ```c
//     rope_index(NULL); // => -1
//     ```
//
//                .--[11]~
//          .----(5)----.
//       .-(3)-.     .-(2)-.
//      (3)   (2)   (2)   (4)
//     "AAA"  "BB" "CC"  "DDDD"
//
//     ```c
//     rope_index(rope, 4); // => B
//     ```
int
rope_index(struct Rope *rope, size_t index) {
  if (rope == NULL) return -1;

  if (rope->length <= index) {
    return rope_index(rope->right, index - rope->length);
  }

  if (rope->left != NULL) {
    return rope_index(rope->left, index);
  }

  return rope->text[index];
}


// Clones a rope.
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Source `Rope` is NULL
//   - 2: Failed to allocate memory with `malloc`
//
int
rope_clone(struct Rope *rope, struct Rope **clone_address) {
  if (rope == NULL) {
    *clone_address = NULL;

    return 1;
  }

  struct Rope *clone;
  int          result;

  result = rope__clone_node(rope, &clone);
  if (result == 1) { return 2; }

  *clone_address = clone;
  return 0;
}

// Concatenates two ropes, returning a pointer to the new rope.
//
// WARNING: Any reference to the old rope needs to be removed since they now
// point to internal nodes which are not valid ropes by themselves.
//
//       .-[5]~        .-[6]~               .--[11]~
//    .-(3)-.   +   .-(2)-.    =>     .----(5)----.
//   (3)   (2)     (2)   (4)       .-(3)-.     .-(2)-.
//  "AAA"  "BB"   "CC"  "DDDD"    (3)   (2)   (2)   (4)
//                               "AAA" "BB"  "CC"  "DDDD"
//
struct Rope *
rope_concatenate(struct Rope *first, struct Rope *second) {
  if (first  == NULL) { return second; }
  if (second == NULL) { return first;  }

  first->right = second->left;
  second->left->parent = first;

  second->left  = first;
  first->parent = second;

  second->length += first->length;

  rope_balance(second);

  return second;
}

// Splits a rope into two.
//
//               .-[17]~                  .-[8]~           .-[9]~
//       .------(11)-----.            .-(3)-.         .---(3)--.
//   .--(3)--.        .-(2)-.    =>  (3)   (5)    +  (3)    .-(2)-.
//  (3)     (8)      (2)   (4)      "AAA" "BBBBB"   "CCC"  (2)   (4)
// "AAA" "BBBBBCCC" "DD"  "EEEE"                          "DD" "EEEE"
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Source `Rope` is NULL
//   - 2: Index out of bounds
//   - 3: Failed to allocate memory with `malloc`
int
rope_split(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
) {
  if (rope == NULL)               { return 1; }
  if (split_index > rope->length) { return 2; }

  struct Rope *first, *second;
  first  = NULL;
  second = NULL;

  int result = rope__split_root_node(rope, split_index, &first, &second);
  if (result != 0) { return 3; }

  rope__recalculate_weights(first);
  rope__recalculate_weights(second);

  rope_balance(first);
  rope_balance(second);

  *first_address  = first;
  *second_address = second;

  return 0;
}

// Insert a string in a rope.
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Source `Rope` is NULL
//   - 2: Source `string` is NULL
//   - 3: Index out of bounds
//   - 4: Failed to allocate memory with `malloc`
int
rope_insert_string(struct Rope *rope, char *string, size_t index) {
  if (rope   == NULL) { return 1; }
  if (string == NULL) { return 2; }

  if (index > rope->length) { return 3; }

  int    result;
  size_t length;

  length = strlen(string);

  result = rope__insert_string_at_index(rope, string, length, index);
  if (result == 1) { return 4; }

  return 0;
}

//------------------------------------------------------------------------------
// Support
//------------------------------------------------------------------------------

void
rope_balance(struct Rope *rope) {
  if (rope == NULL) { return; }

  rope__balance_nodes(rope->left);

  rope->height = rope->left->height + 1;
}

//------------------------------------------------------------------------------
// Debugging
//------------------------------------------------------------------------------

// Prints a rope to stdout.
int
rope_inspect(struct Rope *rope) {
  return rope__inspect_with_direction(rope, 0, NULL, 0);
}

// Prints a rope to stdout with indentaion required by nested calls.
int
rope_inspect_internal(struct Rope *rope, int indentation) {
  return rope__inspect_with_direction(rope, indentation, NULL, 0);
}

//------------------------------------------------------------------------------
// Private functions
//------------------------------------------------------------------------------

struct Rope *
rope__new_empty_node() {
  struct Rope *rope = malloc(sizeof(struct Rope));
  if (rope == NULL) { return NULL; }

  rope->height = 0;
  rope->length = 0;
  rope->text   = NULL;
  rope->parent = NULL;
  rope->left   = NULL;
  rope->right  = NULL;

  return rope;
}

int
rope__equivalent_nodes(struct Rope *first, struct Rope *second) {
  int result, second_result;

  result        = first  == NULL;
  second_result = second == NULL;

  if (!result != !second_result) { return 1; } /* LCOV_EXCL_BR_LINE */
  if (result && second_result)   { return 1; } /* LCOV_EXCL_BR_LINE */

  result = first->height == second->height
        && first->length == second->length;    /* LCOV_EXCL_BR_LINE */
  if (!result) { return 0; }

  result        = first->text  == NULL;
  second_result = second->text == NULL;

  if (!result != !second_result) { return 1; } /* LCOV_EXCL_BR_LINE */
  if (result && second_result) {               /* LCOV_EXCL_BR_LINE */
    result = 1;
  } else {
    result = strncmp(first->text, second->text, first->length) == 0;
  }
  if (result == 0) { return 0; }

  result = rope__equivalent_nodes(first->left, second->left);
  if (result == 0) { return 0; }

  result = rope__equivalent_nodes(first->right, second->right);
  return result;
}

//------------------------------------------------------------------------------
// Validation

// Validates an internediary node.
//
// Returns:
//   - `0` if the node and it's children are valid.
//   - `1` if the node is an invalid leaf.
//   - `2` if the node does not have left and right children.
//   - `3` if the left child is invalid.
//   - `4` if the right child is invalid.
int
rope__validate_intermediary_node(struct Rope *node) {
  int result;

  if (node->text != NULL) {
    result = rope__validate_leaf(node);

    if (result != 0) { return 1; }
  }
  else {
    if (node->left == NULL || node->right == NULL) { return 2; }

    result = rope__validate_intermediary_node(node->left);
    if (result != 0) { return 3; }

    result = rope__validate_intermediary_node(node->right);
    if (result != 0) { return 4; }
  }

  return 0;
}

// Validates a leaf node.
//
// Returns:
//   - `0` if the leaf is valid.
//   - `1` if the leaf does not have text.
//   - `2` if the leaf is longer that allowed.
//   - `3` if the leaf is shorter than allowed.
//   - `4` if the leaf height is not `0`.
//   - `5` if the left leaf does not point back.
//   - `6` if the right leaf does not point back.
//
int
rope__validate_leaf(struct Rope *leaf) {
  if (leaf->text == NULL) { return 1; }

  if (leaf->height > 0) { return 2; }

  if (leaf->length > ROPE_NODE_TEXT_MAX_LENGHT) { return 3; }
  if (leaf->length < ROPE_NODE_TEXT_MIN_LENGHT
        && leaf->parent != NULL) {
    return 4;
  }

  if (leaf->left  != NULL && leaf->left->right != leaf) { return 5; }
  if (leaf->right != NULL && leaf->right->left != leaf) { return 6; }

  return 0;
}

//------------------------------------------------------------------------------
// Splitting

// Splits a string into a rope based on the max threshold.
int
rope__split_string(
  struct Rope *rope,
  char        *string,
  size_t       start,
  size_t       end
) {
  size_t length = end - start;
  if (length > ROPE_NODE_TEXT_MAX_LENGHT) {
    struct Rope  *left, *right;
    unsigned int  max_height, left_height, right_height;
    int           result;
    size_t        middle;

    middle = length / 2;

    left = rope__new_empty_node();
    if (left == NULL) { return 1; }
    rope->left   = left;
    left->parent = rope;

    right = rope__new_empty_node();
    if (right == NULL) { return 1; }
    rope->right   = right;
    right->parent = rope;

    result = rope__split_string(left, string, start, start + middle);
    if (result == 1) { return 1; }

    result = rope__split_string(right, string, start + middle, end);
    if (result == 1) { return 1; }

    left_height  = left->height;
    right_height = right->height;

    max_height   = left_height > right_height ? left_height : right_height;
    rope->height = max_height + 1;

    rope->length = middle;
  } else {
    char *text = malloc(ROPE_NODE_TEXT_MAX_LENGHT);
    if (text == NULL) { return 1; }

    memcpy(text, string + start, length);

    rope->height = 0;
    rope->length = length;
    rope->text   = text;
  }

  return 0;
}

// Copies the text from a rope to the given string.
void
rope__copy_text_to_string(
  struct Rope *rope,
  char        *string,
  size_t       start,
  size_t       end
) {
  if (rope->text == NULL) {
    size_t middle = start + rope->length;

    rope__copy_text_to_string(rope->left,  string, start,  middle);
    rope__copy_text_to_string(rope->right, string, middle, end);
  } else {
    memcpy(string + start, rope->text, rope->length);
  }
}

// Clones a rope recursively.
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Failed to allocate memory with `malloc`
//
int
rope__clone_node(struct Rope *rope, struct Rope **clone_address) {
  if (rope == NULL) { return 0; }

  struct Rope *node;

  node = rope__new_empty_node();
  if (node == NULL) { return 1; }

  node->height = rope->height;
  node->length = rope->length;

  if (rope->text != NULL) {
    char *text = malloc(ROPE_NODE_TEXT_MAX_LENGHT + 1);
    if (text == NULL) {
      rope_free(node);

      return 1;
    }

    memcpy(text, rope->text, rope->length);
  } else {
    int result;

    result = rope__clone_node(rope->left, &node->left);
    if (result == 1) {
      rope_free(node);

      return 1;
    }

    result = rope__clone_node(rope->right, &node->right);
    if (result == 1) {
      rope_free(node);

      return 1;
    }
  }

  *clone_address = node;
  return 0;
}

// Split a rope starting from the root.
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Failed to allocate memory with `malloc`
int
rope__split_root_node(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
) {
  int result = rope__split_intermediary_node(
    rope,
    split_index,
    first_address,
    second_address
  );
  if (result != 0) { return 1; }

  (*first_address)->parent = NULL;

  struct Rope *first_root, *second_root;
  first_root  = *first_address;
  second_root = *second_address;

  if (first_root->left->right == NULL && first_root->left->text == NULL) {
    first_root->left->parent = NULL;
    *first_address           = first_root->left;
    second_root              = first_root;
  } else {
    second_root = rope__new_empty_node();
    if (second_root == NULL) { return 1; }
  }

  second_root->left         = *second_address;
  second_root->left->parent = second_root;
  second_root->length       = second_root->left->length;
  *second_address           = second_root;

  return 0;
}

// Splits an intermediary node joining the results in two ropes.
//
//       ?                   ?           ?
//   .--(3)--.      =>   .--(3)-.    +  (3)
//  (3)     (8)         (3)    (5)     "CCC"
// "AAA" "BBBBBCCC"    "AAA" "BBBBB"
//
// Returns the success/error status:
//   - 0: Success
//   - 1: Failed to allocate memory with `malloc`
int
rope__split_intermediary_node(
  struct Rope  *rope,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
) {
  int result;

  if (rope->text == NULL) {
    if (split_index <= rope->length) {
      result = rope__split_intermediary_node(
        rope->left,
        split_index,
        first_address,
        second_address
      );
      if (result != 0) { return result; }

      rope->left         = *first_address;
      rope->left->parent = rope;

      struct Rope *node = NULL;
      if (rope->parent != NULL && rope->parent->parent != NULL) {
        node = rope->left;

        rope->text   = node->text;
        rope->length = node->length;
        rope->left   = NULL;

        node->parent = NULL;
        node->length = 0;
        node->text   = NULL;
      }

      if (rope->right != NULL){
        if (node == NULL) { node = rope__new_empty_node(); }
        if (node == NULL) { return 1;          }

        node->left          = *second_address;
        node->right         = rope->right;
        node->left->parent  = node;
        node->right->parent = node;
        rope->right         = NULL;

        *second_address = node;
      }
    } else {
      result = rope__split_intermediary_node(
        rope->right,
        split_index - rope->length,
        first_address,
        second_address
      );
      if (result != 0) { return result; }

      rope->right         = *first_address;
      rope->right->parent = rope;

    }
    *first_address = rope;

    return 0;
  } else {
    result = rope__split_leaf_node(
      rope,
      split_index,
      first_address,
      second_address
    );
    if (result == 1) { return 1; }

    return 0;
  }
}

// Splits a leaf node into two.
//
//    ?         ?       ?
//   (6)    => (4)   + (2)
// "AAAABB"   "AAAA"   "BB"
//
// Returns the success/error status:
//   - `0`: Success.
//   - `1`: Failed to allocate memory with `malloc`.
int
rope__split_leaf_node(
  struct Rope  *leaf,
  size_t        split_index,
  struct Rope **first_address,
  struct Rope **second_address
) {
  struct Rope *second;
  char        *first_text, *second_text;
  size_t       first_length, second_length;

  first_length  = leaf->length;
  second_length = first_length - split_index;

  first_text = leaf->text;

  second_text = malloc(ROPE_NODE_TEXT_MAX_LENGHT);
  if (second_text == NULL) {
    return 1;
  }

  memcpy(second_text, first_text + split_index, second_length);

  second = rope__new_empty_node();
  if (second == NULL) {
    free(second_text);

    return 1;
  }

  second->length  = second_length;
  second->text    = second_text;
  *second_address = second;

  leaf->length   = split_index;
  *first_address = leaf;

  return 0;
}

void rope__recalculate_weights(struct Rope *rope) {
  if (rope == NULL) { return; }

  if (rope->left != NULL) {
    rope__recalculate_weights(rope->left);
  }
  if (rope->text == NULL) {
    size_t right_weight;

    right_weight = rope__sum_of_right_weights(rope->left->right);
    rope->length = rope->left->length + right_weight;

    rope__recalculate_weights(rope->right);
  }
}

size_t
rope__sum_of_right_weights(struct Rope *rope) {
  if (rope == NULL) { return 0; }

  return rope->length + rope__sum_of_right_weights(rope->right);
}

int
rope__insert_string_at_index(struct Rope *rope, char *string, size_t string_length, size_t index) {
  if (index > rope->length) {
    return rope__insert_string_at_index(rope->right, string, string_length, index - rope->length);
  }

  if (rope->left != NULL) {
    rope->length += string_length;

    return rope__insert_string_at_index(rope->left, string, string_length, index);
  }

  return rope__insert_string_at_leaf(rope, string, string_length, index);
}

int
rope__insert_string_at_leaf(struct Rope *rope, char *string, size_t string_length, size_t index) {
  size_t text_length, text_unused, string_start, string_end;

  text_length = rope->length;
  text_unused = ROPE_NODE_TEXT_MAX_LENGHT - text_length;

  string_start = 0;
  string_end   = string_length;

  if (text_unused > 0 && string_length <= text_unused) { /* LCOV_EXCL_BR_LINE */
    size_t to_copy = string_length;

    memcpy(rope->text + index + to_copy, rope->text + index, text_length - to_copy + 1);
    memcpy(rope->text + index, string, to_copy);

    rope->length = text_length + to_copy;
  } else {
    struct Rope *old, *new, *left, *right;
    char        *text;
    int          result;

    // Left node with the old text.
    old = rope__new_empty_node();
    if (old == NULL) { return 1; }

    old->length = rope->length;
    old->text   = rope->text;
    rope->text  = NULL;

    rope->left  = old;
    old->parent = rope;

    // Right node with 2 children.
    new = rope__new_empty_node();
    if (new == NULL) { return 1; }

    rope->right = new;
    new->parent = rope;

    // Left child with the inserted text.
    left = rope__new_empty_node();
    if (left == NULL) { return 1; }

    new->left    = left;
    left->parent = new;

    result = rope__split_string(left, string, string_start, string_end);
    if (result == 1) { return 1; }

    left->length = string_end;
    new->length  = string_end;

    // Right child with the rest of the text.
    right = rope__new_empty_node();
    if (right == NULL) { return 1; }

    new->right    = right;
    right->parent = new;

    text = malloc(ROPE_NODE_TEXT_MAX_LENGHT);
    if (text == NULL) { return 1; }

    right->text   = text;
    right->length = old->length - index;

    memcpy(text, old->text + index, old->length - index);
    old->length  -= right->length;
    rope->length -= right->length;
  }

  return 0;
}

void
rope__balance_nodes(struct Rope *rope) {
  if (rope->text == NULL) {
    unsigned int left_height, right_height, maximum_height;
    int          balance;

    rope__balance_nodes(rope->left);
    rope__balance_nodes(rope->right);

    balance = rope->right->height - rope->left->height;

    if (balance < -1) {
      balance = rope->left->right->height - rope->left->left->height;

      if (balance <= 0) {
        rope__balance_with_right_rotation(rope);
      } else {
        rope__balance_with_left_right_rotation(rope);
      }

      rope = rope->parent;
    } else if (balance > 1) {
      balance = rope->right->right->height - rope->right->left->height;

      if (balance >= 0) {
        rope__balance_with_left_rotation(rope);
      } else {
        rope__balance_with_right_left_rotation(rope);
      }

      rope = rope->parent;
    }

    left_height  = rope->left->height;
    right_height = rope->right->height;

    maximum_height = left_height > right_height ? left_height : right_height;
    rope->height   = maximum_height + 1;
  }
}

//------------------------------------------------------------------------------
// Rotations

// Performs the following transformations:
//
//            [R]                [R]
//             ?                  ?
//         .--(A)-.          .---(B)---.
//     .--(B)-.  "4"  =>  .-(C)-.   .-(A)-.
//  .-(C)-.  "3"         "1"   "2" "3"   "4"
// "1"   "2"
//
//               [R]                    [R]
//                ?                      ?
//          .----(A)----.         .-----(B)-----.
//     .---(B)---.     "5" =>  .-(C)-.      .--(A)-.
//  .-(C)-.   .-(D)-.         "1"   "2"  .-(D)-.  "5"
// "1"   "2" "3"   "4"                  "3"   "4"
//
void
rope__balance_with_right_rotation(struct Rope *rope) {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope->parent;
  node_A = rope;
  node_B = node_A->left;
  node_C = node_B->left;
  node_1 = node_C->left;
  node_2 = node_C->right;
  node_3 = node_B->right;
  node_4 = node_A->right;

  node_A->left   = node_3;
  node_3->parent = node_A;

  node_B->right  = node_A;
  node_A->parent = node_B;

  node_C->left   = node_1;
  node_C->right  = node_2;
  node_1->parent = node_C;
  node_2->parent = node_C;

  node_B->parent = node_R;
  if (node_R->left == node_A) {
    node_R->left = node_B;
  } else {
    node_R->right = node_B;
  }

  node_A->length = node_A->length - node_B->length;

  unsigned int left_height, right_height, maximum_height;

  left_height    = node_3->height;
  right_height   = node_4->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_A->height = maximum_height + 1;

  left_height    = node_C->height;
  right_height   = node_A->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_B->height = maximum_height + 1;

  node_R->height = node_B->height + 1;
}

// Performs the following transformation:
//
//          [R]                 [R]
//           ?                   ?
//      .---(A)---.         .---(C)---.
//  .--(B)-.     "4" =>  .-(B)-.   .-(A)-.
// "1"  .-(C)-.         "1"   "2" "3"   "4"
//     "2"   "3"
//
void
rope__balance_with_left_right_rotation(struct Rope *rope) {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope->parent;
  node_A = rope;
  node_B = node_A->left;
  node_C = node_B->right;
  node_1 = node_B->left;
  node_2 = node_C->left;
  node_3 = node_C->right;
  node_4 = node_A->right;

  node_A->left   = node_3;
  node_3->parent = node_A;

  node_B->right  = node_2;
  node_2->parent = node_B;

  node_C->left   = node_B;
  node_C->right  = node_A;
  node_B->parent = node_C;
  node_A->parent = node_C;

  node_C->parent = node_R;
  if (node_R->left == node_A) {
    node_R->left = node_C;
  } else {
    node_R->right = node_C;
  }

  node_A->length = node_A->length - node_B->length - node_C->length;
  node_C->length = node_B->length + node_C->length;

  unsigned int left_height, right_height, maximum_height;

  left_height    = node_3->height;
  right_height   = node_4->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_A->height = maximum_height + 1;

  left_height    = node_1->height;
  right_height   = node_2->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_B->height = maximum_height + 1;

  left_height    = node_B->height;
  right_height   = node_A->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_C->height = maximum_height + 1;

  node_R->height = node_B->height + 1;
}

// Performs the following transformation:
//
//       [R]                    [R]
//        ?                      ?
//  .----(A)---.            .---(C)---.
// "1"     .--(B)-.  =>  .-(A)-.   .-(B)-.
//      .-(C)-.  "4"    "1"   "2" "3"   "4"
//     "2"   "3"
//
void
rope__balance_with_right_left_rotation(struct Rope *rope) {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope->parent;
  node_A = rope;
  node_B = node_A->right;
  node_C = node_B->left;
  node_1 = node_A->left;
  node_2 = node_C->left;
  node_3 = node_C->right;
  node_4 = node_B->right;

  node_A->right  = node_2;
  node_2->parent = node_A;

  node_B->left   = node_3;
  node_3->parent = node_B;

  node_C->left   = node_A;
  node_C->right  = node_B;
  node_B->parent = node_C;
  node_A->parent = node_C;

  node_C->parent = node_R;
  if (node_R->left == node_A) {
    node_R->left = node_C;
  } else {
    node_R->right = node_C;
  }

  node_B->length = node_B->length - node_C->length;
  node_C->length = node_A->length + node_C->length;

  unsigned int left_height, right_height, maximum_height;

  left_height    = node_1->height;
  right_height   = node_2->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_A->height = maximum_height + 1;

  left_height    = node_3->height;
  right_height   = node_4->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_B->height = maximum_height + 1;

  left_height    = node_A->height;
  right_height   = node_B->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_C->height = maximum_height + 1;

  node_R->height = node_B->height + 1;
}

// Performs the following transformation:
//
//      .-[R]~                    .--[R]~
//  .--(A)--.                .---(B)---.
// "1"  .--(B)--.     =>  .-(A)-.   .-(C)-.
//     "2"   .-(C)-.     "1"   "2" "3"   "4"
//          "3"   "4"
//
void
rope__balance_with_left_rotation(struct Rope *rope) {
  struct Rope *node_R, *node_A, *node_B, *node_C,
              *node_1, *node_2, *node_3, *node_4;

  node_R = rope->parent;
  node_A = rope;
  node_B = node_A->right;
  node_C = node_B->right;
  node_1 = node_A->left;
  node_2 = node_B->left;
  node_3 = node_C->left;
  node_4 = node_C->right;

  node_A->right  = node_2;
  node_2->parent = node_A;

  node_B->left   = node_A;
  node_A->parent = node_B;

  node_C->left   = node_3;
  node_C->right  = node_4;
  node_3->parent = node_C;
  node_4->parent = node_C;

  node_B->parent = node_R;
  if (node_R->left == node_A) {
    node_R->left = node_B;
  } else {
    node_R->right = node_B;
  }

  node_B->length = node_A->length + node_B->length;

  unsigned int left_height, right_height, maximum_height;

  left_height    = node_1->height;
  right_height   = node_2->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_A->height = maximum_height + 1;

  left_height    = node_A->height;
  right_height   = node_C->height;
  maximum_height = left_height > right_height ? left_height : right_height;
  node_B->height = maximum_height + 1;

  node_R->height = node_B->height + 1;
}

//------------------------------------------------------------------------------
// Inspect

// Prints a rope to stdout, taknig into account the node's parent and direction,
// with indentaion required by nested calls.
int
rope__inspect_with_direction(
  struct Rope *rope,
  int          indentation,
  struct Rope *parent,
  short int    direction
) {
  printf("<#Rope\n");

  printf("%*s", indentation, "");
  printf("  height: %u\n", rope->height);

  printf("%*s", indentation, "");
  printf("  weight: %u\n", rope->length);

  printf("%*s", indentation, "");
  printf("  text:   \"%.*s\"\n", rope->length, rope->text);

  printf("%*s", indentation, "");
  if (parent == NULL) {
    printf("  parent: NULL\n");
  } else {
    if (direction == -1) {
      if (parent == rope->parent) {
        printf("  parent: Valid (I am the LEFT child)\n");
      } else {
        printf("  parent: Missmatch (I am the LEFT child)\n");
      }
    } else if (direction == 1) {
      if (parent == rope->parent) {
        printf("  parent: Valid (I am the RIGHT child)\n");
      } else {
        printf("  parent: Missmatch (I am the RIGHT child)\n");
      }
    } else {
      printf("  parent: Corrupt direction: %d\n", direction);
    }
  }

  printf("%*s", indentation, "");
  if (rope->left == NULL) {
    printf("  left:   NULL\n");
  } else {
    printf("  left:   ");
    rope__inspect_with_direction(rope->left, indentation + 10, rope, -1);
    printf("\n");
  }

  printf("%*s", indentation, "");
  if (rope->right == NULL) {
    printf("  right:  NULL>");
  } else {
    printf("  right:  ");
    rope__inspect_with_direction(rope->right, indentation + 10, rope, 1);
    printf(">");
  }

  if (indentation == 0) {
    printf("\n");
  }

  return 0;
}

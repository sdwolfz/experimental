# Native Tests

Unit tests for the rope implementation.

# Run tests

```bash
make test
```

# Clean

To clean up test artifacts:
```bash
make clean
```

To clean up all generated files:
```
make purge
```

# Debug

One of the following:
```bash
gdb -tui ./clang-test
gdb -tui ./gcc-test
gdb -tui ./gcc-test-coverage
gdb -tui ./test
```

# Guard

```bash
make guard
```

# Docker

## Prerequisites

Add these aliases to your `.bash_profile` or `.bashrc`
```bash
# Runs a temporary container as the `root` user inside the current directory.
alias docker-root-here='docker run --rm -it -v "$PWD":/work -w /work'

# Runs a temporary container as the current user inside the current directory.
alias docker-here='docker-root-here -u `id -u`:`id -g`'
```

## Build

Long version:
```bash
docker build                        \
  -t emacs                          \
  --build-arg HOST_USER_UID=`id -u` \
  --build-arg HOST_USER_GID=`id -g` \
  -f test/native/docker/Dockerfile  \
  "$PWD"
```
OR the short version if you are user 1000:1000
```bash
docker build -t emacs -f test/native/docker/Dockerfile "$PWD"
```

## Run

```bash
docker-here -w /work/test/native emacs
```

## Shell

```bash
docker-here -w /work/test/native emacs bash -l
```

## Coverage Report

```bash
xdg-open ./test/native/tmp/cover/index.html
```

void test_rope_clone_for_a_null_rope() {
  struct Rope *rope, *clone, *placeholder;
  int          result;

  rope        = NULL;
  placeholder = rope_new();
  clone       = placeholder;

  result = rope_clone(rope, &clone);

  assert(result == 1);
  assert(clone  == NULL);

  rope_free(placeholder);
}

void test_rope_clone_for_an_empty_rope() {
  struct Rope *rope, *clone, *placeholder;
  int          result;

  rope        = rope_new();
  placeholder = rope_new();
  clone       = placeholder;

  result = rope_clone(rope, &clone);

  assert(result == 0);
  assert(clone  != rope);
  assert(clone  != placeholder);

  rope_free(clone);
  rope_free(placeholder);
  rope_free(rope);
}

void test_rope_clone_for_a_small_rope() {
  struct Rope *rope, *clone, *placeholder;
  char        *string;
  int          result;

  rope        = rope_from_string("Hello World");
  placeholder = rope_new();
  clone       = placeholder;

  result = rope_clone(rope, &clone);
  string = rope_to_string(rope);

  assert(result == 0);
  assert(clone  != rope);
  assert(clone  != placeholder);
  assert_strings_are_equal(string, "Hello World");

  rope_free(clone);
  rope_free(placeholder);
  rope_free(rope);
}

void test_rope_clone_for_a_deep_rope() {
  size_t  length = 2 * ROPE_NODE_TEXT_MAX_LENGHT + 1;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length; index += 1) {
    string[index] = 'a';
  }
  string[length] = '\0';

  struct Rope *rope, *clone, *placeholder;
  char        *output;
  int          result;

  rope        = rope_from_string(string);
  placeholder = rope_new();
  clone       = placeholder;

  result = rope_clone(rope, &clone);
  output = rope_to_string(rope);

  assert(result == 0);
  assert(clone  != rope);
  assert(clone  != placeholder);
  assert(rope_equivalent(rope, clone) == 1);
  assert_strings_are_equal(output, string);

  free(output);
  free(string);
  rope_free(clone);
  rope_free(placeholder);
  rope_free(rope);
}

void test_rope_clone_when_malloc_fails() {
  size_t  length = 2 * ROPE_NODE_TEXT_MAX_LENGHT + 1;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length; index += 1) {
    string[index] = 'a';
  }
  string[length] = '\0';

  struct Rope *rope, *clone, *placeholder;
  int          result;

  rope        = rope_from_string(string);
  placeholder = rope_new();
  clone       = placeholder;

  // When allocating the new node clone.
  WITH_NTH_FAILING_MALLOC(1,
    result = rope_clone(rope, &clone);

    ASSERT_MALLOC_CALLS(1);

    assert(result == 2);
    assert(clone  != rope);
    assert(clone  == placeholder);
  );

  // When allocating the new left node.
  WITH_NTH_FAILING_MALLOC(2,
    result = rope_clone(rope, &clone);

    ASSERT_MALLOC_CALLS(2);

    assert(result == 2);
    assert(clone  != rope);
    assert(clone  == placeholder);
  );

  // When allocating the new text.
  WITH_NTH_FAILING_MALLOC(3,
    result = rope_clone(rope, &clone);

    ASSERT_MALLOC_CALLS(3);

    assert(result == 2);
    assert(clone  != rope);
    assert(clone  == placeholder);
  );

  // When allocating the new right node.
  WITH_NTH_FAILING_MALLOC(7,
    result = rope_clone(rope, &clone);

    ASSERT_MALLOC_CALLS(7);

    assert(result == 2);
    assert(clone  != rope);
    assert(clone  == placeholder);
  );

  free(string);
  rope_free(placeholder);
  rope_free(rope);
}

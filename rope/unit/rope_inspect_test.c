//-------------------------------------------------------------------------------
// `rope_inspect` tests.
//-------------------------------------------------------------------------------

void test_rope_inspect_displays_an_empty_rope_structure__body() {
  struct Rope *rope = rope_new();

  int result = rope_inspect(rope);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_inspect_displays_an_empty_rope_structure() {
  char *result = capture_stdout(
    test_rope_inspect_displays_an_empty_rope_structure__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 0\n"
    "  text:   \"\"\n"
    "  parent: NULL\n"
    "  left:   NULL\n"
    "  right:  NULL>\n";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_displays_a_root_rope_node__body() {
  struct Rope *rope = rope_new();
  char        *string = malloc(11);

  memcpy(string, "Hello World", 11);
  rope->text = string;
  rope->length = 11;

  int result = rope_inspect(rope);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_inspect_displays_a_root_rope_node() {
  char *result = capture_stdout(
    test_rope_inspect_displays_a_root_rope_node__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 11\n"
    "  text:   \"Hello World\"\n"
    "  parent: NULL\n"
    "  left:   NULL\n"
    "  right:  NULL>\n";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_displays_valid_rope_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope__inspect_with_direction(left_child, 0, parent, -1);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 0, parent, 1);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_displays_valid_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_displays_valid_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 10\n"
    "  text:   \"Left Child\"\n"
    "  parent: Valid (I am the LEFT child)\n"
    "  left:   NULL\n"
    "  right:  NULL>\n"
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 11\n"
    "  text:   \"Right Child\"\n"
    "  parent: Valid (I am the RIGHT child)\n"
    "  left:   NULL\n"
    "  right:  NULL>\n";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_displays_mismatched_rope_children__body() {
  struct Rope *non_parent, *parent, *left_child, *right_child;
  int   result;
  char *string;

  non_parent = rope_new();
  string = malloc(10);
  memcpy(string, "Non Parent", 10);
  non_parent->text = string;
  non_parent->length = 10;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = non_parent;
  right_child->parent = non_parent;

  result = rope__inspect_with_direction(left_child, 0, parent, -1);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 0, parent, 1);
  assert(result == 0);

  rope_free(parent);
  rope_free(non_parent);
}

void test_rope_inspect_displays_mismatched_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_displays_mismatched_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 10\n"
    "  text:   \"Left Child\"\n"
    "  parent: Missmatch (I am the LEFT child)\n"
    "  left:   NULL\n"
    "  right:  NULL>\n"
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 11\n"
    "  text:   \"Right Child\"\n"
    "  parent: Missmatch (I am the RIGHT child)\n"
    "  left:   NULL\n"
    "  right:  NULL>\n";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_displays_corrupt_rope_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope__inspect_with_direction(left_child, 0, parent, 0);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 0, parent, 42);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_displays_corrupt_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_displays_corrupt_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 10\n"
    "  text:   \"Left Child\"\n"
    "  parent: Corrupt direction: 0\n"
    "  left:   NULL\n"
    "  right:  NULL>\n"
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 11\n"
    "  text:   \"Right Child\"\n"
    "  parent: Corrupt direction: 42\n"
    "  left:   NULL\n"
    "  right:  NULL>\n";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_displays_a_small_rope_with_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope_inspect(parent);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_displays_a_small_rope_with_children() {
  char *result = capture_stdout(
    test_rope_inspect_displays_a_small_rope_with_children__body
  );

  char *expected = \
    "<#Rope\n"
    "  height: 0\n"
    "  weight: 6\n"
    "  text:   \"Parent\"\n"
    "  parent: NULL\n"
    "  left:   <#Rope\n"
    "            height: 0\n"
    "            weight: 10\n"
    "            text:   \"Left Child\"\n"
    "            parent: Valid (I am the LEFT child)\n"
    "            left:   NULL\n"
    "            right:  NULL>\n"
    "  right:  <#Rope\n"
    "            height: 0\n"
    "            weight: 11\n"
    "            text:   \"Right Child\"\n"
    "            parent: Valid (I am the RIGHT child)\n"
    "            left:   NULL\n"
    "            right:  NULL>>\n";

  assert_strings_are_equal(result, expected);
}

//-------------------------------------------------------------------------------
// `rope_inspect_internal` tests.
//-------------------------------------------------------------------------------

void test_rope_inspect_internal_displays_an_empty_rope_structure__body() {
  struct Rope *rope = rope_new();

  int result = rope_inspect_internal(rope, 8);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_inspect_internal_displays_an_empty_rope_structure() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_an_empty_rope_structure__body
  );

  char *expected = \
    "<#Rope\n"
    "          height: 0\n"
    "          weight: 0\n"
    "          text:   \"\"\n"
    "          parent: NULL\n"
    "          left:   NULL\n"
    "          right:  NULL>";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_internal_displays_a_root_rope_node__body() {
  struct Rope *rope;
  char        *string;

  rope = rope_new();
  string = malloc(11);
  memcpy(string, "Hello World", 11);
  rope->text = string;
  rope->length = 11;

  int result = rope_inspect_internal(rope, 9);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_inspect_internal_displays_a_root_rope_node() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_a_root_rope_node__body
  );

  char *expected = \
    "<#Rope\n"
    "           height: 0\n"
    "           weight: 11\n"
    "           text:   \"Hello World\"\n"
    "           parent: NULL\n"
    "           left:   NULL\n"
    "           right:  NULL>";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_internal_displays_valid_rope_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope__inspect_with_direction(left_child, 4, parent, -1);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 5, parent, 1);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_internal_displays_valid_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_valid_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "      height: 0\n"
    "      weight: 10\n"
    "      text:   \"Left Child\"\n"
    "      parent: Valid (I am the LEFT child)\n"
    "      left:   NULL\n"
    "      right:  NULL>"
    "<#Rope\n"
    "       height: 0\n"
    "       weight: 11\n"
    "       text:   \"Right Child\"\n"
    "       parent: Valid (I am the RIGHT child)\n"
    "       left:   NULL\n"
    "       right:  NULL>";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_internal_displays_mismatched_rope_children__body() {
  struct Rope *non_parent, *parent, *left_child, *right_child;
  int   result;
  char *string;

  non_parent = rope_new();
  string = malloc(10);
  memcpy(string, "Non Parent", 10);
  non_parent->text = string;
  non_parent->length = 10;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = non_parent;
  right_child->parent = non_parent;

  result = rope__inspect_with_direction(left_child, 4, parent, -1);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 5, parent, 1);
  assert(result == 0);

  rope_free(parent);
  rope_free(non_parent);
}

void test_rope_inspect_internal_displays_mismatched_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_mismatched_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "      height: 0\n"
    "      weight: 10\n"
    "      text:   \"Left Child\"\n"
    "      parent: Missmatch (I am the LEFT child)\n"
    "      left:   NULL\n"
    "      right:  NULL>"
    "<#Rope\n"
    "       height: 0\n"
    "       weight: 11\n"
    "       text:   \"Right Child\"\n"
    "       parent: Missmatch (I am the RIGHT child)\n"
    "       left:   NULL\n"
    "       right:  NULL>";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_internal_displays_corrupt_rope_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope__inspect_with_direction(left_child, 4, parent, 0);
  assert(result == 0);
  result = rope__inspect_with_direction(right_child, 5, parent, 42);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_internal_displays_corrupt_rope_children() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_corrupt_rope_children__body
  );

  char *expected = \
    "<#Rope\n"
    "      height: 0\n"
    "      weight: 10\n"
    "      text:   \"Left Child\"\n"
    "      parent: Corrupt direction: 0\n"
    "      left:   NULL\n"
    "      right:  NULL>"
    "<#Rope\n"
    "       height: 0\n"
    "       weight: 11\n"
    "       text:   \"Right Child\"\n"
    "       parent: Corrupt direction: 42\n"
    "       left:   NULL\n"
    "       right:  NULL>";

  assert_strings_are_equal(result, expected);
}

void test_rope_inspect_internal_displays_a_small_rope_with_children__body() {
  struct Rope *parent, *left_child, *right_child;
  int   result;
  char *string;

  parent = rope_new();
  string = malloc(6);
  memcpy(string, "Parent", 6);
  parent->text = string;
  parent->length = 6;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  right_child = rope_new();
  string      = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->left  = left_child;
  parent->right = right_child;

  left_child->parent  = parent;
  right_child->parent = parent;

  result = rope_inspect_internal(parent, 3);
  assert(result == 0);

  rope_free(parent);
}

void test_rope_inspect_internal_displays_a_small_rope_with_children() {
  char *result = capture_stdout(
    test_rope_inspect_internal_displays_a_small_rope_with_children__body
  );

  char *expected = \
    "<#Rope\n"
    "     height: 0\n"
    "     weight: 6\n"
    "     text:   \"Parent\"\n"
    "     parent: NULL\n"
    "     left:   <#Rope\n"
    "               height: 0\n"
    "               weight: 10\n"
    "               text:   \"Left Child\"\n"
    "               parent: Valid (I am the LEFT child)\n"
    "               left:   NULL\n"
    "               right:  NULL>\n"
    "     right:  <#Rope\n"
    "               height: 0\n"
    "               weight: 11\n"
    "               text:   \"Right Child\"\n"
    "               parent: Valid (I am the RIGHT child)\n"
    "               left:   NULL\n"
    "               right:  NULL>>";

  assert_strings_are_equal(result, expected);
}

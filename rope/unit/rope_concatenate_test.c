void test_rope_concatenate_for_null_ropes() {
  struct Rope *rope, *result;
  rope = rope_new();

  result = rope_concatenate(NULL, NULL);
  assert(result == NULL);

  result = rope_concatenate(rope, NULL);
  assert(result == rope);

  result = rope_concatenate(NULL, rope);
  assert(result == rope);
}

// Concatenates the following ropes:
//
//    .-[3]~    .-[2]~        .-[5]~
//   (3)     + (2)     =   .-(3)-.
//  "AAA"      "BB"       (3)   (2)
//                       "AAA"  "BB"
//
void test_rope_concatenate_for_two_ropes() {
  struct Rope *first, *second, *result;

  first  = rope_from_string("AAA");
  second = rope_from_string("BB");
  result = NULL;

  result = rope_concatenate(first, second);
  first  = NULL;
  second = NULL;

  assert(result != NULL);
  assert(result->length == 5);

  char *string = rope_to_string(result);
  assert_strings_are_equal("AAABB", string);

  free(string);
  rope_free(result);
}

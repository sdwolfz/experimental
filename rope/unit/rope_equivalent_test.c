//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

void test_rope_equivalent_for_null_ropes() {
  struct Rope *rope;
  int          result;

  result = rope_equivalent(NULL, NULL);
  assert(result == 0);

  rope = rope_new();

  result = rope_equivalent(rope, NULL);
  assert(result == 0);

  result = rope_equivalent(NULL, rope);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_equivalent_for_two_small_ropes() {
  struct Rope *first, *second, *third;
  int          result;

  first  = rope_from_string("Hello");
  second = rope_from_string("World");
  third  = rope_from_string("World");

  result = rope_equivalent(first, second);
  assert(result == 0);

  result = rope_equivalent(second, second);
  assert(result == 1);

  result = rope_equivalent(second, third);
  assert(result == 1);
}

void test_rope_equivalent_for_two_deep_ropes() {
  struct Rope *first, *second, *third;
  int          result;

  first  = test_helper_build_balanced_deep_rope();
  second = test_helper_build_balanced_deep_rope();
  third  = test_helper_build_unbalanced_right_right_rope();

  result = rope_equivalent(first, first);
  assert(result == 1);

  result = rope_equivalent(first, second);
  assert(result == 1);

  result = rope_equivalent(second, third);
  assert(result == 0);
}

//------------------------------------------------------------------------------
// Helpers
//------------------------------------------------------------------------------

// Builds the following rope:
//
//                 .----[20]~
//       .-------(11)------.
//   .--(3)--.         .--(5)-.
//  (3)     (8)       (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"
//
struct Rope *
test_rope_helper_build_deep_rope() {
  char        *string;
  struct Rope *root, *grandparent, *left_parent, *right_parent,
              *left_left, *left_right, *right_left, *right_right;

  root         = rope__new_empty_node();
  grandparent  = rope__new_empty_node();
  left_parent  = rope__new_empty_node();
  right_parent = rope__new_empty_node();
  left_left    = rope__new_empty_node();
  left_right   = rope__new_empty_node();
  right_left   = rope__new_empty_node();
  right_right  = rope__new_empty_node();

  root->length         = 20;
  grandparent->length  = 11;
  left_parent->length  = 3;
  right_parent->length = 5;
  left_left->length    = 3;
  left_right->length   = 8;
  right_left->length   = 5;
  right_right->length  = 4;

  string = malloc(left_left->length);
  memcpy(string, "AAA", left_left->length);
  left_left->text = string;

  string = malloc(left_right->length);
  memcpy(string, "BBBBBCCC", left_right->length);
  left_right->text = string;

  string = malloc(right_left->length);
  memcpy(string, "DDDDD", right_left->length);
  right_left->text = string;

  string = malloc(right_right->length);
  memcpy(string, "EEEE", right_right->length);
  right_right->text = string;

  root->left          = grandparent;
  grandparent->left   = left_parent;
  grandparent->right  = right_parent;
  left_parent->left   = left_left;
  left_parent->right  = left_right;
  right_parent->left  = right_left;
  right_parent->right = right_right;

  grandparent->parent  = root;
  left_parent->parent  = grandparent;
  right_parent->parent = grandparent;
  left_left->parent    = left_parent;
  left_right->parent   = left_parent;
  right_left->parent   = right_parent;
  right_right->parent  = right_parent;

  return root;
}

// Splits the following rope at index.
//
//                 .----[20]~               .-[8]~           .-[12]~
//       .-------(11)------.            .--(3)-.        .---(3)--.
//   .--(3)--.         .--(5)-.    =>  (3)    (5)   +  (3)     .--(5)-.
//  (3)     (8)       (5)    (4)      "AAA" "BBBBB"   "CCC"   (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"                          "DDDDD" "EEEE"
//
void
test_rope_helper_assert_split_at_index(
  size_t  index,
  char   *expected_first,
  char   *expected_second
) {
  int          result;
  char        *string;
  struct Rope *root, *first, *second;

  root = test_rope_helper_build_deep_rope();
  first  = NULL;
  second = NULL;

  result = rope_split(root, index, &first, &second);
  root = NULL;

  assert(result == 0);

  string = rope_to_string(first);
  assert_strings_are_equal(string, expected_first);

  string = rope_to_string(second);
  assert_strings_are_equal(string, expected_second);

  rope_free(first);
  rope_free(second);
}

void
test_rope_helper_assert_malloc_faliure_on_deep_rope(
  unsigned int nth,
  unsigned int calls
) {
  int          result;
  struct Rope *root, *first, *second;

  root = test_rope_helper_build_deep_rope();
  first  = NULL;
  second = NULL;

  WITH_NTH_FAILING_MALLOC(nth,
    result = rope_split(root, 7, &first, &second);

    ASSERT_MALLOC_CALLS(calls);

    assert(result == 3);
  );

  rope_free(second);
  rope_free(first);
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

void test_rope_split_for_a_null_rope() {
  int result = rope_split(NULL, 1, NULL, NULL);

  assert(result == 1);
}

void test_rope_split_for_an_index_out_of_bounds() {
  struct Rope *rope = rope__new_empty_node();
  int          result;

  result = rope_split(rope, -1, NULL, NULL);
  assert(result == 2);

  result = rope_split(rope, 1, NULL, NULL);
  assert(result == 2);

  rope_free(rope);
}

// Splitting a small rope:
//
//       .-[11]~        .-[7]~      .-[4]~
//     (11)      =>    (7)     +   (4)
// "Hello World"    "Hello W"    "orld"
//
void test_rope_split_for_a_small_rope() {
  struct Rope *rope, *first, *second;
  int          result;

  rope   = rope_from_string("Hello World");
  first  = NULL;
  second = NULL;

  result = rope_split(rope, 7, &first, &second);
  rope = NULL;

  assert(result == 0);
  assert_strings_are_equal(rope_to_string(first),  "Hello W");
  assert_strings_are_equal(rope_to_string(second), "orld");

  rope_free(second);
  rope_free(first);
}

// Splitting a deep rope:
//
//                 .----[20]~               .-[8]~           .-[12]~
//       .-------(11)------.            .--(3)-.        .---(3)--.
//   .--(3)--.         .--(5)-.    =>  (3)    (5)   +  (3)     .--(5)-.
//  (3)     (8)       (5)    (4)      "AAA" "BBBBB"   "CCC"   (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"                          "DDDDD" "EEEE"
//
void test_rope_split_for_a_deep_rope() {
  // Split at the beginning of the first child.
  test_rope_helper_assert_split_at_index(0, "", "AAABBBBBCCCDDDDDEEEE");
  // Split inside the first child.
  test_rope_helper_assert_split_at_index(1, "A", "AABBBBBCCCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(2, "AA", "ABBBBBCCCDDDDDEEEE");
  // Split at the end of the first child.
  test_rope_helper_assert_split_at_index(3, "AAA", "BBBBBCCCDDDDDEEEE");

  // Split at the beginning of the second child.
  test_rope_helper_assert_split_at_index(4, "AAAB", "BBBBCCCDDDDDEEEE");
  // Split inside the second child.
  test_rope_helper_assert_split_at_index(5, "AAABB", "BBBCCCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(6, "AAABBB", "BBCCCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(7, "AAABBBB", "BCCCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(8, "AAABBBBB", "CCCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(9, "AAABBBBBC", "CCDDDDDEEEE");
  test_rope_helper_assert_split_at_index(10, "AAABBBBBCC", "CDDDDDEEEE");
  // Split at the end of the second child.
  test_rope_helper_assert_split_at_index(11, "AAABBBBBCCC", "DDDDDEEEE");

  // Split at the beginning of the third child.
  test_rope_helper_assert_split_at_index(12, "AAABBBBBCCCD", "DDDDEEEE");
  // Split inside the third child.
  test_rope_helper_assert_split_at_index(13, "AAABBBBBCCCDD", "DDDEEEE");
  test_rope_helper_assert_split_at_index(14, "AAABBBBBCCCDDD", "DDEEEE");
  test_rope_helper_assert_split_at_index(15, "AAABBBBBCCCDDDD", "DEEEE");
  // Split at the end of the third child.
  test_rope_helper_assert_split_at_index(16, "AAABBBBBCCCDDDDD", "EEEE");

  // Split at the beginning of the fourth child.
  test_rope_helper_assert_split_at_index(17, "AAABBBBBCCCDDDDDE", "EEE");
  // Split inside the fourth child.
  test_rope_helper_assert_split_at_index(18, "AAABBBBBCCCDDDDDEE", "EE");
  test_rope_helper_assert_split_at_index(19, "AAABBBBBCCCDDDDDEEE", "E");
  // Split at the end of the fourth child.
  test_rope_helper_assert_split_at_index(20, "AAABBBBBCCCDDDDDEEEE", "");
}

void test_rope_split_handles_malloc_faliure_on_small_rope() {
  struct Rope *rope, *first, *second;
  int          result;

  rope   = rope_from_string("Hello World");
  first  = NULL;
  second = NULL;

  WITH_NTH_FAILING_MALLOC(3,
    result = rope_split(rope, 6, &first, &second);

    ASSERT_MALLOC_CALLS(3);

    assert(result == 3);
  );

  rope_free(second);
  rope_free(first);
}

void test_rope_split_handles_malloc_faliure_on_deep_rope() {
  // When spliting leaf node, allocating right string.
  test_rope_helper_assert_malloc_faliure_on_deep_rope(1, 1);

  // When spliting leaf node, allocating the node for the string.
  test_rope_helper_assert_malloc_faliure_on_deep_rope(2, 2);

  // When not abble to reuse a left node as internediary for the right rope.
  test_rope_helper_assert_malloc_faliure_on_deep_rope(3, 3);
}

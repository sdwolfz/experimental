void test_rope_index_for_an_empty_rope_structure() {
  struct Rope *rope   = rope_new();
  int          result = rope_index(rope, 1);

  assert(result == -1);
}

void test_rope_index_for_a_small_rope() {
  struct Rope *rope   = rope_from_string("Hello World");
  char         result = rope_index(rope, 1);

  assert(result == 'e');
}

void test_rope_index_for_the_left_child() {
  struct Rope *root, *parent, *left_child, *right_child;
  char        *string;

  root = rope_new();
  root->length = 21;

  parent = rope_new();
  parent->length = 10;

  root->left = parent;
  parent->parent = root;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  parent->left       = left_child;
  left_child->parent = parent;

  right_child = rope_new();
  string     = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->right       = right_child;
  right_child->parent = parent;

  char result = rope_index(parent, 7);
  assert(result == 'i');
}

void test_rope_index_for_the_right_child() {
  struct Rope *root, *parent, *left_child, *right_child;
  char        *string;

  root = rope_new();
  root->length = 21;

  parent = rope_new();
  parent->length = 10;

  root->left = parent;
  parent->parent = root;

  left_child = rope_new();
  string     = malloc(10);
  memcpy(string, "Left Child", 10);
  left_child->text   = string;
  left_child->length = 10;

  parent->left       = left_child;
  left_child->parent = parent;

  right_child = rope_new();
  string     = malloc(11);
  memcpy(string, "Right Child", 11);
  right_child->text   = string;
  right_child->length = 11;

  parent->right       = right_child;
  right_child->parent = parent;

  char result = rope_index(parent, 13);
  assert(result == 'h');
}

// Testing `rope_new()`

// A rope should call malloc twice:
//   - once for the root node
//   - once for the text node

void test_rope_new_creates_a_new_rope() {
  WITH_STUBBED_MALLOC(
    struct Rope *rope = rope_new();

    ASSERT_MALLOC_CALLS(2);
    assert(rope != NULL);

    assert(rope->text != NULL);
    ASSERT_MALLOC_SIZE(ROPE_NODE_TEXT_MAX_LENGHT);

    assert(rope->height == 0);
    assert(rope->length == 0);
    assert(rope->parent == NULL);
    assert(rope->left   == NULL);
    assert(rope->right  == NULL);

    rope_free(rope);
  );
}

// A rope calls free if the allocation fo the text node fails, removing the
// newly allocated root node from memory.

void test_rope_new_handles_malloc_faliure() {
  WITH_NTH_FAILING_MALLOC(1,
    struct Rope *rope = rope_new();

    ASSERT_MALLOC_CALLS(1);

    assert(rope == NULL);
  );

  WITH_STUBBED_FREE(
    WITH_NTH_FAILING_MALLOC(2,
      struct Rope *rope = rope_new();

      ASSERT_MALLOC_CALLS(2);
      ASSERT_FREE_CALLS(1);

      assert(rope == NULL);
    );
  );
}

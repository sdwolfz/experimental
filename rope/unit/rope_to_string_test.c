void test_rope_to_string_for_a_null_rope() {
  char *string = rope_to_string(NULL);

  assert(string == NULL);
}

void test_rope_to_string_for_an_empty_rope() {
  struct Rope *rope   = rope_new();
  char        *string = rope_to_string(rope);
  size_t       length = strlen(string);

  assert(length == 0);
  assert_strings_are_equal(string, "");
}

void test_rope_to_string_when_malloc_fails() {
  char        *text = "Hello World";
  struct Rope *rope = rope_from_string(text);

  WITH_FAILING_MALLOC(
    char *string = rope_to_string(rope);

    assert(string == NULL);
  );

  rope_free(rope);
}

void test_rope_to_string_for_a_small_rope() {
  char        *text        = "Hello World";
  size_t       text_length = strlen(text);
  struct Rope *rope        = rope_from_string(text);

  char   *string        = rope_to_string(rope);
  size_t  string_length = strlen(string);

  assert(text_length == string_length);
  assert_strings_are_equal(text, string);

  free(string);
  rope_free(rope);
}

void test_rope_to_string_for_a_deep_rope() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  struct Rope *rope = rope_from_string(string);

  char   *result        = rope_to_string(rope);
  size_t  result_length = strlen(result);

  assert(result_length == length);
  assert_strings_are_equal(result, string);

  free(result);
  rope_free(rope);
  free(string);
}

//------------------------------------------------------------------------------
// Helpers
//------------------------------------------------------------------------------

// Builds the following rope:
//
//                 .----[20]~                         .----[21]~
//       .-------(11)------.                .-------(12)------.
//   .--(3)--.         .--(5)-.    =>    .---(3)--.         .--(5)-.
//  (3)     (8)       (5)    (4)        (3)      (9)       (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"     "AAA" "BBBBBxCCC" "DDDDD" "EEEE"
//
struct Rope *
test_rope_helper_build_deep_rope_for_insert() {
  char        *string;
  struct Rope *root, *grandparent, *left_parent, *right_parent,
              *left_left, *left_right, *right_left, *right_right;

  root         = rope__new_empty_node();
  grandparent  = rope__new_empty_node();
  left_parent  = rope__new_empty_node();
  right_parent = rope__new_empty_node();
  left_left    = rope__new_empty_node();
  left_right   = rope__new_empty_node();
  right_left   = rope__new_empty_node();
  right_right  = rope__new_empty_node();

  root->length         = 20;
  grandparent->length  = 11;
  left_parent->length  = 3;
  right_parent->length = 5;
  left_left->length    = 3;
  left_right->length   = 8;
  right_left->length   = 5;
  right_right->length  = 4;

  string = malloc(left_left->length);
  memcpy(string, "AAA", left_left->length);
  left_left->text = string;

  string = malloc(left_right->length);
  memcpy(string, "BBBBBCCC", left_right->length);
  left_right->text = string;

  string = malloc(right_left->length);
  memcpy(string, "DDDDD", right_left->length);
  right_left->text = string;

  string = malloc(right_right->length);
  memcpy(string, "EEEE", right_right->length);
  right_right->text = string;

  root->left          = grandparent;
  grandparent->left   = left_parent;
  grandparent->right  = right_parent;
  left_parent->left   = left_left;
  left_parent->right  = left_right;
  right_parent->left  = right_left;
  right_parent->right = right_right;

  grandparent->parent  = root;
  left_parent->parent  = grandparent;
  right_parent->parent = grandparent;
  left_left->parent    = left_parent;
  left_right->parent   = left_parent;
  right_left->parent   = right_parent;
  right_right->parent  = right_parent;

  return root;
}

// Inserts a string into a rope
//
//                 .----[20]~                         .----[21]~
//       .-------(11)------.                .-------(12)------.
//   .--(3)--.         .--(5)-.    =>    .---(3)--.         .--(5)-.
//  (3)     (8)       (5)    (4)        (3)      (9)       (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"     "AAA" "BBBBBxCCC" "DDDDD" "EEEE"
//
void
test_rope_helper_assert_insert_at_index(size_t index, char *expected) {
  int          result;
  char        *string;
  struct Rope *rope;

  rope = test_rope_helper_build_deep_rope_for_insert();

  result = rope_insert_string(rope, "x", index);
  assert(result == 0);

  string = rope_to_string(rope);
  assert_strings_are_equal(string, expected);

  free(string);
  rope_free(rope);
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------


void test_rope_insert_string_for_a_null_rope() {
  int result;

  result = rope_insert_string(NULL, NULL, 0);
  assert(result == 1);
}

void test_rope_insert_string_for_a_null_string() {
  struct Rope *rope;
  int          result;

  rope = rope__new_empty_node();

  result = rope_insert_string(rope, NULL, 0);
  assert(result == 2);

  rope_free(rope);
}

void test_rope_insert_string_for_an_out_of_bounds_index() {
  struct Rope *rope;
  char        *string;
  int          result;

  rope   = rope__new_empty_node();
  string = "Hello World";

  result = rope_insert_string(rope, string, 10000);
  assert(result == 3);

  rope_free(rope);
}

void test_rope_insert_string_for_a_small_rope() {
  struct Rope *rope;
  char        *string;
  int          result;

  rope   = rope_from_string("abcd");
  result = rope_insert_string(rope, "E", 0);
  string = rope_to_string(rope);
  assert(result == 0);
  assert_strings_are_equal(string, "Eabcd");
  rope_free(rope);

  rope   = rope_from_string("abcd");
  result = rope_insert_string(rope, "E", 1);
  string = rope_to_string(rope);
  assert(result == 0);
  assert_strings_are_equal(string, "aEbcd");
  rope_free(rope);

  rope   = rope_from_string("abcd");
  result = rope_insert_string(rope, "E", 2);
  string = rope_to_string(rope);
  assert(result == 0);
  assert_strings_are_equal(string, "abEcd");
  rope_free(rope);

  rope   = rope_from_string("abcd");
  result = rope_insert_string(rope, "E", 3);
  string = rope_to_string(rope);
  assert(result == 0);
  assert_strings_are_equal(string, "abcEd");
  rope_free(rope);

  rope   = rope_from_string("abcd");
  result = rope_insert_string(rope, "E", 4);
  string = rope_to_string(rope);
  assert(result == 0);
  assert_strings_are_equal(string, "abcdE");
  rope_free(rope);
}

// Inserting into a deep rope:
//
//                 .----[20]~                          .----[21]~
//       .-------(11)------.                 .-------(12)------.
//   .--(3)--.         .--(5)-.    =>   .---(3)--.         .--(5)-.
//  (3)     (8)       (5)    (4)       (3)      (9)       (5)    (4)
// "AAA" "BBBBBCCC" "DDDDD" "EEEE"    "AAA" "BBBBBxCCC" "DDDDD" "EEEE"
//
void test_rope_insert_string_for_a_deep_rope() {
  // Insert at the beginning of the first child.
  test_rope_helper_assert_insert_at_index(0,  "xAAABBBBBCCCDDDDDEEEE");
  // Insert inside the first child.
  test_rope_helper_assert_insert_at_index(1,  "AxAABBBBBCCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(2,  "AAxABBBBBCCCDDDDDEEEE");
  // Insert at the end of the first child.
  test_rope_helper_assert_insert_at_index(3,  "AAAxBBBBBCCCDDDDDEEEE");

  // Insert at the beginning of the second child.
  test_rope_helper_assert_insert_at_index(4,  "AAABxBBBBCCCDDDDDEEEE");
  // Insert inside the second child.
  test_rope_helper_assert_insert_at_index(5,  "AAABBxBBBCCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(6,  "AAABBBxBBCCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(7,  "AAABBBBxBCCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(8,  "AAABBBBBxCCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(9,  "AAABBBBBCxCCDDDDDEEEE");
  test_rope_helper_assert_insert_at_index(10, "AAABBBBBCCxCDDDDDEEEE");
  // Insert at the end of the second child.
  test_rope_helper_assert_insert_at_index(11, "AAABBBBBCCCxDDDDDEEEE");

  // Insert at the beginning of the third child.
  test_rope_helper_assert_insert_at_index(12, "AAABBBBBCCCDxDDDDEEEE");
  // Insert inside the third child.
  test_rope_helper_assert_insert_at_index(13, "AAABBBBBCCCDDxDDDEEEE");
  test_rope_helper_assert_insert_at_index(14, "AAABBBBBCCCDDDxDDEEEE");
  test_rope_helper_assert_insert_at_index(15, "AAABBBBBCCCDDDDxDEEEE");
  // Insert at the end of the third child.
  test_rope_helper_assert_insert_at_index(16, "AAABBBBBCCCDDDDDxEEEE");

  // Insert at the beginning of the fourth child.
  test_rope_helper_assert_insert_at_index(17, "AAABBBBBCCCDDDDDExEEE");
  // Insert inside the fourth child.
  test_rope_helper_assert_insert_at_index(18, "AAABBBBBCCCDDDDDEExEE");
  test_rope_helper_assert_insert_at_index(19, "AAABBBBBCCCDDDDDEEExE");
  // Insert at the end of the fourth child.
  test_rope_helper_assert_insert_at_index(20, "AAABBBBBCCCDDDDDEEEEx");
}

void test_rope_insert_string_when_allocating_new_nodes() {
  size_t  length = 2 * ROPE_NODE_TEXT_MAX_LENGHT + 1;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length; index += 1) {
    string[index] = 'a';
  }
  string[length] = '\0';

  struct Rope *rope;
  int          result;

  rope = rope_from_string(string);

  result = rope_insert_string(rope, "BBBBBB", 1000);
  assert(result == 0);

  char *output = rope_to_string(rope);
  assert(strlen(output) == length + 6);
  for(index = 0; index < 1000; index += 1) {
    assert(output[index] == 'a');
  }
  for(index = 1000; index < 1006; index += 1) {
    assert(output[index] == 'B');
  }
  for(index = 1006; index < length; index += 1) {
    assert(output[index] == 'a');
  }

  free(output);
  rope_free(rope);
  free(string);
}

void test_rope_insert_string_when_malloc_fails() {
  size_t  length = 2 * ROPE_NODE_TEXT_MAX_LENGHT + 1;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length; index += 1) {
    string[index] = 'a';
  }
  string[length] = '\0';

  struct Rope *rope;
  int          result;

  // For left node with old text.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(1,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(1);
  );
  rope_free(rope);

  // For right node with two children.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(2,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(2);
  );
  rope_free(rope);

  // Left child with the inserted text.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(3,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(3);
  );
  rope_free(rope);

  // Split string failing.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(4,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(4);
  );
  rope_free(rope);

  // Right child with the rest of the text.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(5,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(5);
  );
  rope_free(rope);

  // Text for right child.
  rope = rope_from_string(string);
  WITH_NTH_FAILING_MALLOC(6,
    result = rope_insert_string(rope, "BBBBBB", 1000);
    assert(result == 4);

    ASSERT_MALLOC_CALLS(6);
  );
  rope_free(rope);

  free(string);
}

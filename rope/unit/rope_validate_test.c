void test_rope_validate_for_a_null_rope() {
  int result = rope_validate(NULL);

  assert(result == 1);
}

void test_rope_validate_for_an_empty_rope() {
  struct Rope *rope = rope_new();
  int result = rope_validate(rope);

  assert(result == 0);
}

void test_rope_validate_for_a_small_rope() {
  struct Rope *rope = rope_from_string("Hello World");

  rope_validate(rope);

  /* int result = rope_validate(rope); */
  /* assert(result == 0); */
}

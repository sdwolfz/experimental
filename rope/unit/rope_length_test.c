void test_rope_length_for_a_null_rope() {
  size_t length = rope_length(NULL);

  assert(length == 0);
}

void test_rope_length_for_a_small_rope() {
  struct Rope *rope   = rope_from_string("Hello World");
  size_t       length = rope_length(rope);

  assert(length == 11);

  rope_free(rope);
}

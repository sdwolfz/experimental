void test_rope_from_string_for_a_null_string() {
  struct Rope *rope = rope_from_string(NULL);

  assert(rope == NULL);
}

void test_rope_from_string_for_an_empty_string() {
  char        *string = "";
  struct Rope *rope   = rope_from_string(string);

  char *result = rope_to_string(rope);
  assert_strings_are_equal(string, result);

  free(result);
}

void test_rope_from_string_when_malloc_fails() {
  WITH_FAILING_MALLOC(
    char        *string = "Hello World";
    struct Rope *rope   = rope_from_string(string);

    assert(rope == NULL);
  );
}

void test_rope_from_string_when_splitting_fails_on_a_small_rope() {
  WITH_NTH_FAILING_MALLOC(2,
    char        *string = "Hello World";
    struct Rope *rope   = rope_from_string(string);

    assert(rope == NULL);
  );
}

void test_rope_from_string_when_splitting_fails_on_the_left_node_of_a_deep_rope() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  WITH_NTH_FAILING_MALLOC(2,
    struct Rope *rope = rope_from_string(string);

    assert(rope == NULL);
  );

  free(string);
}

void test_rope_from_string_when_splitting_fails_on_the_right_node_of_a_deep_rope() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  WITH_NTH_FAILING_MALLOC(3,
    struct Rope *rope = rope_from_string(string);

    assert(rope == NULL);
  );

  free(string);
}

void test_rope_from_string_when_splitting_fails_on_the_left_string_of_a_deep_rope() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  WITH_NTH_FAILING_MALLOC(4,
    struct Rope *rope = rope_from_string(string);

    assert(rope == NULL);
  );

  free(string);
}
void test_rope_from_string_when_splitting_fails_on_the_right_string_of_a_deep_rope() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  WITH_NTH_FAILING_MALLOC(5,
    struct Rope *rope = rope_from_string(string);

    assert(rope == NULL);
  );

  free(string);
}

void test_rope_from_string_when_root_node_fails() {
  WITH_NTH_FAILING_MALLOC(3,
    char        *string = "Hello World";
    struct Rope *rope   = rope_from_string(string);

    assert(rope == NULL);
  );
}

void test_rope_from_string_for_a_small_string() {
  char         *string = "Hello World";
  unsigned int  length = strlen(string);
  struct Rope  *rope   = rope_from_string(string);

  assert(rope->length == length);

  char *result = rope_to_string(rope);
  assert_strings_are_equal(string, result);

  free(result);
  rope_free(rope);
}

void test_rope_from_string_for_a_large_string() {
  size_t  length = 2000;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length / 2; index += 1) {
    string[index] = 'a';
    string[index + length / 2] = 'b';
  }
  string[length] = '\0';

  struct Rope *rope = rope_from_string(string);

  char *result = rope_to_string(rope);
  assert_strings_are_equal(string, result);

  free(result);
  rope_free(rope);
  free(string);
}

void test_rope_from_string_for_a_large_unbalanced_string() {
  size_t  length = 2 * ROPE_NODE_TEXT_MAX_LENGHT + 1;
  char   *string = malloc(length + 1);

  size_t index;
  for(index = 0; index < length; index += 1) {
    string[index] = 'a';
  }
  string[length] = '\0';

  struct Rope *rope = rope_from_string(string);

  char *result = rope_to_string(rope);
  assert_strings_are_equal(string, result);

  free(result);
  rope_free(rope);
  free(string);
}

void test_rope_identical_for_null_ropes() {
  struct Rope *rope;
  int          result;

  result = rope_identical(NULL, NULL);
  assert(result == 0);

  rope = rope_new();

  result = rope_identical(rope, NULL);
  assert(result == 0);

  result = rope_identical(NULL, rope);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_identical_for_two_ropes() {
  struct Rope *first, *second;
  int          result;

  first = rope_new();
  second = rope_new();

  result = rope_identical(first, second);
  assert(result == 0);

  result = rope_identical(first, first);
  assert(result == 1);

  rope_free(second);
  rope_free(first);
}

void test_rope_equal_for_null_ropes() {
  struct Rope *rope;
  int          result;

  result = rope_equal(NULL, NULL);
  assert(result == 0);

  rope = rope_new();

  result = rope_equal(rope, NULL);
  assert(result == 0);

  result = rope_equal(NULL, rope);
  assert(result == 0);

  rope_free(rope);
}

void test_rope_equal_for_two_ropes() {
  struct Rope *first, *second, *third;
  int          result;

  first  = rope_from_string("Hello");
  second = rope_from_string("World");
  third  = rope_from_string("World");

  result = rope_equal(first, second);
  assert(result == 0);

  result = rope_equal(second, second);
  assert(result == 1);

  result = rope_equal(second, third);
  assert(result == 1);
}

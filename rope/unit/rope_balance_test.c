//------------------------------------------------------------------------------
// Small ropes
//------------------------------------------------------------------------------

// There is not much to test in this case.
// Just checking we don't segfault I guess...
void test_rope_balance_for_a_null_rope() {
  rope_balance(NULL);
}

//------------------------------------------------------------------------------
// Deep ropes
//------------------------------------------------------------------------------

// Performs the following transformation:
//
//             .-[R]~             .--[R]~
//         .--(A)-.          .---(B)---.
//     .--(B)-.  "4"  =>  .-(C)-.   .-(A)-.
//  .-(C)-.  "3"         "1"   "2" "3"   "4"
// "1"   "2"
//
void test_rope_balance_for_a_left_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_deep_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "1234");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//           .--[R]~             .--[R]~
//      .---(A)---.         .---(C)---.
//  .--(B)-.     "4" =>  .-(B)-.   .-(A)-.
// "1"  .-(C)-.         "1"   "2" "3"   "4"
//     "2"   "3"
//
void test_rope_balance_for_a_left_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_deep_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "1234");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//        .--[R]~                .--[R]~
//  .----(A)---.            .---(C)---.
// "1"     .--(B)-.  =>  .-(A)-.   .-(B)-.
//      .-(C)-.  "4"    "1"   "2" "3"   "4"
//     "2"   "3"
//
void test_rope_balance_for_a_right_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_deep_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "1234");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//      .-[R]~                    .--[R]~
//  .--(A)--.                .---(B)---.
// "1"  .--(B)--.     =>  .-(A)-.   .-(C)-.
//     "2"   .-(C)-.     "1"   "2" "3"   "4"
//          "3"   "4"
//
void test_rope_balance_for_a_right_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_deep_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "1234");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

//------------------------------------------------------------------------------
// Deeper ropes
//------------------------------------------------------------------------------

// Performs the following transformations:
//
//                  .-[R]~                   .--[R]~                .---[R]~
//              .--(A)-.               .----(A)----.         .-----(C)-----.
//          .--(B)-.  "5"  =>     .---(C)---.     "5" =>  .-(D)-.      .--(A)-.
//      .--(C)-.  "4"          .-(D)-.   .-(B)-.         "1"   "2"  .-(B)-.  "5"
//   .-(D)-.  "3"             "1"   "2" "3"   "4"                  "3"   "4"
//  "1"   "2"
//
void test_rope_balance_for_a_left_left_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_right_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_left_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformations:
//
//               .-[R]~                    .--[R]~                .---[R]~
//          .---(A)---.              .----(A)----.         .-----(D)-----.
//      .---(B)---.  "5" =>     .---(D)---.     "5" =>  .-(C)-.      .--(A)-.
//  .--(C)-.     "4"         .-(C)-.   .-(B)-.         "1"   "2"  .-(B)-.  "5"
// "1"  .-(D)-.             "1"   "2" "3"   "4"                  "3"   "4"
//     "2"   "3"
//
void test_rope_balance_for_a_left_left_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_right_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_left_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformations:
//
//              .--[R]~                    .--[R]~                .---[R]~
//        .----(A)----.              .----(A)----.         .-----(D)-----.
//  .----(B)---.     "5" =>     .---(D)---.     "5" =>  .-(B)-.      .--(A)-.
// "1"     .--(C)-.          .-(B)-.   .-(C)-.         "1"   "2"  .-(C)-.  "5"
//      .-(D)-.  "4"        "1"   "2" "3"   "4"                  "3"   "4"
//     "2"   "3"
//
void test_rope_balance_for_a_left_right_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_right_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_right_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformations:
//
//             .---[R]~                    .--[R]~                .---[R]~
//      .-----(A)-----.              .----(A)----.         .-----(C)-----.
//  .--(B)--.        "5" =>     .---(C)---.     "5" =>  .-(B)-.      .--(A)-.
// "1"  .--(C)-.             .-(B)-.   .-(D)-.         "1"   "2"  .-(D)-.  "5"
//     "2"  .-(D)-.         "1"   "2" "3"   "4"                  "3"   "4"
//         "3"   "4"
//
void test_rope_balance_for_a_left_right_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_right_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_left_right_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformations:
//
//          .---[R]~               .--[R]~                         .--[R]~
//  .------(A)-----.         .----(A)----.                  .-----(C)----.
// "1"         .--(B)-.  => "1"     .---(C)---.     =>  .--(A)-.      .-(B)-.
//         .--(C)-.  "5"         .-(D)-.   .-(B)-.     "1"  .-(D)-.  "4"   "5"
//      .-(D)-.  "4"            "2"   "3" "4"   "5"        "2"   "3"
//     "2"   "3"
//
void test_rope_balance_for_a_right_left_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_left_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_left_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//         .--[R]~                 .--[R]~                         .--[R]~
//  .-----(A)----.           .----(A)----.                  .-----(D)----.
// "1"      .---(B)---.  => "1"     .---(D)---.     =>  .--(A)-.      .-(B)-.
//      .--(C)-.     "5"         .-(C)-.   .-(B)-.     "1"  .-(C)-.  "4"   "5"
//     "2"  .-(D)-.             "2"   "3" "4"   "5"        "2"   "3"
//         "3"   "4"
//
void test_rope_balance_for_a_right_left_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_left_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_left_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//       .-[R]~                    .--[R]~                         .--[R]~
//  .---(A)---.              .----(A)----.                  .-----(D)----.
// "1"  .----(B)---.     => "1"     .---(D)---.     =>  .--(A)-.      .-(C)-.
//     "2"     .--(C)-.          .-(B)-.   .-(C)-.     "1"  .-(B)-.  "4"   "5"
//          .-(D)-.  "5"        "2"   "3" "4"   "5"        "2"   "3"
//         "3"   "4"
//
void test_rope_balance_for_a_right_right_left_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_left_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_right_left_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

// Performs the following transformation:
//
//      .-[R]~                     .--[R]~                         .--[R]~
//  .--(A)--.                .----(A)----.                  .-----(C)----.
// "1"  .--(B)--.        => "1"     .---(C)---.     =>  .--(A)-.      .-(D)-.
//     "2"  .--(C)-.             .-(B)-.   .-(D)-.     "1"  .-(B)-.  "4"   "5"
//         "3"  .-(D)-.         "2"   "3" "4"   "5"        "2"   "3"
//             "4"   "5"
//
void test_rope_balance_for_a_right_right_right_heavy_rope() {
  struct Rope *balanced_rope, *unbalanced_rope;
  char        *string;
  int          result;

  balanced_rope   = test_helper_build_balanced_left_heavy_deeper_rope();
  unbalanced_rope = test_helper_build_unbalanced_right_right_right_rope();

  rope_balance(unbalanced_rope);

  string = rope_to_string(unbalanced_rope);
  assert_strings_are_equal(string, "12345");

  result = rope_equivalent(balanced_rope, unbalanced_rope);
  assert(result == 1);

  free(string);
  rope_free(unbalanced_rope);
  rope_free(balanced_rope);
}

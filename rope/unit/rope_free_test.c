void test_rope_free_for_a_null_rope() {
  WITH_STUBBED_FREE(
    rope_free(NULL);

    ASSERT_FREE_CALLS(0);
  );
}

void test_rope_free_deallocates_a_rope() {
  struct Rope *rope = rope_new();

  WITH_STUBBED_FREE(
    rope_free(rope);

    ASSERT_FREE_CALLS(2);
  );
}

void test_rope_free_deallocates_a_deep_rope() {
  struct Rope *root, *left_child, *right_grandchild;

  root             = rope_new();
  left_child       = rope_new();
  right_grandchild = rope_new();

  left_child->right = right_grandchild;
  root->left        = left_child;

  WITH_STUBBED_FREE(
    rope_free(root);

    ASSERT_FREE_CALLS(6);
  );
}

#include "./test_macros.h"
#include "./test_utils.h"

#include "./src/rope.h"

#include "./test_helper.c"

#include "./unit/rope_balance_test.c"
#include "./unit/rope_clone_test.c"
#include "./unit/rope_concatenate_test.c"
#include "./unit/rope_equal_test.c"
#include "./unit/rope_equivalent_test.c"
#include "./unit/rope_free_test.c"
#include "./unit/rope_from_string_test.c"
#include "./unit/rope_identical_test.c"
#include "./unit/rope_index_test.c"
#include "./unit/rope_insert_string_test.c"
#include "./unit/rope_inspect_test.c"
#include "./unit/rope_length_test.c"
#include "./unit/rope_new_test.c"
#include "./unit/rope_split_test.c"
#include "./unit/rope_to_string_test.c"
#include "./unit/rope_validate_test.c"

int main() {
  common_test_setup();

  // ./unit/rope_balance_test.c
  test_rope_balance_for_a_null_rope();

  test_rope_balance_for_a_left_left_heavy_rope();
  test_rope_balance_for_a_left_right_heavy_rope();
  test_rope_balance_for_a_right_left_heavy_rope();
  test_rope_balance_for_a_right_right_heavy_rope();

  test_rope_balance_for_a_left_left_left_heavy_rope();
  test_rope_balance_for_a_left_left_right_heavy_rope();
  test_rope_balance_for_a_left_right_left_heavy_rope();
  test_rope_balance_for_a_left_right_right_heavy_rope();
  test_rope_balance_for_a_right_left_left_heavy_rope();
  test_rope_balance_for_a_right_left_right_heavy_rope();
  test_rope_balance_for_a_right_right_left_heavy_rope();
  test_rope_balance_for_a_right_right_right_heavy_rope();

  // ./unit/rope_clone_test.c
  test_rope_clone_for_a_null_rope();
  test_rope_clone_for_an_empty_rope();
  test_rope_clone_for_a_small_rope();
  test_rope_clone_for_a_deep_rope();
  test_rope_clone_when_malloc_fails();

  // ./unit/rope_concatenate_test.c
  test_rope_concatenate_for_null_ropes();
  test_rope_concatenate_for_two_ropes();

  // ./unit/rope_equal_test.c
  test_rope_equal_for_null_ropes();
  test_rope_equal_for_two_ropes();

  // ./unit/rope_equivalent_test.c
  test_rope_equivalent_for_null_ropes();
  test_rope_equivalent_for_two_small_ropes();
  test_rope_equivalent_for_two_deep_ropes();

  // ./unit/rope_free_test.c
  test_rope_free_for_a_null_rope();
  test_rope_free_deallocates_a_rope();
  test_rope_free_deallocates_a_deep_rope();

  // ./unit/rope_from_string_test.c
  test_rope_from_string_for_a_null_string();
  test_rope_from_string_for_an_empty_string();
  test_rope_from_string_when_malloc_fails();
  test_rope_from_string_when_splitting_fails_on_a_small_rope();
  test_rope_from_string_when_splitting_fails_on_the_left_node_of_a_deep_rope();
  test_rope_from_string_when_splitting_fails_on_the_right_node_of_a_deep_rope();
  test_rope_from_string_when_splitting_fails_on_the_left_string_of_a_deep_rope();
  test_rope_from_string_when_splitting_fails_on_the_right_string_of_a_deep_rope();
  test_rope_from_string_when_root_node_fails();
  test_rope_from_string_for_a_small_string();
  test_rope_from_string_for_a_large_string();
  test_rope_from_string_for_a_large_unbalanced_string();

  // ./unit/rope_identical_test.c
  test_rope_identical_for_null_ropes();
  test_rope_identical_for_two_ropes();

  // ./unit/rope_index_test.c
  test_rope_index_for_an_empty_rope_structure();
  test_rope_index_for_a_small_rope();
  test_rope_index_for_the_left_child();
  test_rope_index_for_the_right_child();

  // ./unit/rope_insert_string_test.c
  test_rope_insert_string_for_a_null_rope();
  test_rope_insert_string_for_a_null_string();
  test_rope_insert_string_for_an_out_of_bounds_index();
  test_rope_insert_string_for_a_small_rope();
  test_rope_insert_string_for_a_deep_rope();
  test_rope_insert_string_when_allocating_new_nodes();
  test_rope_insert_string_when_malloc_fails();

  // ./unit/rope_inspect_test.c
  test_rope_inspect_displays_an_empty_rope_structure();
  test_rope_inspect_displays_a_root_rope_node();
  test_rope_inspect_displays_valid_rope_children();
  test_rope_inspect_displays_mismatched_rope_children();
  test_rope_inspect_displays_corrupt_rope_children();
  test_rope_inspect_displays_a_small_rope_with_children();

  test_rope_inspect_internal_displays_an_empty_rope_structure();
  test_rope_inspect_internal_displays_a_root_rope_node();
  test_rope_inspect_internal_displays_valid_rope_children();
  test_rope_inspect_internal_displays_mismatched_rope_children();
  test_rope_inspect_internal_displays_corrupt_rope_children();
  test_rope_inspect_internal_displays_a_small_rope_with_children();

  // ./unit/rope_length_test.c
  test_rope_length_for_a_null_rope();
  test_rope_length_for_a_small_rope();

  // ./unit/rope_new_test.c
  test_rope_new_creates_a_new_rope();
  test_rope_new_handles_malloc_faliure();

  // ./unit/rope_split_test.c
  test_rope_split_for_a_null_rope();
  test_rope_split_for_an_index_out_of_bounds();
  test_rope_split_for_a_small_rope();
  test_rope_split_for_a_deep_rope();
  test_rope_split_handles_malloc_faliure_on_small_rope();
  test_rope_split_handles_malloc_faliure_on_deep_rope();

  // ./unit/rope_to_string_test.c
  test_rope_to_string_for_a_null_rope();
  test_rope_to_string_for_an_empty_rope();
  test_rope_to_string_when_malloc_fails();
  test_rope_to_string_for_a_small_rope();
  test_rope_to_string_for_a_deep_rope();

  // ./unit/rope_validate_test.c
  test_rope_validate_for_a_null_rope();
  test_rope_validate_for_an_empty_rope();
  test_rope_validate_for_a_small_rope();

  printf("%s", "All tests executed successfully!\n");
  return 0;
}

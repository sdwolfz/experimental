const repository = {
  '1': 'Website Redesign',
  '2': 'Marketing Campaign',
  '3': 'Client Meeting',
  '4': 'Project Alpha',
  '5': 'Code Review',
  '6': 'Sales Presentation',
  '7': 'User Testing',
  '8': 'Bug Fixing',
  '9': 'Product Launch'
};

const periods = [
  {
    period: 1,
    current: ['1', '2', '3', '4', '5']
  },
  {
    period: 2,
    completed: ['1'],
    failed: ['3'],
    current: ['2', '4', '5', '6']
  },
  {
    period: 3,
    completed: ['2', '1'],
    failed: ['3'],
    current: ['4', '5', '6', '7']
  },
  {
    period: 4,
    completed: ['5', '2', '1'],
    failed: ['4', '7', '3'],
    current: ['6', '8']
  },
  {
    period: 5,
    completed: ['6', '5', '2', '1'],
    failed: ['4', '7', '3'],
    current: ['8', '9']
  },
  {
    period: 6,
    completed: ['9', '6', '5', '2', '1'],
    failed: ['4', '7', '3'],
    current: ['8']
  }
];


// -----------------------------------------------------------------------------

const prevButton = document.getElementById('prevButton');
const nextButton = document.getElementById('nextButton');
const periodNumber = document.getElementById('periodNumber');
let currentPeriodIndex = 0;

prevButton.addEventListener('click', () => {
    if (currentPeriodIndex > 0) {
        currentPeriodIndex--;
        updatePeriod();
    }
});

nextButton.addEventListener('click', () => {
    if (currentPeriodIndex < periods.length - 1) {
        currentPeriodIndex++;
        updatePeriod();
    }
});

function updatePeriod() {
    const currentPeriod = periods[currentPeriodIndex];
    const previousPeriod = currentPeriodIndex > 0 ? periods[currentPeriodIndex - 1] : null;
    periodNumber.textContent = currentPeriod.period;

    const completedColumn = document.getElementById('completed');
    const failedColumn = document.getElementById('failed');
    const currentPeriodColumn = document.getElementById('currentPeriod');

    // Clear columns
    completedColumn.innerHTML = '';
    failedColumn.innerHTML = '';
    currentPeriodColumn.innerHTML = '';

    // Populate columns with tasks
    if (currentPeriod.completed) {
        currentPeriod.completed.forEach(taskId => {
            const card = createCard(repository[taskId], 'blue-border');
            card.classList.add('transparent-card');
            completedColumn.appendChild(card);
        });
    }

    if (currentPeriod.failed) {
        currentPeriod.failed.forEach(taskId => {
            const card = createCard(repository[taskId], 'red-border');
            card.classList.add('transparent-card');
            failedColumn.appendChild(card);
        });
    }

    const remainingTasks = new Set(currentPeriod.current);

    if (previousPeriod) {
        previousPeriod.current.forEach(taskId => {
            if (remainingTasks.has(taskId)) {
                remainingTasks.delete(taskId);
                const card = createCard(repository[taskId], 'orange-border');
                currentPeriodColumn.appendChild(card);
            }
        });
    }

    remainingTasks.forEach(taskId => {
        const card = createCard(repository[taskId], 'green-border');
        currentPeriodColumn.appendChild(card);
    });
}

function createCard(text, borderColorClass) {
    const card = document.createElement('div');
    card.className = `card ${borderColorClass}`;
    card.textContent = text;
    return card;
}

// Initialize the first period
updatePeriod();

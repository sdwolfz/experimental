#!/usr/bin/env ruby
# frozen_string_literal: true

require './regression.rb'

@fcf = [
  2000,
  2200,
  2420,
  2662,
  2928,
  3221,
  3543,
  3953,
  4327,
  4746
]
@years = 8
@discount = 0.12
@perpetuity = 0.03

@shares = 100_000
@safety = 0.3

def discounted_fcf(projected, discount)
  result = []

  projected.each_with_index do |value, index|
    discounted = value / (1 + discount)**(index + 1)

    result << discounted
  end

  result
end

def perpetuity_value(value, discount, perpetuity, year)
  pp year
  pp ((value * (1 + perpetuity)) / (discount - perpetuity)) / (1 + discount)**year
end

projected  = Regression.new.predict_next(@years, @fcf)
discounted = discounted_fcf(projected, @discount)

perpetuity_year = @fcf.size + @years + 1
perpetuity = perpetuity_value(projected.last, @discount, @perpetuity, perpetuity_year)
discounted << perpetuity

pp discounted

valuation = discounted.sum / @shares / (1 + @safety)

pp valuation

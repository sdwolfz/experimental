# frozen_string_literal: true

class Regression
  def linear(pairs)
    n = pairs.size

    sum_x = pairs.map { |x, _| x }.sum.to_f
    sum_y = pairs.map { |_, y| y }.sum.to_f

    sum_xx = pairs.map { |x, _| x * x }.sum.to_f
    sum_xy = pairs.map { |x, y| x * y }.sum.to_f

    base = (n * sum_xx - sum_x**2)
    a    = (n * sum_xy - sum_x * sum_y ) / base
    b    = (sum_y * sum_xx - sum_x * sum_xy) / base

    [a, b]
  end

  def predict_next(count, values)
    pairs = []
    values.each_with_index { |e, i| pairs << [i, e] }

    a, b = linear(pairs)
    n    = values.size

    result = []
    count.times { |i| result << a * (i + n) + b }
    result
  end
end

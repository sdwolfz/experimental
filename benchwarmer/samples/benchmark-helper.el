(add-to-list 'load-path ".")

(require 'benchwarmer)
(setq benchwarmer-report-directory "./samples/reports")

;; An example of a good, working benchamrk suite.
(benchwarmer-suite "working"
  (benchwarmer-run "+" 100000 (+ 1 2 3))
  (benchwarmer-run "-" 100000 (- 1 2 3))
  (benchwarmer-run "*" 100000 (* 1 2 3))
  (benchwarmer-run "/" 100000 (/ 1 2 3)))

;; A suite with a name and no benchmakrs is still a valid suite.
;; (benchwarmer-suite "empty")

;; Suites with wrong argument types produce an error before any suite is run.
;; You must fix the error before proceeding.
;;
;; (benchwarmer-suite :wrong-name)
;; NAME: Argument value :wrong-name is not a string.
;;
;; (benchwarmer-suite "wrong form"
;;   :wrong-form)
;; FORMS: Argument sub-element :wrong-form is not a list.
;;
;; A suite with a duplicate name is not allowed.
;;
;; (benchwarmer-suite "same")
;; (benchwarmer-suite "same")
;; ERROR: A suite named < "same" > already exists.

;; Suites with errors inside their benchamrks are valid.
;; They return the error message as a report instead of the benchmark result.
;;
;; (benchwarmer-suite "wrong benchmark name"
;;   (benchwarmer-run :wrong-benchmark-name 1 (+ 1 2 3)))
;;
;; (benchwarmer-suite "wrong benchmark times"
;;   (benchwarmer-run "name" :wrong-times (+ 1 2 3)))
;;
;; (benchwarmer-suite "wrong benchmark form"
;;   (benchwarmer-run "name" 1 :wrong-form))
;;
;; (benchwarmer-suite "error in benchmark form"
;;   (benchwarmer-run "+" 1 (error "benchmark form throws an error")))

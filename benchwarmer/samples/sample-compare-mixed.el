;; You need to load the benchmark helper yourself since compare scripts do not
;; rely on the benchmark runner to load it.
(load-file "samples/benchmark-helper.el")

;; An example of a good, working benchamrk comparison.
(benchwarmer-compare-mixed "math"
  (benchwarmer-run "old"   100000 (+ 1 2 3))
  (benchwarmer-run "new"   100000 (- 1 2 3))
  (benchwarmer-run "newer" 100000 (* 1 2 3)))

;; Comparisons with errors inside their benchamrks are valid.
;; They return the error message as a report instead of the benchmark result.
;;
;; (benchwarmer-compare-mixed "wrong benchmark name"
;;   (benchwarmer-run :wrong-benchmark-name 1 (+ 1 2 3))
;;   (benchwarmer-run "new"   1 (- 1 2 3))
;;   (benchwarmer-run "newer" 1 (* 1 2 3)))
;;
;; (benchwarmer-compare-mixed "wrong times"
;;   (benchwarmer-run "old" :wrong-times (+ 1 2 3))
;;   (benchwarmer-run "new"   1 (- 1 2 3))
;;   (benchwarmer-run "newer" 1 (* 1 2 3)))
;;
;; (benchwarmer-compare-mixed "wrong form"
;;   (benchwarmer-run "old"   1 :wrong-form)
;;   (benchwarmer-run "new"   1 (- 1 2 3))
;;   (benchwarmer-run "newer" 1 (* 1 2 3)))
;;
;; (benchwarmer-compare-mixed "wrong form"
;;   (benchwarmer-run "old"   1 (error "benchmark form throws an error"))
;;   (benchwarmer-run "new"   1 (- 1 2 3))
;;   (benchwarmer-run "newer" 1 (* 1 2 3)))

;;; benchwarmer.el --- benchmarking reports -*- lexical-binding: t -*-

;; Copyright (C) 2003-2017 Free Software Foundation, Inc.

;; Author: Codruț Constantin Gușoi <codrut.gusoi@gmail.com>
;; Keywords: lisp, extensions, benchmarking

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Generates benchmark reports and saves them in a file for later comparison.

;;; Code:

;;; Markdown Docs Commentary:
;;
;; Generates benchmark reports and saves them in a file for later comparison.

;;-------------------------------------------------------------------------------
;; Private Variables
;;-------------------------------------------------------------------------------

(defvar benchwarmer--registered-suites '()
  "A list of suites to run.")

(defvar benchwarmer--random-seed nil
  "The seed used to generate random numbers for shuffling the registered suites
list. If set to nil, no shuffling will be done.")

(defconst benchwarmer--output-types
  '(:compare
     :compare-with-first
     :compare-with-previous
     :compare-mixed
     :compare-before-and-after
     :compare-reports
     :compare-reports-with-first
     :compare-reports-with-previous
     :compare-reports-mixed)
  "List of available output types.")

(defun benchwarmer--initial-output-functions ()
  "Return a hash table with the initial output functions."
  (let ((table (make-hash-table :test 'equal)))
    (dolist (type benchwarmer--output-types)
      (puthash type 'pp table))
    table))

(defvar benchwarmer--output-functions (benchwarmer--initial-output-functions)
  "A table with the associated output functions for compare and report.")

;;-------------------------------------------------------------------------------
;; Public Variables
;;-------------------------------------------------------------------------------

(defvar benchwarmer-report-directory "./bench/reports"
  "Path to the directory in which benchmark reports will be saved.")

(defvar benchwarmer-report-name "benchmark"
  "Name of the file in which to save the benchmark report.")

;;-------------------------------------------------------------------------------
;; Private API
;;-------------------------------------------------------------------------------

;;; Markdown Docs Category: CLI Flags
;;
;;; Markdown Docs Element: --benchwarmer-after
;;
;; ```bash
;; emacs --script ./samples/sample-suite.el -- --benchwarmer-after
;; ```
;;
;;; Markdown Docs Element: --benchwarmer-before
;;
;; ```bash
;; emacs --script ./samples/sample-suite.el -- --benchwarmer-before
;; ```
;;
;;; Markdown Docs Element: --benchwarmer-directory
;;
;; ```bash
;; emacs --script ./samples/sample-suite.el -- --benchwarmer-directory ./bench/reports
;; ```
;;
;;; Markdown Docs Element: --benchwarmer-name
;;
;; ```bash
;; emacs --script ./samples/sample-suite.el -- --benchwarmer-benchmark
;; ```

(defun benchwarmer--set-report-vairables-from-cli ()
  "Consumes all CLI arguments to extract benchmwarmer specific parameters."
  (while argv
    (let ((option (pop argv)))
      (cond
        ((string= option "--benchwarmer-after")
          (setq benchwarmer-report-name "after"))
        ((string= option "--benchwarmer-before")
          (setq benchwarmer-report-name "before"))
        ((string= option "--benchwarmer-directory")
          (setq benchwarmer-report-directory (pop argv)))
        ((string= option "--benchwarmer-name")
          (setq benchwarmer-report-name (pop argv)))
        ((string= option "--benchwarmer-random")
          (unless benchwarmer--random-seed
            (setq benchwarmer--random-seed t)))
        ((string= option "--benchwarmer-seed")
          (setq benchwarmer--random-seed (pop argv)))))))

(defun benchwarmer--file-path (&optional name)
  "Return the file path for the benchmark report.

If a file `NAME' is given, use that instead of the default name."
  (concat
    (file-name-as-directory benchwarmer-report-directory)
    (if name
      name
      benchwarmer-report-name)))

(defun benchwarmer--validate-element (form element type &optional limit)
  (let ((base (format "FORM: < %S > is not a valid benchwarmer result, " form)))
    (cond
      ((equal type :list)
        (unless (listp element)
          (error (format (concat base "< %S > is not a list.") element)))
        (when limit
          (unless (= limit (length element))
            (error (format (concat base "< %S > is not a list of length %o.") element limit)))))
      ((equal type :string)
        (unless (stringp element)
          (error (format (concat base "< %S > is not a string.") element))))
      ((equal type :float)
        (unless (floatp element)
          (error (format (concat base "< %S > is not a float.") element))))
      ((equal type :integer)
        (unless (integerp element)
          (error (format (concat base "< %S > is not a integer.") element)))))))

(defun benchwarmer--validate-result (result)
  (benchwarmer--validate-element result result :list 2)
  (let ((benchmark-name (car result))
         (benchmark (nth 1 result)))
    (benchwarmer--validate-element result benchmark-name :string)
    (benchwarmer--validate-element result benchmark :list)
    (if (= 2 (length benchmark))
      (benchwarmer--validate-element result benchmark :error)
      (progn
        (benchwarmer--validate-element result benchmark :list 3)
        (let ((first (car benchmark))
               (second (nth 1 benchmark))
               (third (nth 2 benchmark)))
          (benchwarmer--validate-element result first :float)
          (benchwarmer--validate-element result second :integer)
          (benchwarmer--validate-element result third :float))))))

(defun benchwarmer--validate-element-with-path (element type path &optional limit)
  (let ((base "Report file %s is not a valid report, "))
    (cond
      ((equal type :list)
        (unless (listp element)
          (error (format (concat base "< %S > is not a list.") path element)))
        (when limit
          (unless (= limit (length element))
            (error (format (concat base "< %S > is not a list of length %o.") path element limit)))))
      ((equal type :string)
        (unless (stringp element)
          (error (format (concat base "< %S > is not a string.") path element))))
      ((equal type :float)
        (unless (floatp element)
          (error (format (concat base "< %S > is not a float.") path element))))
      ((equal type :integer)
        (unless (integerp element)
          (error (format (concat base "< %S > is not a integer.") path element))))
      ((equal type :error)
        (let ((first (car element))
              (second (nth 1 element)))
          (unless (and (equal :error first) (stringp second))
            (error (format (concat base "< %S > is not an error message.") path element))))))))

(defun benchwarmer--validate-result-with-path (result path)
  (benchwarmer--validate-element-with-path result :list path 2)
  (let ((benchmark-name (car result))
         (benchmark (nth 1 result)))
    (benchwarmer--validate-element-with-path benchmark-name :string path)
    (benchwarmer--validate-element-with-path benchmark :list path)
    (if (= 2 (length benchmark))
      (benchwarmer--validate-element-with-path benchmark :error path)
      (progn
        (benchwarmer--validate-element-with-path benchmark :list path 3)
        (let ((first (car benchmark))
               (second (nth 1 benchmark))
               (third (nth 2 benchmark)))
          (benchwarmer--validate-element-with-path first :float path)
          (benchwarmer--validate-element-with-path second :integer path)
          (benchwarmer--validate-element-with-path third :float path))))))

(defun benchwarmer--validate-suite (suite path)
  (benchwarmer--validate-element-with-path suite :list path)
  (let ((suite-name (car suite))
         (results (cdr suite)))
    (benchwarmer--validate-element-with-path suite-name :string path)
    (dolist (result results)
      (benchwarmer--validate-result-with-path result path))))

(defun benchwarmer--validate-report (report path)
  "Check if `REPORT' contents is valid."
  (benchwarmer--validate-element-with-path report :list path)
  (let ((report-name (car report))
         (suites (cdr report)))
    (benchwarmer--validate-element-with-path report-name :string path)
    (dolist (suite suites)
      (benchwarmer--validate-suite suite path)))
  report)

(defun benchwarmer--read-report-from-file (name)
  "Read a benchwarmer report from the file with the given `NAME'."
  (let ((path (benchwarmer--file-path name)))
    (condition-case err
      (let ((report (with-temp-buffer
                      (insert-file-contents path)
                      (read (current-buffer)))))
       (benchwarmer--validate-report report path))
     (end-of-file `(:error ,(format "Report file %s is empty." path)))
     (error `(:error ,(error-message-string err))))))

(defun benchwarmer--write-report-file (file-path content)
  "Write to `FILE-PATH' the `CONTENT'."
  (unless (file-directory-p benchwarmer-report-directory)
    (make-directory benchwarmer-report-directory t))
  (write-region "" nil file-path)
  (with-temp-file file-path
    (pp content (current-buffer))))

(defun benchwarmer--calculate-percentage (first second)
  "Calculates the percentage difference between `FIRST' and `SECOND'.

Division by 0 is handled by returning nil."
  (cond
    ((and (= 0 first) (= 0 second)) nil)
    ((= 0 second) nil)
    (t (* 100 (/ (float first) second)))))

(defun benchwarmer--calculate-difference (first second)
  "Calculate the difference between the `FIRST' and `SECOND' reports."
  (let* ((first-runtime (car first))
         (second-runtime (car second))
         (first-gc-count (nth 1 first))
         (second-gc-count (nth 1 second))
         (first-gc-runtime (nth 2 first))
         (second-gc-runtime (nth 2 second))
         (diff-runtime (- second-runtime first-runtime))
         (diff-gc-count (- second-gc-count first-gc-count))
         (diff-gc-runtime (- second-gc-runtime first-gc-runtime))
         (percent-runtime (benchwarmer--calculate-percentage diff-runtime first-runtime))
         (percent-gc-count (benchwarmer--calculate-percentage diff-gc-count first-gc-count))
         (percent-gc-runtime (benchwarmer--calculate-percentage diff-gc-runtime first-gc-runtime)))
    (list
      (list diff-runtime diff-gc-count diff-gc-runtime)
      (list percent-runtime percent-gc-count percent-gc-runtime))))

(defun benchwarmer--pivot-suite (suite report-name)
  "Pivot `SUITE' and return it tagged with `REPORT-NAME'."
  (let* ((suite-name (car suite))
          (benchmarks (mapcar
                        (lambda (benchmark)
                          (let* ((benchmark-name (car benchmark))
                                  (benchmark-body (nth 1 benchmark)))
                            `(,benchmark-name (,report-name ,benchmark-body))))
                        (cdr suite))))
    `(,suite-name ,@benchmarks)))

(defun benchwarmer--pivot-reports (reports)
  "Pivot `REPORTS'."
  (mapcar
    (lambda (report)
      (let* ((report-name (car report))
              (report-body (cdr report)))
        (mapcar
          (lambda (suite)
            (benchwarmer--pivot-suite suite report-name))
          report-body)))
    reports))

(defun benchwarmer--join-suites (reports)
  "Join `REPORTS'."
  (let ((accumulator '()))
    (dolist (report reports)
      (dolist (suite report)
        (setq accumulator (cons suite accumulator))))
    (reverse accumulator)))

(defun benchwarmer--merge-lists-by-string-car (elements)
  "Merge `ELEMENTS' comparing by the `car' of each list."
  (let ((accumulator '()))
    (while elements
      (let* ((first (car elements))
              (other (cdr elements)))
        (let* ((similar (let ((found '()))
                          (dolist (element other)
                            (when (string= (car first) (car element))
                              (setq found (cons element found))))
                          (reverse found)))
                (different (let ((found '()))
                             (dolist (element other)
                               (unless (string= (car first) (car element))
                                 (setq found (cons element found))))
                             (reverse found)))
                (name (car first))
                (mine (cdr first))
                (body (let ((body-parts '()))
                        (dolist (element similar)
                          (dolist (benchmark (cdr element))
                            (setq body-parts (cons benchmark body-parts))))
                        (reverse body-parts)))
                (report `(,name ,@mine ,@body)))
          (setq accumulator (cons report accumulator))
          (setq elements different))))
    (reverse accumulator)))

(defun benchwarmer--merge-suites (suites)
  "Merge `SUITES'."
  (benchwarmer--merge-lists-by-string-car suites))

(defun benchwarmer--group-suites (suites)
  "Group `SUITES'."
  (mapcar
    (lambda (suite)
      (let* ((name (car suite))
              (body (cdr suite))
              (merged (benchwarmer--merge-lists-by-string-car body)))
        `(,name ,@merged)))
    suites))

(defun benchwarmer--add-diffs-to-suites (suites diff-type)
  "Add diffs to `SUITES' depending on `DIFF-TYPE'."
  (mapcar
    (lambda (suite)
      (let* ((name (car suite))
              (body (cdr suite))
              (result (mapcar
                        (lambda (element)
                          (benchwarmer--add-diffs-to-merged-reports element diff-type))
                        body)))
        `(,name ,@result)))
    suites))

(defun benchwarmer--add-diffs-to-merged-reports (merged diff-type)
  "Add diffs to `MERGED' depending on `DIFF-TYPE'."
  (let* ((name (car merged))
          (reports (cdr merged))
          (first (car reports))
          (current first)
          (result (list first name))
          (rest (cdr reports)))
    (dolist (second rest)
      (let* ((diff (benchwarmer--diff-result-by-type
                     first current second diff-type)))
        (setq current second)
        (setq result (cons diff result))))
    (reverse result)))

(defun benchwarmer--merge-reports-from-files (file-names diff-type)
  "Merge reports from `FILE-NAMES' depending on `DIFF-TYPE'."
  (let* ((reports (mapcar 'benchwarmer--read-report-from-file file-names))
          (pivoted (benchwarmer--pivot-reports reports))
          (joined (benchwarmer--join-suites pivoted))
          (merged (benchwarmer--merge-suites joined))
          (grouped (benchwarmer--group-suites merged))
          (body (benchwarmer--add-diffs-to-suites grouped diff-type))
          (result `(,file-names ,@body)))
    result))

(defun benchwarmer--diff-result (first current second diff-with-first diff-with-previous)
  "Private: `FIRST' `CURRENT' `SECOND' `DIFF-WITH-FIRST' `DIFF-WITH-PREVIOUS'."
  (let* ((first-report (nth 1 first))
         (current-report (nth 1 current))
         (second-name (car second))
         (second-report (nth 1 second))
         (diff-with-first
           (if (and diff-with-first (floatp (car first-report)) (floatp (car second-report)))
             `((,diff-with-first ,(benchwarmer--calculate-difference first-report second-report)))
             nil))
         (diff-with-previous
           (if (and diff-with-previous (floatp (car current-report)) (floatp (car second-report)))
             `((,diff-with-previous ,(benchwarmer--calculate-difference current-report second-report)))
             nil))
         (result `(,second-name ,second-report ,@diff-with-first ,@diff-with-previous)))
    result))

(defun benchwarmer--diff-result-by-type (first current second type)
  "Diffs `FIRST' `CURRENT' `SECOND' depending on `TYPE'."
  (cond
    ((equal type :simple)
      (benchwarmer--diff-result
        first nil second :diff nil))
    ((equal type :first)
      (benchwarmer--diff-result
        first nil second :diff-with-first nil))
    ((equal type :previous)
      (benchwarmer--diff-result
        nil current second nil :diff-with-previous))
    ((equal type :mixed)
      (benchwarmer--diff-result
        first current second :diff-with-first :diff-with-previous))))

(defun benchwarmer--evaluate-forms (forms)
  (let ((results '())
         (index 0))
    (dolist (form forms)
      (setq index (+ 1 index))
      (let ((result
              (condition-case err
                (eval form)
                (error
                  (error (format
                           "FORM: < %S > evaluates to error: < %s >."
                           form
                           (error-message-string err)))))))
        (benchwarmer--validate-result result)
        (setq results (cons result results))))
    (reverse results)))

(defun benchwarmer--evaluate-suite-forms (forms)
  (condition-case err
    (benchwarmer--evaluate-forms forms)
    (error `((:error ,(error-message-string err))))))

(defun benchwarmer--compare-multiple-reports (forms compare-type)
  "Compare reports generated by evaluating `FORMS' depending on `COMPARE-TYPE'."
  (let* ((reports (benchwarmer--evaluate-forms forms))
          (first (car reports))
          (current first)
          (rest (cdr reports))
          (all-reports (list first)))
    (dolist (second rest)
      (let* ((second-name (car second))
              (diff (benchwarmer--diff-result-by-type
                      first current second compare-type)))
        (setq all-reports (cons diff all-reports))
        (setq current second)))
    (reverse all-reports)))

(defun benchwarmer--run-by-type (name times form type)
  "Private benchwarmer--run-by-type : `NAME' `TIMES' `FORM' `TYPE' `TWICE'."
  (condition-case err
    (progn
      (eval `(,type 1 ,form))
      (let ((result (eval `(,type ,times ,form))))
        `(,name ,result)))
    (error `(,name (:error ,(error-message-string err))))))

(defun benchwarmer--compare-by-type (name file-names report-type output-type)
  "Private: `NAME' `FILE-NAMES' `REPORT-TYPE' `OUTPUT-TYPE'."
  (benchwarmer--set-report-vairables-from-cli)
  (let* ((output-function (gethash output-type benchwarmer--output-functions))
          (result (benchwarmer--compare-multiple-reports file-names report-type)))
    (funcall output-function `((,output-type . ,name) ,result))))

(defun benchwarmer--compare-reports-by-type (file-names report-type output-type)
  "Compares reports from `FILE-NAMES' depending on `REPORT-TYPE' and `OUTPUT-TYPE'."
  (benchwarmer--set-report-vairables-from-cli)
  (let* ((output-function
           (gethash output-type benchwarmer--output-functions))
          (result (benchwarmer--merge-reports-from-files file-names report-type)))
    (funcall output-function result)))

;; Validation -------------------------------------------------------------------

(defmacro benchwarmer--with-valid-function-arguments (validations body)
  (let ((argument (make-symbol "argument"))
         (value (make-symbol "value"))
         (type (make-symbol "type"))
         (name (make-symbol "name")))
    `(progn
       (dolist (validation ,validations)
         (set (quote ,argument) (car validation))
         (set (quote ,value) (nth 1 validation))
         (set (quote ,type) (nth 2 validation))
         (set (quote ,name) (upcase (symbol-name ,argument)))
         (cond
           ((equal :integer ,type)
             (unless (integerp ,value)
               (error (format "%s: Argument value < %S > is not an integer." ,name ,value))))
           ((equal :string ,type)
             (unless (stringp ,value)
               (error (format "%s: Argument value < %S > is not a string." ,name ,value))))
           ((equal :list ,type)
             (unless (listp ,value)
               (error (format "%s: Argument value < %S > is not a list." ,name ,value))))
           ((equal :list-of-lists ,type)
             (dolist (form ,value)
               (unless (listp form)
                 (error (format "%s: Argument sub-element < %S > is not a list." ,name form)))))
           ((equal :list-of-strings ,type)
             (dolist (element ,value)
               (unless (stringp element)
                 (error (format "%s: Argument sub-element < %S > is not a string." ,name element)))))))
       ,body)))

(defun benchwarmer--validate-output-function-arguments (key &optional function)
  "Validate that `KEY' and `FUNCTION' are correct output function arguments."
  (unless (member key benchwarmer--output-types)
    (error (format "KEY: %s is not a valid output function type." key)))
  (when function
    (unless (or
              (symbolp function)
              (and
                (listp function)
                (equal 'lambda (car function))))
      (error (format "FUNCTION: %s is not a symbol nor a lambda." function)))))

(defun benchwarmer--shuffle-registered-suites ()
  (when (equal t benchwarmer--random-seed)
    (setq benchwarmer--random-seed (number-to-string (random 32767))))
  (random benchwarmer--random-seed)
  (let* ((total (length benchwarmer--registered-suites))
         (container (make-vector total nil)))
    (dotimes (index total)
      (aset container index (car benchwarmer--registered-suites))
      (setq benchwarmer--registered-suites (cdr benchwarmer--registered-suites)))
    (dotimes (index total)
      (let* ((first index)
             (second (+ index (random (- total index))))
             (carry (aref container first)))
        (aset container first (aref container second))
        (aset container second carry)))
    (setq benchwarmer--registered-suites (append container nil))))

(defun benchwarmer--duplicate-suite-names (name)
  (let ((found '()))
    (dolist (element benchwarmer--registered-suites)
      (when (string= name (car element))
        (setq found (cons element found))))
    found))

(defun benchwarmer--suite-runner-body (args)
  (let ((globs "*")
         (pattern ".*"))
    (dolist (arg args)
      (cond
        ((equal :suites (car arg))
          (setq globs (cdr arg)))))
    (when argv
      (let ((option (car argv))
             (value (nth 1 argv)))
        (cond
          ((and (string= option "--") (string-match-p "^[^--]" value))
            (setq pattern (nth 1 argv))))))
    (dolist (glob globs)
      (let ((files (file-expand-wildcards glob)))
        (dolist (file files)
          (when (string-match-p pattern file)
            (load file)))))
    (benchwarmer-report-batch-and-exit)))

(defun benchwarmer--register-suite (name forms)
  (let ((suite `(,name (lambda ()
                         (cons
                           ,name
                           (benchwarmer--evaluate-suite-forms '(,@forms)))))))
    (if (benchwarmer--duplicate-suite-names name)
      (error (format "ERROR: A suite named < %S > already exists." name))
      (setq benchwarmer--registered-suites
        (cons suite benchwarmer--registered-suites)))))

(defun benchwarmer--call-run (name times form function)
  (condition-case err
    (benchwarmer--with-valid-function-arguments
      `((name ,name :string)
         (times ,times :integer)
         (form ,form :list))
      (benchwarmer--run-by-type name times form function))
    (error
      (if (stringp name)
        `(,name (:error ,(error-message-string err)))
        `("NAME ERROR" (:error ,(error-message-string err)))))))

;;-------------------------------------------------------------------------------
;; Public API
;;-------------------------------------------------------------------------------

;; Run benchmakrs ---------------------------------------------------------------

(defmacro benchwarmer-run (name times form)
  "Run a benchmark with `NAME' a numeber of `TIMES' on the given `FORM'.

The benchmark functionality is provided by the `benchmark-run' macro.

Arguments:
- `NAME': a string representing the benchmark name.
- `TIMES': an integer representing the number of runs for the benchmark.
- `FORM': a list containing the code that needs to be benchmarked.

If the arguments are of the wrong type or evaluating `FORM' produces an error,
the benchmark is stopped and an error message is returned.

```emacs-lisp
\(\"name\" (:error \"error message\"))
```

Otherise, a list containing the given `NAME' followed by the result of
`benchmark-run' is given back.

```emacs-lisp
\(\"name\" (1.1 2 3.3))
```"
  `(benchwarmer--call-run ,name ,times '(,@form) 'benchmark-run))

(defmacro benchwarmer-run-compiled (name times form)
  "Run a benchmark with `NAME' a numeber of `TIMES' on the given `FORM'.

The benchmark functionality is provided by the `benchmark-run-compiled' macro.

Arguments:
- `NAME': a string representing the benchmark name.
- `TIMES': an integer representing the number of runs for the benchmark.
- `FORM': a list containing the code that needs to be benchmarked.

If the arguments are of the wrong type or evaluating `FORM' produces an error,
the benchmark is stopped and an error message is returned.

```emacs-lisp
\(\"name\" (:error \"error message\")
```

Otherise, a list containing the given `NAME' followed by the result of
`benchmark-run-compiled' is given back.

```emacs-lisp
\(\"name\" (1.1 2 3.3))
```"
  `(benchwarmer--call-run ,name ,times '(,@form) 'benchmark-run-compiled))

;; Declare suites ---------------------------------------------------------------

(defmacro benchwarmer-suite (name &rest forms)
  "Defines and registers a benchmarking suite.

The first argument, `name', is a sting representing the name of the suite.
The rest of the arguments are forms that are evaluated in order to produce
benchmark results. They must be lists with one of the following function calls:
- `benchwarmer-run'
- `benchwarmer-run-compiled'"
  `(benchwarmer--with-valid-function-arguments
     '((name ,name :string)
        (forms ,forms :list-of-lists))
     (benchwarmer--register-suite ,name '(,@forms))))

;; Compare benchmarks -----------------------------------------------------------

(defmacro benchwarmer-compare (name first second)
  "Compares `first' and `second' benchmarks."
  `(benchwarmer--with-valid-function-arguments
     '((name ,name :string)
        (first ,first :list)
        (second ,second :list))
     (benchwarmer--compare-by-type ,name (list '(,@first) '(,@second)) :simple :compare)))

(defmacro benchwarmer-compare-with-first (name &rest forms)
  "Compares each of the provided `forms' with the first one."
  `(benchwarmer--with-valid-function-arguments
     '((name ,name :string)
        (forms ,forms :list-of-lists))
     (benchwarmer--compare-by-type ,name '(,@forms) :first :compare-with-first)))

(defmacro benchwarmer-compare-with-previous (name &rest forms)
  "Compares each of the provided `forms' with the previous one."
  `(benchwarmer--with-valid-function-arguments
     '((name ,name :string)
        (forms ,forms :list-of-lists))
     (benchwarmer--compare-by-type ,name '(,@forms) :previous :compare-with-previous)))

(defmacro benchwarmer-compare-mixed (name &rest forms)
  "Compares each of the provided `forms' both with the previous and the first one."
  `(benchwarmer--with-valid-function-arguments
     '((name ,name :string)
        (forms ,forms :list-of-lists))
     (benchwarmer--compare-by-type ,name '(,@forms) :mixed :compare-mixed)))

;; Compare reports --------------------------------------------------------------

(defun benchwarmer-compare-before-and-after ()
  "Compares the `before' and `after' benchmark results."
  (benchwarmer--compare-reports-by-type (list "before" "after") :simple :compare-before-and-after))

(defun benchwarmer-compare-reports (first second)
  "Compares `FIRST' and `SECOND' benchmark results."
  (benchwarmer--with-valid-function-arguments
    `((first ,first :string)
       (second ,second :string))
    (benchwarmer--compare-reports-by-type (list first second) :simple :compare-reports)))

(defun benchwarmer-compare-reports-with-first (&rest file-names)
  "Compares each of the provided benchmark results with the first one."
  (benchwarmer--with-valid-function-arguments
    `((file-names ,file-names :list-of-strings))
    (benchwarmer--compare-reports-by-type file-names :first :compare-reports-with-first)))

(defun benchwarmer-compare-reports-with-previous (&rest file-names)
  "Compares each of the provided benchmark results with the previous one."
  (benchwarmer--with-valid-function-arguments
    `((file-names ,file-names :list-of-strings))
    (benchwarmer--compare-reports-by-type file-names :previous :compare-reports-with-previous)))

(defun benchwarmer-compare-reports-mixed (&rest file-names)
  "Compares each of the provided benchmark results with both the previous and
the first one."
  (benchwarmer--with-valid-function-arguments
    `((file-names ,file-names :list-of-strings))
    (benchwarmer--compare-reports-by-type file-names :mixed :compare-reports-mixed)))

;; Run suites -------------------------------------------------------------------

(defun benchwarmer-report-batch-and-exit ()
  "Executes the registered benchmark suites and exits."
  (benchwarmer--set-report-vairables-from-cli)
  (if benchwarmer--random-seed
    (benchwarmer--shuffle-registered-suites)
    (setq benchwarmer--registered-suites (reverse benchwarmer--registered-suites)))
  (let* ((suites (mapcar
                   (lambda (suite)
                     (let ((name (car suite))
                           (body (nth 1 suite)))
                       (funcall body)))
                   benchwarmer--registered-suites))
          (result (cons benchwarmer-report-name suites)))
    (benchwarmer--write-report-file (benchwarmer--file-path) result)))

;; Configure --------------------------------------------------------------------

(defun benchwarmer-initialize-output-functions ()
  "Set the output functions to their default values."
  (setq benchwarmer--output-functions (benchwarmer--initial-output-functions)))

(defun benchwarmer-output-function (key &optional function)
  "Return the function found by `KEY' or set a new one if `FUNCTION' is given."
  (benchwarmer--validate-output-function-arguments key function)
  (if function
    (puthash key function benchwarmer--output-functions)
    (gethash key benchwarmer--output-functions)))

;; Suite runner -----------------------------------------------------------------

(defmacro benchwarmer-suite-runner (&rest args)
  "Declares a suite runner configured by `ARGS'."
  `(benchwarmer--with-valid-function-arguments
     '((args ,args :list-of-lists))
     (benchwarmer--suite-runner-body (quote ,args))))

;; Provide ----------------------------------------------------------------------

(provide 'benchwarmer)

;;; benchwarmer.el ends here

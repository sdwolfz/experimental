#!/usr/bin/env bash

# Generates the documentation and check if any changes are missing.

set -ex

emacs --batch --load ./markdown-docs.el --eval '(markdown-docs-generate "./benchwarmer.el")'

git diff --exit-code

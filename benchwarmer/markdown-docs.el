;;; markdown-docs.el --- makrdown documentation -*- lexical-binding: t -*-
;; A package for generating documentation in markdown format.
;;
;; This package will be extracted in a separate repository once the
;; minimal functionality is complete.

;; Variables --------------------------------------------------------------------

(defvar markdown-docs-directory "./docs"
  "Directory where the documentation is generated.")

;; Files ------------------------------------------------------------------------

(defun markdown-docs--file-path (file-name &optional package-name)
  (if package-name
    (concat
     (file-name-as-directory
       (concat
         (file-name-as-directory markdown-docs-directory)
         package-name))
     file-name)
    (concat
      (file-name-as-directory markdown-docs-directory)
      file-name)))

(defun markdown-docs--write-to-file (title toc elements path)
  (let ((directory (file-name-directory path)))
    (unless (file-directory-p directory)
      (make-directory directory t)))
  (write-region "" nil path)
  (with-temp-file path
    (when title
      (princ title (current-buffer))
      (princ "\n\n" (current-buffer)))
    (when toc
      (princ toc (current-buffer))
      (princ "\n\n" (current-buffer)))
    (when elements
      (princ (mapconcat 'identity elements "\n\n") (current-buffer))
      (princ "\n" (current-buffer)))))

;; Generators -------------------------------------------------------------------

(defun markdown-docs--generate-function-docs (package-name functions)
  (let* ((package-string (format "%s Functions" (mapconcat 'capitalize (split-string package-name "-") " ")))
         (elements '())
         (toc      `(,(format "- [%s](#%s-functions)" package-string package-name)
                      "**Table of Contents**\n")))
    (dolist (element functions)
      (let* ((name (car element))
             (args (nth 1 element))
             (docs (nth 2 element))
             (body (replace-regexp-in-string "\\(`.+?\\)'" "\\1`" docs))
             (signature (format "```emacs-lisp
(defun %s %s ...)
```" name args))
             (result (concat
                       (format "## %s" name)
                       "\n\n"
                       (format "%s" signature)
                       "\n\n"
                       body
                       "\n\n"
                       (format "[↑ Top](#%s-functions)"  package-name))))
        (setq toc (cons (format "    - [%s](#%s)" name name) toc))
        (setq elements (cons result elements))))
    (let* ((title (format "# %s" package-string))
           (toc (mapconcat 'identity (reverse toc) "\n"))
           (elements (reverse elements)))
      (markdown-docs--write-to-file title toc elements (markdown-docs--file-path "functions.md" package-name)))))

(defun markdown-docs--generate-macros-docs (package-name macros)
  (let* ((package-string (format "%s Macros" (mapconcat 'capitalize (split-string package-name "-") " ")))
         (elements '())
         (toc      `(,(format "- [%s](#%s-macros)" package-string package-name)
                      "**Table of Contents**\n")))
    (dolist (element macros)
      (let* ((name (car element))
             (args (nth 1 element))
             (docs (nth 2 element))
             (body (replace-regexp-in-string "\\(`.+?\\)'" "\\1`" docs))
             (signature (format "```emacs-lisp
(defun %s %s ...)
```" name args))
             (result (concat
                       (format "## %s" name)
                       "\n\n"
                       (format "%s" signature)
                       "\n\n"
                       body
                       "\n\n"
                       (format "[↑ Top](#%s-macros)"  package-name))))
        (setq toc (cons (format "    - [%s](#%s)" name name) toc))
        (setq elements (cons result elements))))
    (let* ((title (format "# %s" package-string))
           (toc (mapconcat 'identity (reverse toc) "\n"))
           (elements (reverse elements)))
      (markdown-docs--write-to-file title toc elements (markdown-docs--file-path "macros.md" package-name)))))

(defun markdown-docs--generate-variable-docs (package-name variables)
  (let* ((package-string (format "%s Variables" (mapconcat 'capitalize (split-string package-name "-") " ")))
         (elements '())
         (toc      `(,(format "- [%s](#%s-variables)" package-string package-name)
                      "**Table of Contents**\n")))
    (dolist (element variables)
      (let* ((name (car element))
             (args (nth 1 element))
             (docs (nth 2 element))
             (body (replace-regexp-in-string "\\(`.+?\\)'" "\\1`" docs))
             (signature (format "```emacs-lisp
(setq %s %S)
```" name args))
             (result (concat
                       (format "## %s" name)
                       "\n\n"
                       (format "%s" signature)
                       "\n\n"
                       body
                       "\n\n"
                       (format "[↑ Top](#%s-variables)"  package-name))))
        (setq toc (cons (format "    - [%s](#%s)" name name) toc))
        (setq elements (cons result elements))))
    (let* ((title (format "# %s" package-string))
           (toc (mapconcat 'identity (reverse toc) "\n"))
           (elements (reverse elements)))
      (markdown-docs--write-to-file title toc elements (markdown-docs--file-path "variables.md" package-name)))))

(defun markdown-docs--generate-commentary-from-string (package-name string)
  (let* ((decommented (replace-regexp-in-string "^;; *" "" string))
          (body (mapconcat 'identity (cdr (split-string decommented "\n")) "\n"))
          (trimmed-left (replace-regexp-in-string "\\(^\\s-*$\\)\n" "" body))
          (trimmed-right (replace-regexp-in-string "\n\\'" "" trimmed-left))
          (backticked (replace-regexp-in-string "\\(`.+?\\)'" "\\1`" trimmed-right))
          (title (format "# %s" (mapconcat 'capitalize (split-string package-name "-") " ")))
          (toc nil)
          (elements (list backticked)))
    (markdown-docs--write-to-file title toc elements (markdown-docs--file-path (format "%s.md" package-name)))))

;; ";;; Markdown Docs Commentary:.*?\\(\n\\(;;[^;].*\\|\n\\)*\\)*"
(defun markdown-docs--generate-commentary-docs (package-name content)
  (if (string-match "^;;; Markdown Docs Commentary:.*?\n\\(;;.*?\n\\)*" content)
    (markdown-docs--generate-commentary-from-string package-name (match-string 0 content))
    (when (string-match "^;;; Commentary:.*?\n\\(;;.*?\n\\)*" content)
      (markdown-docs--generate-commentary-from-string package-name (match-string 0 content)))))

(defun markdown-docs--generate-category-docs (package-name string)
  (string-match "^;;; Markdown Docs Category: \\(.*?\\)\n" string)
  (let* ((category (match-string 1 string))
         (category-id (mapconcat 'identity (split-string (downcase category) " ") "-"))
          (header-content (format "%s %s"
                           (mapconcat 'capitalize (split-string package-name "-") " ")
                           category))
          (header-id (mapconcat 'identity (split-string (downcase header-content) " ") "-"))
         (header (format "# %s" header-content))
          (toc `(,(format "- [%s](#%s)" header-content header-id)
                    "**Table of Contents**\n"))
         (result nil))
    (when (string-match "^;;; Markdown Docs Element: \\(.*?\\)\n\\(;;.*?\n\\)+" string)
      (let* ((matched (match-string 0 string))
              (elements (cdr (split-string matched ";;; Markdown Docs Element: ")))
              (processed '()))
        (dolist (element elements)
          (let* ((lines (split-string element "\n"))
                  (title (car lines))
                  (body (mapconcat 'identity (cdr lines) "\n"))
                  (decommented (replace-regexp-in-string "^;; *" "" body))
                  (trimmed-left (replace-regexp-in-string "\\(^\\s-*$\\)\n" "" decommented))
                  (trimmed-right (replace-regexp-in-string "\n\\'" "" trimmed-left))
                  (backticked (replace-regexp-in-string "\\(`.+?\\)'" "\\1`" trimmed-right))
                  (content (list backticked)))
            (setq processed (cons `(,title . ,content) processed))))
        (dolist (element (reverse processed))
          (let* ((first (car element))
                  (first-id (mapconcat 'identity (split-string (downcase first) " ") "-"))
                  (title (format "## %s" first))
                  (body (format "%s\n\n%s\n\n%s"
                          title
                          (nth 1 element)
                          (format "[↑ Top](#%s)" header-id))))
            (setq toc (cons (format "    - [%s](#%s)" first first-id) toc))
            (setq result (cons body result))))
        (markdown-docs--write-to-file
          header
          (mapconcat 'identity (reverse toc) "\n")
          result
          (markdown-docs--file-path (format "%s.md" category-id) package-name))))))

(defun markdown-docs--generate-categories-docs (package-name content)
  (when (string-match "^;;; Markdown Docs Category: \\(.*?\\)\n\\(;;.*?\n\\)*" content)
    (markdown-docs--generate-category-docs package-name (match-string 0 content))))

;; Public API -------------------------------------------------------------------

;; Execute with the following command:
;; emacs --batch --load ./markdown-docs.el --eval '(markdown-docs-generate "./benchwarmer.el")'
(defun markdown-docs-generate (path)
  "Generates documentation for package found at `PATH'."
  (let ((content (with-temp-buffer
                   (insert-file-contents-literally path)
                   (decode-coding-region (point-min) (point-max) 'utf-8 t))))
    (let ((package-name "")
           (position 0)
           (constants '())
           (functions '())
           (macros    '())
           (variables '()))
      ;; Extract Public API documentation from AST.
      (condition-case err
        (while t
          (let* ((result (read-from-string content position))
                  (body (car result))
                  (new-position (cdr result))
                  (first (car body)))
            (let ((name (format "%s" (nth 1 body))))
              (unless (string-match-p "--" name)
                (cond
                  ((equal 'defconst first)
                    (setq constants (cons name constants)))
                  ((equal 'defun first)
                    (let ((args (nth 2 body))
                           (docs (nth 3 body)))
                      (setq functions (cons (list name args docs) functions))))
                  ((equal 'defmacro first)
                    (let ((args (nth 2 body))
                           (docs (nth 3 body)))
                      (setq macros (cons (list name args docs) macros))))
                  ((equal 'defvar first)
                    (let ((args (nth 2 body))
                           (docs (nth 3 body)))
                      (setq variables (cons (list name args docs) variables))))
                  ((equal 'provide first)
                    (setq package-name (format "%s" (nth 1 (nth 1 body))))))))
            (setq position new-position)))
        (error ;; (message (format "Error: %s" (error-message-string err)))
          nil))
      (markdown-docs--generate-commentary-docs package-name content)
      (markdown-docs--generate-categories-docs package-name content)
      ;; (message "Constants: %s" (reverse constants))
      (markdown-docs--generate-function-docs package-name (reverse functions))
      (markdown-docs--generate-macros-docs package-name (reverse macros))
      (markdown-docs--generate-variable-docs package-name (reverse variables)))))

;; Provide ----------------------------------------------------------------------

(provide 'markdown-docs)

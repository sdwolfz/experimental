# Benchwarmer Macros

**Table of Contents**

- [Benchwarmer Macros](#benchwarmer-macros)
    - [benchwarmer-run](#benchwarmer-run)
    - [benchwarmer-run-compiled](#benchwarmer-run-compiled)
    - [benchwarmer-suite](#benchwarmer-suite)
    - [benchwarmer-compare](#benchwarmer-compare)
    - [benchwarmer-compare-with-first](#benchwarmer-compare-with-first)
    - [benchwarmer-compare-with-previous](#benchwarmer-compare-with-previous)
    - [benchwarmer-compare-mixed](#benchwarmer-compare-mixed)
    - [benchwarmer-suite-runner](#benchwarmer-suite-runner)

## benchwarmer-run

```emacs-lisp
(defun benchwarmer-run (name times form) ...)
```

Run a benchmark with `NAME` a numeber of `TIMES` on the given `FORM`.

The benchmark functionality is provided by the `benchmark-run` macro.

Arguments:
- `NAME`: a string representing the benchmark name.
- `TIMES`: an integer representing the number of runs for the benchmark.
- `FORM`: a list containing the code that needs to be benchmarked.

If the arguments are of the wrong type or evaluating `FORM` produces an error,
the benchmark is stopped and an error message is returned.

```emacs-lisp
("name" (:error "error message"))
```

Otherise, a list containing the given `NAME` followed by the result of
`benchmark-run` is given back.

```emacs-lisp
("name" (1.1 2 3.3))
```

[↑ Top](#benchwarmer-macros)

## benchwarmer-run-compiled

```emacs-lisp
(defun benchwarmer-run-compiled (name times form) ...)
```

Run a benchmark with `NAME` a numeber of `TIMES` on the given `FORM`.

The benchmark functionality is provided by the `benchmark-run-compiled` macro.

Arguments:
- `NAME`: a string representing the benchmark name.
- `TIMES`: an integer representing the number of runs for the benchmark.
- `FORM`: a list containing the code that needs to be benchmarked.

If the arguments are of the wrong type or evaluating `FORM` produces an error,
the benchmark is stopped and an error message is returned.

```emacs-lisp
("name" (:error "error message")
```

Otherise, a list containing the given `NAME` followed by the result of
`benchmark-run-compiled` is given back.

```emacs-lisp
("name" (1.1 2 3.3))
```

[↑ Top](#benchwarmer-macros)

## benchwarmer-suite

```emacs-lisp
(defun benchwarmer-suite (name &rest forms) ...)
```

Defines and registers a benchmarking suite.

The first argument, `name`, is a sting representing the name of the suite.
The rest of the arguments are forms that are evaluated in order to produce
benchmark results. They must be lists with one of the following function calls:
- `benchwarmer-run`
- `benchwarmer-run-compiled`

[↑ Top](#benchwarmer-macros)

## benchwarmer-compare

```emacs-lisp
(defun benchwarmer-compare (name first second) ...)
```

Compares `first` and `second` benchmarks.

[↑ Top](#benchwarmer-macros)

## benchwarmer-compare-with-first

```emacs-lisp
(defun benchwarmer-compare-with-first (name &rest forms) ...)
```

Compares each of the provided `forms` with the first one.

[↑ Top](#benchwarmer-macros)

## benchwarmer-compare-with-previous

```emacs-lisp
(defun benchwarmer-compare-with-previous (name &rest forms) ...)
```

Compares each of the provided `forms` with the previous one.

[↑ Top](#benchwarmer-macros)

## benchwarmer-compare-mixed

```emacs-lisp
(defun benchwarmer-compare-mixed (name &rest forms) ...)
```

Compares each of the provided `forms` both with the previous and the first one.

[↑ Top](#benchwarmer-macros)

## benchwarmer-suite-runner

```emacs-lisp
(defun benchwarmer-suite-runner (&rest args) ...)
```

Declares a suite runner configured by `ARGS`.

[↑ Top](#benchwarmer-macros)

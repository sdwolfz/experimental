# Benchwarmer CLI Flags

**Table of Contents**

- [Benchwarmer CLI Flags](#benchwarmer-cli-flags)
    - [--benchwarmer-after](#--benchwarmer-after)
    - [--benchwarmer-before](#--benchwarmer-before)
    - [--benchwarmer-directory](#--benchwarmer-directory)
    - [--benchwarmer-name](#--benchwarmer-name)

## --benchwarmer-name

```bash
emacs --script ./samples/sample-suite.el -- --benchwarmer-benchmark
```

[↑ Top](#benchwarmer-cli-flags)

## --benchwarmer-directory

```bash
emacs --script ./samples/sample-suite.el -- --benchwarmer-directory ./bench/reports
```

[↑ Top](#benchwarmer-cli-flags)

## --benchwarmer-before

```bash
emacs --script ./samples/sample-suite.el -- --benchwarmer-before
```

[↑ Top](#benchwarmer-cli-flags)

## --benchwarmer-after

```bash
emacs --script ./samples/sample-suite.el -- --benchwarmer-after
```

[↑ Top](#benchwarmer-cli-flags)

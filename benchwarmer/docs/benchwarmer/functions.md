# Benchwarmer Functions

**Table of Contents**

- [Benchwarmer Functions](#benchwarmer-functions)
    - [benchwarmer-compare-before-and-after](#benchwarmer-compare-before-and-after)
    - [benchwarmer-compare-reports](#benchwarmer-compare-reports)
    - [benchwarmer-compare-reports-with-first](#benchwarmer-compare-reports-with-first)
    - [benchwarmer-compare-reports-with-previous](#benchwarmer-compare-reports-with-previous)
    - [benchwarmer-compare-reports-mixed](#benchwarmer-compare-reports-mixed)
    - [benchwarmer-report-batch-and-exit](#benchwarmer-report-batch-and-exit)
    - [benchwarmer-initialize-output-functions](#benchwarmer-initialize-output-functions)
    - [benchwarmer-output-function](#benchwarmer-output-function)

## benchwarmer-compare-before-and-after

```emacs-lisp
(defun benchwarmer-compare-before-and-after nil ...)
```

Compares the `before` and `after` benchmark results.

[↑ Top](#benchwarmer-functions)

## benchwarmer-compare-reports

```emacs-lisp
(defun benchwarmer-compare-reports (first second) ...)
```

Compares `FIRST` and `SECOND` benchmark results.

[↑ Top](#benchwarmer-functions)

## benchwarmer-compare-reports-with-first

```emacs-lisp
(defun benchwarmer-compare-reports-with-first (&rest file-names) ...)
```

Compares each of the provided benchmark results with the first one.

[↑ Top](#benchwarmer-functions)

## benchwarmer-compare-reports-with-previous

```emacs-lisp
(defun benchwarmer-compare-reports-with-previous (&rest file-names) ...)
```

Compares each of the provided benchmark results with the previous one.

[↑ Top](#benchwarmer-functions)

## benchwarmer-compare-reports-mixed

```emacs-lisp
(defun benchwarmer-compare-reports-mixed (&rest file-names) ...)
```

Compares each of the provided benchmark results with both the previous and
the first one.

[↑ Top](#benchwarmer-functions)

## benchwarmer-report-batch-and-exit

```emacs-lisp
(defun benchwarmer-report-batch-and-exit nil ...)
```

Executes the registered benchmark suites and exits.

[↑ Top](#benchwarmer-functions)

## benchwarmer-initialize-output-functions

```emacs-lisp
(defun benchwarmer-initialize-output-functions nil ...)
```

Set the output functions to their default values.

[↑ Top](#benchwarmer-functions)

## benchwarmer-output-function

```emacs-lisp
(defun benchwarmer-output-function (key &optional function) ...)
```

Return the function found by `KEY` or set a new one if `FUNCTION` is given.

[↑ Top](#benchwarmer-functions)

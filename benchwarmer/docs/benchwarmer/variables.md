# Benchwarmer Variables

**Table of Contents**

- [Benchwarmer Variables](#benchwarmer-variables)
    - [benchwarmer-report-directory](#benchwarmer-report-directory)
    - [benchwarmer-report-name](#benchwarmer-report-name)

## benchwarmer-report-directory

```emacs-lisp
(setq benchwarmer-report-directory "./bench/reports")
```

Path to the directory in which benchmark reports will be saved.

[↑ Top](#benchwarmer-variables)

## benchwarmer-report-name

```emacs-lisp
(setq benchwarmer-report-name "benchmark")
```

Name of the file in which to save the benchmark report.

[↑ Top](#benchwarmer-variables)

(ert-deftest test--benchwarmer-run-compiled--success ()
  (let ((result (benchwarmer-run-compiled "name" 1 (+ 1 2 3))))
    (assert-benchwarmer-result result "name")))

(ert-deftest test--benchwarmer-run-compiled--error ()
  (let* ((expected '("name" (:error "error message")))
          (result (benchwarmer-run-compiled "name" 1 (error "error message"))))
    (assert-benchwarmer-result result "name")
    (should (equal expected result))))

(ert-deftest test--benchwarmer-run-compiled--wrong-name ()
  (let* ((expected '("NAME ERROR" (:error "NAME: Argument value < :wrong > is not a string.")))
          (result (benchwarmer-run-compiled :wrong 1 (+ 1 2 3))))
    (should (equal expected result ))))

(ert-deftest test--benchwarmer-run-compiled--wrong-times ()
  (let* ((expected '("name" (:error "TIMES: Argument value < :wrong > is not an integer.")))
          (result (benchwarmer-run-compiled "name" :wrong (+ 1 2 3))))
    (should (equal expected result))))

(ert-deftest test--benchwarmer-run-compiled--wrong-form ()
  (let* ((expected '("name" (:error "FORM: Argument value < :wrong > is not a list.")))
          (result (benchwarmer-run-compiled "name" 1 :wrong)))
    (should (equal expected result))))

(ert-deftest test--benchwarmer-suite--no-benchmarks ()
  (setq benchwarmer--registered-suites '())
  (let* ((suite-name "no-benchmarks"))
    (benchwarmer-suite "no-benchmarks")
    (should (= 1 (length benchwarmer--registered-suites)))
    (let* ((declared (car benchwarmer--registered-suites))
           (name (car declared))
           (body (nth 1 declared))
           (expected (list suite-name))
           (result (funcall body)))
      (assert-benchwarmer-suite result suite-name)
      (should (string= suite-name name))
      (should (equal expected result)))))

(ert-deftest test--benchwarmer-suite--one-benchmarks ()
  (setq benchwarmer--registered-suites '())
  (let* ((suite-name "one-benchmark")
         (benchmark-name "name"))
    (benchwarmer-suite "one-benchmark"
      (benchwarmer-run "name" 1 (+ 1 2 3)))
    (should (= 1 (length benchwarmer--registered-suites)))
    (let* ((declared (car benchwarmer--registered-suites))
           (name (car declared))
           (body (nth 1 declared))
           (result (funcall body)))
      (should (string= suite-name name))
      (assert-benchwarmer-suite
        result suite-name (list benchmark-name)))))

(ert-deftest test--benchwarmer-suite--two-benchmarks ()
  (setq benchwarmer--registered-suites '())
  (let ((suite-name "benchmark")
        (first-benchmark-name "first")
        (second-benchmark-name "second"))
    (benchwarmer-suite "benchmark"
      (benchwarmer-run "first" 1 (+ 1 2 3))
      (benchwarmer-run "second" 1 (- 4 5 6)))
    (should (= 1 (length benchwarmer--registered-suites)))
    (let* ((declared (car benchwarmer--registered-suites))
            (name (car declared))
            (body (nth 1 declared))
            (result (funcall body)))
      (should (string= suite-name name))
      (assert-benchwarmer-suite
        result
        suite-name
        (list first-benchmark-name second-benchmark-name)))))

(ert-deftest test--benchwarmer-suite--duplicate-suite-names ()
  (setq benchwarmer--registered-suites '())
  (let ((suite-name "suite")
         (first-benchmark-name "first")
         (second-benchmark-name "second")
         (expected '(error "ERROR: A suite named < \"suite\" > already exists.")))
    (benchwarmer-suite "suite"
      (benchwarmer-run "first" 1 (+ 1 2 3))
      (benchwarmer-run "second" 1 (- 4 5 6)))
    (should (= 1 (length benchwarmer--registered-suites)))
    (let* ((declared (car benchwarmer--registered-suites))
            (name (car declared))
            (body (nth 1 declared))
            (result (funcall body)))
      (should (string= suite-name name))
      (assert-benchwarmer-suite
        result
        suite-name
        (list first-benchmark-name second-benchmark-name)))
    (should
      (equal expected
        (should-error
          (benchwarmer-suite "suite"
            (benchwarmer-run "first"  1 (+ 1 2 3))
            (benchwarmer-run "second" 1 (- 4 5 6))))))))

(ert-deftest test--benchwarmer-suite--wrong-name ()
  (setq benchwarmer--registered-suites '())
  (let ((suite-name 8)
         (first-benchmark-name "first")
         (second-benchmark-name "second")
         (expected '(error "NAME: Argument value < 8 > is not a string.")))
    (should
      (equal expected
        (should-error
          (benchwarmer-suite 8
            (benchwarmer-run "first" 1 (+ 1 2 3))
            (benchwarmer-run "second" 1 (- 4 5 6))))))
    (should (= 0 (length benchwarmer--registered-suites)))))

(ert-deftest test--benchwarmer-suite--wrong-first-suite ()
  (setq benchwarmer--registered-suites '())
  (let ((suite-name "benchmark")
         (second-benchmark-name "second")
         (expected '(error "FORMS: Argument sub-element < 8 > is not a list.")))
    (should
      (equal expected
        (should-error
          (benchwarmer-suite "benchmark"
            8
            (benchwarmer-run "second" 1 (- 4 5 6))))))
    (should (= 0 (length benchwarmer--registered-suites)))))

(ert-deftest test--benchwarmer-suite--wrong-second-suite ()
  (setq benchwarmer--registered-suites '())
  (let ((suite-name "benchmark")
         (first-benchmark-name "first")
         (expected '(error "FORMS: Argument sub-element < 8 > is not a list.")))
    (should
      (equal expected
        (should-error
          (benchwarmer-suite "benchmark"
            (benchwarmer-run "first" 1 (+ 1 2 3))
            8))))
    (should (= 0 (length benchwarmer--registered-suites)))))

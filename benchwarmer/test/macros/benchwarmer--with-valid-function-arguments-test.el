(defun call-body-from-macro (form)
  form)

(defun call-macro-from-function (arg)
  (let ((validations `((arg ,arg :string))))
    (benchwarmer--with-valid-function-arguments
      validations
      (call-body-from-macro body-form))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--body ()
  (let* ((expected (format "%S"
                      '(progn
                         (dolist (validation '(1 2 3))
                           (set (quote argument) (car validation))
                           (set (quote value) (nth 1 validation))
                           (set (quote type) (nth 2 validation))
                           (set (quote name) (upcase (symbol-name argument)))
                           (cond
                             ((equal :integer type)
                               (unless (integerp value)
                                 (error (format "%s: Argument value < %S > is not an integer." name value))))
                             ((equal :string type)
                               (unless (stringp value)
                                 (error (format "%s: Argument value < %S > is not a string." name value))))
                             ((equal :list type)
                               (unless (listp value)
                                 (error (format "%s: Argument value < %S > is not a list." name value))))
                             ((equal :list-of-lists type)
                               (dolist (form value)
                                 (unless (listp form)
                                   (error (format "%s: Argument sub-element < %S > is not a list." name form)))))
                             ((equal :list-of-strings type)
                               (dolist (element value)
                                 (unless (stringp element)
                                   (error (format "%s: Argument sub-element < %S > is not a string." name element)))))))
                         (+ 4 5 6))))
          (result (format "%S"
                    (macroexpand `(benchwarmer--with-valid-function-arguments '(1 2 3) (+ 4 5 6))))))
    (should (equal expected result))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--integer ()
  (should
    (equal '(error "ARGS: Argument value < \"3\" > is not an integer.")
      (should-error
        (benchwarmer--with-valid-function-arguments
          '((args "3" :integer))
          "Success!"))))
  (should (string= "Success!"
            (benchwarmer--with-valid-function-arguments
              '((args 3 :integer))
              "Success!"))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--list ()
  (should
    (equal '(error "ARGS: Argument value < 3 > is not a list.")
      (should-error
        (benchwarmer--with-valid-function-arguments
          '((args 3 :list))
          "Success!"))))
  (should (string= "Success!"
            (benchwarmer--with-valid-function-arguments
              '((args (3) :list))
              "Success!"))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--list-of-lists ()
  (should
    (equal '(error "ARGS: Argument sub-element < 3 > is not a list.")
      (should-error
       (benchwarmer--with-valid-function-arguments
         '((args ((1) (2) 3) :list-of-lists))
         "Success!"))))
  (should (string= "Success!"
            (benchwarmer--with-valid-function-arguments
              '((args ((1) (2) (3)) :list-of-lists))
              "Success!"))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--list-of-strings ()
  (should
    (equal '(error "ARGS: Argument sub-element < 3 > is not a string.")
      (should-error
        (benchwarmer--with-valid-function-arguments
          '((args ("(1)" "(2)" 3) :list-of-strings))
          "Success!"))))
  (should (string= "Success!"
            (benchwarmer--with-valid-function-arguments
              '((args ("(1)" "(2)" "(3)") :list-of-strings))
              "Success!"))))

(ert-deftest test--benchwarmer--with-valid-function-arguments--string ()
  (let ((body-form
          (let ((first "Succ")
                (second "ess!"))
            (concat first second))))
    (should
      (equal '(error "ARGS: Argument value < 3 > is not a string.")
        (should-error
          (benchwarmer--with-valid-function-arguments
            '((args 3 :string))
            (call-body-from-macro body-form)))))
    (should (string= "Success!"
              (benchwarmer--with-valid-function-arguments
                '((args "3" :string))
                (call-body-from-macro body-form))))
    (should (string= "Success!"
              (call-macro-from-function "123")))))

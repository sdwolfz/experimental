(ert-deftest test--benchwarmer-compare-with-first--success ()
  (benchwarmer-initialize-output-functions)
  (let ((result (with-output-to-string
                  (benchwarmer-compare-with-first "benchmark"
                    (benchwarmer-run "first" 1 (+ 1 2 3))
                    (benchwarmer-run "second" 1 (- 1 2 3))
                    (benchwarmer-run "third" 1 (* 1 2 3))))))
    (should (stringp result))
    (should (string-match-p "^((:compare-with-first \. \"benchmark\")
 ((\"first\"
   (.*? .*? .*?))
  (\"second\"
   (.*? .*? .*?)
   (:diff-with-first
    ((.*? .*? .*?)
     (.*? .*? .*?))))
  (\"third\"
   (.*? .*? .*?)
   (:diff-with-first
    ((.*? .*? .*?)
     (.*? .*? .*?))))))$" result))))

(ert-deftest test--benchwarmer-compare-with-first--first-error ()
  (benchwarmer-initialize-output-functions)
  (let ((result (with-output-to-string
                  (benchwarmer-compare-with-first "benchmark"
                    (benchwarmer-run "first" 1 (error "first error"))
                    (benchwarmer-run "second" 1 (- 1 2 3))))))
    (should (stringp result))
    (should (string-match-p "^((:compare-with-first \. \"benchmark\")
 ((\"first\"
   (:error \"first error\"))
  (\"second\"
   (.*? .*? .*?))))$" result))))

(ert-deftest test--benchwarmer-compare-with-first--second-error ()
  (benchwarmer-initialize-output-functions)
  (let ((result (with-output-to-string
                  (benchwarmer-compare-with-first "benchmark"
                    (benchwarmer-run "first" 1 (+ 1 2 3))
                    (benchwarmer-run "second" 1 (error "second error"))))))
    (should (stringp result))
    (should (string-match-p "^((:compare-with-first \. \"benchmark\")
 ((\"first\"
   (.*? .*? .*?))
  (\"second\"
   (:error \"second error\"))))$" result))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-comparison-name ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "NAME: Argument value < 8 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first 8
                        (benchwarmer-run "first" 1 (+ 1 2 3))
                        (benchwarmer-run "second" 1 (- 1 2 3))
                        (benchwarmer-run "third" 1 (* 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-first-form ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORMS: Argument sub-element < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "benchmark"
                        8
                        (benchwarmer-run "second" 1 (- 1 2 3))
                        (benchwarmer-run "third" 1 (* 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-second-form ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORMS: Argument sub-element < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "benchmark"
                        (benchwarmer-run "first" 1 (+ 1 2 3))
                        8
                        (benchwarmer-run "third" 1 (* 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-third-form ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORMS: Argument sub-element < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "benchmark"
                        (benchwarmer-run "first" 1 (+ 1 2 3))
                        (benchwarmer-run "second" 1 (- 1 2 3))
                        8))))))

(ert-deftest test--benchwarmer-compare-with-first--error-evaluating-first ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > evaluates to error: < Invalid function: 8 >.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--error-evaluating-second ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > evaluates to error: < Invalid function: 8 >.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (benchwarmer-run "first" 1 (+ 1 2 3))
                        (8)))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-list ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > is not a valid benchwarmer result, < (8) > is not a list of length 2.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-name ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (7 8) > is not a valid benchwarmer result, < 7 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list 7 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-benchmark ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (\"benchmark\" 8) > is not a valid benchwarmer result, < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list "benchmark" 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-metrics-list ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (\"benchmark\" (8)) > is not a valid benchwarmer result, < (8) > is not a list of length 3.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list "benchmark" '(8))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-metrics-list-fist ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (\"7\" \"8\" \"9\")) > is not a valid benchwarmer result, < \"7\" > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list "benchmark" '("7" "8" "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-metrics-list-second ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (7.0 \"8\" \"9\")) > is not a valid benchwarmer result, < \"8\" > is not a integer.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list "benchmark" '(7.0 "8" "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare-with-first--invalid-result-metrics-list-third ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (7.0 8 \"9\")) > is not a valid benchwarmer result, < \"9\" > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-with-first "name"
                        (list "benchmark" '(7.0 8 "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-run--success ()
  (let ((result (benchwarmer-run "name" 1 (+ 1 2 3))))
    (assert-benchwarmer-result result "name")))

(ert-deftest test--benchwarmer-run--error ()
  (let* ((expected '("name" (:error "error message")))
          (result (benchwarmer-run "name" 1 (error "error message"))))
    (assert-benchwarmer-result result "name")
    (should (equal expected result))))

(ert-deftest test--benchwarmer-run--wrong-name ()
  (let* ((expected '("NAME ERROR" (:error "NAME: Argument value < :wrong > is not a string.")))
         (result (benchwarmer-run :wrong 1 (+ 1 2 3))))
    (should (equal expected result))))

(ert-deftest test--benchwarmer-run--wrong-times ()
  (let* ((expected '("name" (:error "TIMES: Argument value < :wrong > is not an integer.")))
          (result (benchwarmer-run "name" :wrong (+ 1 2 3))))
    (should (equal expected result))))

(ert-deftest test--benchwarmer-run--wrong-form ()
  (let* ((expected '("name" (:error "FORM: Argument value < :wrong > is not a list.")))
          (result (benchwarmer-run "name" 1 :wrong)))
    (should (equal expected result))))

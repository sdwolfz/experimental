(ert-deftest test--benchwarmer-compare--success ()
  (benchwarmer-initialize-output-functions)
  (let ((result
          (with-output-to-string
            (benchwarmer-compare "benchmark"
              (benchwarmer-run "first" 1 (+ 1 2 3))
              (benchwarmer-run "second" 1 (- 1 2 3))))))
    (should (stringp result))
    (should (string-match-p "^((:compare \. \"benchmark\")
 ((\"first\"
   (.*? .*? .*?))
  (\"second\"
   (.*? .*? .*?)
   (:diff
    ((.*? .*? .*?)
     (.*? .*? .*?))))))$" result))))

(ert-deftest test--benchwarmer-compare--first-error ()
  (benchwarmer-initialize-output-functions)
  (let* ((result
           (with-output-to-string
             (benchwarmer-compare "benchmark"
               (benchwarmer-run "first" 1 (error "first error"))
               (benchwarmer-run "second" 1 (- 1 2 3))))))
    (should (stringp result))
    (should (string-match-p "^((:compare \. \"benchmark\")
 ((\"first\"
   (:error \"first error\"))
  (\"second\"
   (.*? .*? .*?))))$" result))))

(ert-deftest test--benchwarmer-compare--second-error ()
  (benchwarmer-initialize-output-functions)
  (let* ((result
           (with-output-to-string
             (benchwarmer-compare "benchmark"
               (benchwarmer-run "first" 1 (+ 1 2 3))
               (benchwarmer-run "second" 1 (error "second error"))))))
    (should (stringp result))
    (should (string-match-p "^((:compare \. \"benchmark\")
 ((\"first\"
   (.*? .*? .*?))
  (\"second\"
   (:error \"second error\"))))$" result))))

(ert-deftest test--benchwarmer-compare--invalid-comparison-name ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "NAME: Argument value < 1 > is not a string.")))
    (should
      (equal expected
        (should-error
          (benchwarmer-compare 1
            (benchwarmer-run "first" 1 (+ 1 2 3))
            (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-list-first ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FIRST: Argument value < 8 > is not a list.")))
    (should
      (equal expected
        (should-error
          (benchwarmer-compare "name"
            8
            (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-list-second ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "SECOND: Argument value < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (benchwarmer-run "first" 1 (+ 1 2 3))
                        8))))))

(ert-deftest test--benchwarmer-compare--error-evaluating-first ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > evaluates to error: < Invalid function: 8 >.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--error-evaluating-second ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > evaluates to error: < Invalid function: 8 >.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (benchwarmer-run "name" 1 (+ 1 2 3))
                        (8)))))))

(ert-deftest test--benchwarmer-compare--invalid-result-list ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (8) > is not a valid benchwarmer result, < (8) > is not a list of length 2.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-name ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (7 8) > is not a valid benchwarmer result, < 7 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list 7 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-benchmark ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" 8) > is not a valid benchwarmer result, < 8 > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list "benchmark" 8)
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-metrics-list ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (8)) > is not a valid benchwarmer result, < (8) > is not a list of length 3.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list "benchmark" '(8))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-metrics-list-fist ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (\"7\" \"8\" \"9\")) > is not a valid benchwarmer result, < \"7\" > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list "benchmark" '("7" "8" "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-metrics-list-second ()
  (benchwarmer-initialize-output-functions)
  (let
    ((expected
       '(error "FORM: < (\"benchmark\" (7.0 \"8\" \"9\")) > is not a valid benchwarmer result, < \"8\" > is not a integer.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list "benchmark" '(7.0 "8" "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

(ert-deftest test--benchwarmer-compare--invalid-result-metrics-list-third ()
  (benchwarmer-initialize-output-functions)
  (let ((expected '(error "FORM: < (\"benchmark\" (7.0 8 \"9\")) > is not a valid benchwarmer result, < \"9\" > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare "name"
                        (list "benchmark" '(7.0 8 "9"))
                        (benchwarmer-run "second" 1 (- 1 2 3))))))))

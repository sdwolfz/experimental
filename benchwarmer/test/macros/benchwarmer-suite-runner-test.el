(ert-deftest test--benchwarmer-suite-runner--success ()
  (setq argv '("--" ".*"))
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "suite-runner")
  (benchwarmer-suite-runner
    (:suites "./test/fixtures/runner/*-suite.el"))
  (let* ((path "./test/tmp/suite-runner")
          (result (with-temp-buffer
                    (insert-file-contents path)
                    (read (current-buffer)))))
    (assert-benchwarmer-report result benchwarmer-report-name)))

(ert-deftest test--benchwarmer-suite-runner--wrong-args ()
  (let* ((expected '(error "ARGS: Argument sub-element < 8 > is not a list.")))
    (should (equal expected
              (should-error (benchwarmer-suite-runner 8))))))

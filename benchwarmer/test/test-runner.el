(load-file "test/test-helper.el")

(defvar test-matching-glob "./test/**/*-test.el"
  "Glob pattern used to find test files.")

(defvar test-matching-pattern ".*"
  "Regex used to select which tests to run.")

;; Get a new value for `test-matching-pattern' from the cli.
(while argv
  (let ((option (pop argv)))
    (cond
      ((string= option "--")
        (setq test-matching-pattern (pop argv))))))

;; Load all matching tests.
(let ((files (file-expand-wildcards test-matching-glob)))
  (dolist (file files)
    (when (string-match-p test-matching-pattern file)
      (load-file file))))

;; Execute tests.
(ert-run-tests-batch-and-exit)

(add-to-list 'load-path ".")

(require 'ert)

;; Install `undercover' if running on Travis.
(when (string= (getenv "TRAVIS") "true")
  (require 'package)
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
  (package-initialize)
  (package-refresh-contents)

  ;; Install and configure undercover for coverage reports.
  (unless (package-installed-p 'undercover)
    (package-install 'undercover))
  (require 'undercover)
  (undercover "benchwarmer.el"
    (:report-file "/tmp/undercover-report.json")))

(require 'benchwarmer)

;; Custom assertions ------------------------------------------------------------

(defun assert-benchmark-result (result)
  "Assert that `RESULT' represents a benchmark result.

A valid result is a list with 3 numbers:
1. A float representing the benchmark elapsed time.
2. A integer representing the number of gc runs.
3. A float representing the elapsed time doing gc.

Example:
```emacs-lisp
(1.1 2 3.3)
```"
  (should (listp result))
  (should (eq 3 (length result)))
  (let ((first (car result))
        (second (nth 1 result))
        (third (nth 2 result)))
    (should (floatp first))
    (should (integerp second))
    (should (floatp third))))

(defun assert-benchwarmer-result (result &optional name)
  "Assert that `RESULT' represents a benchwamer result with the given `NAME'.

There are two types of acceptable results:

1. A valid result is a list with 2 element:
  1.1 A string representing the name of the result.
  1.2 A benchmark result (see above).
2. An error result is a list with 2 elements:
  2.1 The `:error' symbol.
  2.2 A string representing the error message.

Example:
```emacs-lisp
;; 1. A valid result:
\(\"name\" (1.1 2 3.3))

;; 2. An error result:
\(\"name\" (:error \"error message\"))
```"
  (should (listp result))
  (should (= 2 (length result)))
  (let* ((result-name (car result))
          (result-body (nth 1 result))
          (first-body-element (car result-body)))
    (should (stringp result-name))
    (should (or (floatp first-body-element) (symbolp first-body-element)))
    (cond
      ((floatp first-body-element)
        (when name
          (should (string= name result-name)))
        (assert-benchmark-result result-body))
      ((symbolp first-body-element)
        (should (= 2 (length result-body)))
        (should (equal :error (car result-body)))
        (should (stringp (nth 1 result-body)))))))

(defun assert-benchwarmer-suite (suite &optional name benchmark-names)
  "Assert a `SUITE' with `NAME' containing benchmarks with `BENCHMARK-NAMES'.

A valid suite is a list containing the following:
1. A string representing the name of the suite.
2. Zero or more benchwarmer results (see above).

Examples:
```emacs-lisp
# An empty suite:
(\"empty\")

# One result suite:
(\"one\"
  (\"+\" (1.1 2 3.3)))

# Two result suite:
(\"two\"
  (\"+\" (1.1 2 3.3))
  (\"-\" (4.5 6 7.8)))
```"
  (should (listp suite))
  (let ((suite-name (car suite))
         (suite-body (cdr suite)))
    (when name
      (should (string= name suite-name)))
    (should (listp suite-body))
    (when benchmark-names
      (should (= (length benchmark-names) (length suite-body))))
    (let ((current-name (car benchmark-names))
           (other-names (cdr benchmark-names)))
      (dolist (result suite-body)
        (assert-benchwarmer-result result current-name)
        (setq current-name (car other-names))
        (setq other-names (cdr other-names))))))

(defun assert-benchwarmer-report (report &optional name suite-names)
  "Assert a `REPORT' with `NAME' containing suites with `SUITE-NAMES'.

A valid report is a list containing the following:
1. A string representing the name of the report.
2. Zero or more benchwarmer suites (see above).

Examples:

```emacs-lisp
# An empty report:
(\"empty\")

# One suite report:
(\"one\"
  (\"suite\"
    (\"+\" (1.1 2 3.3))))

# Two suite report:
(\"two\"
  (\"suite 1\"
    (\"+\" (1.1 2 3.3)))
  (\"suite 2\"
    (\"-\" (4.5 6 7.8))))
```"
  (should (listp report))
  (let ((report-name (car report))
         (report-body (cdr report)))
    (when name
      (should (string= name report-name)))
    (should (listp report-body))
    (when suite-names
      (should (= (length suite-names) (length report-body))))
    (let ((current-name (car suite-names))
           (other-names (cdr suite-names)))
      (dolist (suite report-body)
        (assert-benchwarmer-suite suite (car current-name) (cdr current-name))
        (setq current-name (car other-names))
        (setq other-names (cdr other-names))))))

;; TODO: see if the below functions are still needed.
(defun assert-benchwarmer-diff (diff)
  (should (listp diff))
  (should (eq 2 (length diff)))
  (let ((result (car diff))
        (percent (nth 1 diff)))
    (should (listp result))
    (should (eq 3 (length result)))
    (let ((first (car result))
          (second (nth 1 result))
          (third  (nth 2 result)))
      (should (floatp first))
      (should (integerp second))
      (should (floatp third)))
    (should (listp percent))
    (should (eq 3 (length percent)))
    (let ((first (car percent))
          (second (nth 1 percent))
          (third  (nth 2 percent)))
      (should (floatp first))
      (should (floatp second))
      (should (floatp third)))))

(defun assert-comparison-result (comparison-name first-name second-name result)
  (should (listp result))
  (should (eq 2 (length result)))
  (let ((name (car result))
        (body (nth 1 result)))
    (should (listp body))
    (should (eq 3 (length body)))
    (let ((first (car body))
          (second (nth 1 body))
          (diff (nth 2 body)))
      (assert-benchwarmer-result first-name first)
      (assert-benchwarmer-result second-name second)
      (let ((diff-name (car diff))
            (diff-body (nth 1 diff)))
        (should (string= diff-name "diff"))
        (assert-benchwarmer-diff diff-body)))))

(ert-deftest test--benchwarmer--write-report-file ()
  (setq benchwarmer-report-directory "./test/tmp")
  (let ((file-path "./test/tmp/write")
         (content '("first"
                     ("one"
                       ("1234" (1.0 0 0.0))
                       ("123" (2.0 0 0.0))
                       ("124" (3.0 0 0.0))
                       ("134" (4.0 0 0.0))
                       ("12" (5.0 0 0.0))
                       ("13" (6.0 0 0.0))
                       ("14" (7.0 0 0.0))
                       ("1" (8.0 0 0.0)))
                     ("two"
                       ("1234" (11.0 0 0.0))
                       ("123" (22.0 0 0.0))
                       ("124" (33.0 0 0.0))
                       ("234" (44.0 0 0.0))
                       ("12" (55.0 0 0.0))
                       ("23" (66.0 0 0.0))
                       ("24" (77.0 0 0.0))
                       ("2" (88.0 0 0.0))))))
    (assert-benchwarmer-report
      content
      "first"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")
        (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
    (benchwarmer--write-report-file file-path content)
    (let ((result (with-temp-buffer
                    (insert-file-contents file-path)
                    (read (current-buffer)))))
      (assert-benchwarmer-report
        content
        "first"
        (list
          (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")
          (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
      (should (equal content result)))))

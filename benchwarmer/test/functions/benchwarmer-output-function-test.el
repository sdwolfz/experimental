(ert-deftest test--benchwarmer-output-function--get ()
  (benchwarmer-initialize-output-functions)
  (let* ((expected 'pp)
         (result (benchwarmer-output-function :compare)))
    (should (equal expected result))))

(ert-deftest test--benchwarmer-output-function--set ()
  (benchwarmer-initialize-output-functions)
  (let* ((new-function 'princ)
          (new-lambda (lambda (report) (printc report))))
    (benchwarmer-output-function :compare new-function)
    (should (equal new-function (benchwarmer-output-function :compare)))
    (benchwarmer-output-function :compare new-lambda)
    (should (equal new-lambda (benchwarmer-output-function :compare)))))

(ert-deftest test--benchwarmer-output-function--wrong-key ()
  (benchwarmer-initialize-output-functions)
  (let* ((key :this-is-a-wrong-key)
          (expected '(error "KEY: :this-is-a-wrong-key is not a valid output function type.")))
    (should
      (equal expected
        (should-error (benchwarmer-output-function key) :type 'error)))
    (should
      (equal expected
        (should-error (benchwarmer-output-function key 'pp) :type 'error)))))

(ert-deftest test--benchwarmer-output-function--wrong-value ()
  (benchwarmer-initialize-output-functions)
  (let* ((key :compare)
          (first-wrong-function 123)
          (second-wrong-function '(123))
          (first-expected '(error "FUNCTION: 123 is not a symbol nor a lambda."))
          (second-expected '(error "FUNCTION: (123) is not a symbol nor a lambda.")))
    (should
      (equal first-expected
        (should-error
          (benchwarmer-output-function key first-wrong-function) :type 'error)))
    (should
      (equal second-expected
        (should-error
          (benchwarmer-output-function key second-wrong-function) :type 'error)))))

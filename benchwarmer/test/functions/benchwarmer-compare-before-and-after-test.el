(ert-deftest test--benchwarmer-compare-before-and-after--with-custom-output-function ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (benchwarmer-output-function :compare-before-and-after
    (lambda (report) (princ "dummy")))
  (let* ((first "before")
         (second "after")
         (expected "dummy")
         (result (with-output-to-string
                   (benchwarmer-compare-before-and-after))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-before-and-after--with-default-output-function ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "before")
         (second "after")
          (expected "((\"before\" \"after\")
 (\"suite\"
  (\"+\"
   (\"math\"
    (1.0 2 3.0))
   (\"math\"
    (15.0 21 31.0)
    (:diff
     ((14.0 19 28.0)
      (1400.0 950.0 933.3333333333334)))))
  (\"-\"
   (\"math\"
    (2.0 3 4.0))
   (\"math\"
    (22.0 31 49.0)
    (:diff
     ((20.0 28 45.0)
      (1000.0 933.3333333333334 1125.0)))))
  (\"*\"
   (\"math\"
    (3.0 4 5.0))
   (\"math\"
    (33.0 45 53.0)
    (:diff
     ((30.0 41 48.0)
      (1000.0 1025.0 960.0)))))
  (\"/\"
   (\"math\"
    (4.0 5 6.0))
   (\"math\"
    (48.0 57 62.0)
    (:diff
     ((44.0 52 56.0)
      (1100.0 1040.0 933.3333333333334)))))))
")
          (result (with-output-to-string
                    (benchwarmer-compare-before-and-after))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports-mixed ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first-name "first")
         (second-name "second")
         (third-name "third")
         (fourth-name "fourth")
         (expected "((\"first\" \"second\" \"third\" \"fourth\")
 (\"benchmark\"
  (\"1234\"
   (\"first\"
    (1.0 0 0.0))
   (\"second\"
    (11.0 0 0.0)
    (:diff-with-first
     ((10.0 0 0.0)
      (1000.0 nil nil)))
    (:diff-with-previous
     ((10.0 0 0.0)
      (1000.0 nil nil))))
   (\"third\"
    (111.0 0 0.0)
    (:diff-with-first
     ((110.0 0 0.0)
      (11000.0 nil nil)))
    (:diff-with-previous
     ((100.0 0 0.0)
      (909.0909090909091 nil nil))))
   (\"fourth\"
    (1111.0 0 0.0)
    (:diff-with-first
     ((1110.0 0 0.0)
      (111000.0 nil nil)))
    (:diff-with-previous
     ((1000.0 0 0.0)
      (900.900900900901 nil nil)))))
  (\"123\"
   (\"first\"
    (2.0 0 0.0))
   (\"second\"
    (22.0 0 0.0)
    (:diff-with-first
     ((20.0 0 0.0)
      (1000.0 nil nil)))
    (:diff-with-previous
     ((20.0 0 0.0)
      (1000.0 nil nil))))
   (\"third\"
    (222.0 0 0.0)
    (:diff-with-first
     ((220.0 0 0.0)
      (11000.0 nil nil)))
    (:diff-with-previous
     ((200.0 0 0.0)
      (909.0909090909091 nil nil)))))
  (\"124\"
   (\"first\"
    (3.0 0 0.0))
   (\"second\"
    (33.0 0 0.0)
    (:diff-with-first
     ((30.0 0 0.0)
      (1000.0 nil nil)))
    (:diff-with-previous
     ((30.0 0 0.0)
      (1000.0 nil nil))))
   (\"fourth\"
    (2222.0 0 0.0)
    (:diff-with-first
     ((2219.0 0 0.0)
      (73966.66666666666 nil nil)))
    (:diff-with-previous
     ((2189.0 0 0.0)
      (6633.333333333333 nil nil)))))
  (\"134\"
   (\"first\"
    (4.0 0 0.0))
   (\"third\"
    (333.0 0 0.0)
    (:diff-with-first
     ((329.0 0 0.0)
      (8225.0 nil nil)))
    (:diff-with-previous
     ((329.0 0 0.0)
      (8225.0 nil nil))))
   (\"fourth\"
    (3333.0 0 0.0)
    (:diff-with-first
     ((3329.0 0 0.0)
      (83225.0 nil nil)))
    (:diff-with-previous
     ((3000.0 0 0.0)
      (900.900900900901 nil nil)))))
  (\"12\"
   (\"first\"
    (5.0 0 0.0))
   (\"second\"
    (55.0 0 0.0)
    (:diff-with-first
     ((50.0 0 0.0)
      (1000.0 nil nil)))
    (:diff-with-previous
     ((50.0 0 0.0)
      (1000.0 nil nil)))))
  (\"13\"
   (\"first\"
    (6.0 0 0.0))
   (\"third\"
    (555.0 0 0.0)
    (:diff-with-first
     ((549.0 0 0.0)
      (9150.0 nil nil)))
    (:diff-with-previous
     ((549.0 0 0.0)
      (9150.0 nil nil)))))
  (\"14\"
   (\"first\"
    (7.0 0 0.0))
   (\"fourth\"
    (5555.0 0 0.0)
    (:diff-with-first
     ((5548.0 0 0.0)
      (79257.14285714286 nil nil)))
    (:diff-with-previous
     ((5548.0 0 0.0)
      (79257.14285714286 nil nil)))))
  (\"1\"
   (\"first\"
    (8.0 0 0.0)))
  (\"234\"
   (\"second\"
    (44.0 0 0.0))
   (\"third\"
    (444.0 0 0.0)
    (:diff-with-first
     ((400.0 0 0.0)
      (909.0909090909091 nil nil)))
    (:diff-with-previous
     ((400.0 0 0.0)
      (909.0909090909091 nil nil))))
   (\"fourth\"
    (4444.0 0 0.0)
    (:diff-with-first
     ((4400.0 0 0.0)
      (10000.0 nil nil)))
    (:diff-with-previous
     ((4000.0 0 0.0)
      (900.900900900901 nil nil)))))
  (\"23\"
   (\"second\"
    (66.0 0 0.0))
   (\"third\"
    (666.0 0 0.0)
    (:diff-with-first
     ((600.0 0 0.0)
      (909.0909090909091 nil nil)))
    (:diff-with-previous
     ((600.0 0 0.0)
      (909.0909090909091 nil nil)))))
  (\"24\"
   (\"second\"
    (77.0 0 0.0))
   (\"fourth\"
    (6666.0 0 0.0)
    (:diff-with-first
     ((6589.0 0 0.0)
      (8557.142857142857 nil nil)))
    (:diff-with-previous
     ((6589.0 0 0.0)
      (8557.142857142857 nil nil)))))
  (\"2\"
   (\"second\"
    (88.0 0 0.0)))
  (\"34\"
   (\"third\"
    (777.0 0 0.0))
   (\"fourth\"
    (7777.0 0 0.0)
    (:diff-with-first
     ((7000.0 0 0.0)
      (900.900900900901 nil nil)))
    (:diff-with-previous
     ((7000.0 0 0.0)
      (900.900900900901 nil nil)))))
  (\"3\"
   (\"third\"
    (888.0 0 0.0)))
  (\"4\"
   (\"fourth\"
    (8888.0 0 0.0)))))
")
         (result (with-output-to-string
                   (benchwarmer-compare-reports-mixed first-name second-name third-name fourth-name))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports-mixed--invalid-first-name ()
  (benchwarmer-initialize-output-functions)
  (let* ((first 8)
          (second "second")
          (expected '(error "FILE-NAMES: Argument sub-element < 8 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-reports-mixed first second))))))

(ert-deftest test--benchwarmer-compare-reports-mixed--invalid-second-name ()
  (benchwarmer-initialize-output-functions)
  (let* ((first "first")
          (second 8)
          (expected '(error "FILE-NAMES: Argument sub-element < 8 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-reports-mixed first second))))))

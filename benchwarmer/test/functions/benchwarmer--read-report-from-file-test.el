(ert-deftest test--benchwarmer--read-report-from-file--success ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (let* ((report-name "benchmark")
          (expected '("benchmark"
                       ("one"
                         ("1234" (1.0 0 0.0))
                         ("123" (2.0 0 0.0))
                         ("124" (3.0 0 0.0))
                         ("134" (4.0 0 0.0))
                         ("12" (5.0 0 0.0))
                         ("13" (6.0 0 0.0))
                         ("14" (7.0 0 0.0))
                         ("1" (8.0 0 0.0)))
                       ("two"
                         ("1234" (11.0 0 0.0))
                         ("123" (22.0 0 0.0))
                         ("124" (33.0 0 0.0))
                         ("234" (44.0 0 0.0))
                         ("12" (55.0 0 0.0))
                         ("23" (66.0 0 0.0))
                         ("24" (77.0 0 0.0))
                         ("2" (88.0 0 0.0)))))
         (actual (benchwarmer--read-report-from-file report-name)))
    (assert-benchwarmer-report
      expected
      "benchmark"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")
        (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
    (assert-benchwarmer-report
      actual
      "benchmark"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")
        (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
    (should (equal expected actual))))

(ert-deftest test--benchwarmer--read-report-from-file--invalid-report ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (let* ((report-name "invalid-report")
         (expected '(:error "Report file ./test/fixtures/invalid-report is not a valid report, < this > is not a list."))
         (actual (benchwarmer--read-report-from-file report-name)))
    (should (equal expected actual))))

(ert-deftest test--benchwarmer--read-report-from-file--empty-file ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (let* ((report-name "empty-file")
          (expected '(:error "Report file ./test/fixtures/empty-file is empty."))
          (actual (benchwarmer--read-report-from-file report-name)))
    (should (equal expected actual))))

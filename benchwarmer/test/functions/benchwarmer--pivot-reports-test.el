(ert-deftest test--benchwarmer--pivot-reports--no-reports ()
  (let* ((reports '())
          (expected '())
          (result (benchwarmer--pivot-reports reports)))
    (should (equal expected result))))

(ert-deftest test--benchwarmer--pivot-reports--one-report-one-suite ()
  (let* ((reports '(("first"
                      ("one"
                        ("1234" (1.0 0 0.0))
                        ("123" (2.0 0 0.0))
                        ("124" (3.0 0 0.0))
                        ("134" (4.0 0 0.0))
                        ("12" (5.0 0 0.0))
                        ("13" (6.0 0 0.0))
                        ("14" (7.0 0 0.0))
                        ("1" (8.0 0 0.0))))))
          (expected '((("one"
                         ("1234" ("first" (1.0 0 0.0)))
                         ("123" ("first" (2.0 0 0.0)))
                         ("124" ("first" (3.0 0 0.0)))
                         ("134" ("first" (4.0 0 0.0)))
                         ("12" ("first" (5.0 0 0.0)))
                         ("13" ("first" (6.0 0 0.0)))
                         ("14" ("first" (7.0 0 0.0)))
                         ("1" ("first" (8.0 0 0.0)))))))
          (result (benchwarmer--pivot-reports reports)))
    (assert-benchwarmer-report
      (car reports)
      "first"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")))
    (should (equal expected result))))

(ert-deftest test--benchwarmer--pivot-reports--one-report-two-suites ()
  (let* ((reports '(("first"
                      ("one"
                        ("1234" (1.0 0 0.0))
                        ("123" (2.0 0 0.0))
                        ("124" (3.0 0 0.0))
                        ("134" (4.0 0 0.0))
                        ("12" (5.0 0 0.0))
                        ("13" (6.0 0 0.0))
                        ("14" (7.0 0 0.0))
                        ("1" (8.0 0 0.0)))
                      ("two"
                        ("1234" (11.0 0 0.0))
                        ("123" (22.0 0 0.0))
                        ("124" (33.0 0 0.0))
                        ("234" (44.0 0 0.0))
                        ("12" (55.0 0 0.0))
                        ("23" (66.0 0 0.0))
                        ("24" (77.0 0 0.0))
                        ("2" (88.0 0 0.0))))))
          (expected '((("one"
                         ("1234" ("first" (1.0 0 0.0)))
                         ("123" ("first" (2.0 0 0.0)))
                         ("124" ("first" (3.0 0 0.0)))
                         ("134" ("first" (4.0 0 0.0)))
                         ("12" ("first" (5.0 0 0.0)))
                         ("13" ("first" (6.0 0 0.0)))
                         ("14" ("first" (7.0 0 0.0)))
                         ("1" ("first" (8.0 0 0.0))))
                        ("two"
                          ("1234" ("first" (11.0 0 0.0)))
                          ("123" ("first" (22.0 0 0.0)))
                          ("124" ("first" (33.0 0 0.0)))
                          ("234" ("first" (44.0 0 0.0)))
                          ("12" ("first" (55.0 0 0.0)))
                          ("23" ("first" (66.0 0 0.0)))
                          ("24" ("first" (77.0 0 0.0)))
                          ("2" ("first" (88.0 0 0.0)))))))
          (result (benchwarmer--pivot-reports reports)))
    (assert-benchwarmer-report
      (car reports)
      "first"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")
        (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
    (should (equal expected result))))

(ert-deftest test--benchwarmer--pivot-reports--two-reports-one-suite-each ()
  (let* ((reports '(("first"
                      ("one"
                        ("1234" (1.0 0 0.0))
                        ("123" (2.0 0 0.0))
                        ("124" (3.0 0 0.0))
                        ("134" (4.0 0 0.0))
                        ("12" (5.0 0 0.0))
                        ("13" (6.0 0 0.0))
                        ("14" (7.0 0 0.0))
                        ("1" (8.0 0 0.0))))
                     ("second"
                       ("two"
                         ("1234" (11.0 0 0.0))
                         ("123" (22.0 0 0.0))
                         ("124" (33.0 0 0.0))
                         ("234" (44.0 0 0.0))
                         ("12" (55.0 0 0.0))
                         ("23" (66.0 0 0.0))
                         ("24" (77.0 0 0.0))
                         ("2" (88.0 0 0.0))))))
          (expected '((("one"
                         ("1234" ("first" (1.0 0 0.0)))
                         ("123" ("first" (2.0 0 0.0)))
                         ("124" ("first" (3.0 0 0.0)))
                         ("134" ("first" (4.0 0 0.0)))
                         ("12" ("first" (5.0 0 0.0)))
                         ("13" ("first" (6.0 0 0.0)))
                         ("14" ("first" (7.0 0 0.0)))
                         ("1" ("first" (8.0 0 0.0)))))
                       (("two"
                          ("1234" ("second" (11.0 0 0.0)))
                          ("123" ("second" (22.0 0 0.0)))
                          ("124" ("second" (33.0 0 0.0)))
                          ("234" ("second" (44.0 0 0.0)))
                          ("12" ("second" (55.0 0 0.0)))
                          ("23" ("second" (66.0 0 0.0)))
                          ("24" ("second" (77.0 0 0.0)))
                          ("2" ("second" (88.0 0 0.0)))))))
          (result (benchwarmer--pivot-reports reports)))
    (assert-benchwarmer-report
      (car reports)
      "first"
      (list
        (list "one" "1234" "123" "124" "134" "12" "13" "14" "1")))
    (assert-benchwarmer-report
      (nth 1 reports)
      "second"
      (list
        (list "two" "1234" "123" "124" "234" "12" "23" "24" "2")))
    (should (equal expected result))))

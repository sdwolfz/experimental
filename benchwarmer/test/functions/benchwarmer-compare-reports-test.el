(ert-deftest test--benchwarmer-compare-reports--both-empty ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "before-empty")
         (second "after-empty")
          (expected "((\"before-empty\" \"after-empty\"))
")
         (result (with-output-to-string
                   (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--one-in-first-none-in-second ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "only-one")
          (second "after-empty")
          (expected "((\"only-one\" \"after-empty\")
 (\"suite\"
  (\"+\"
   (\"only-one\"
    (0.023921592000000002 0 0.0)))))
")
          (result (with-output-to-string
                    (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--none-in-first-one-in-second ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "before-empty")
          (second "only-one")
          (expected "((\"before-empty\" \"only-one\")
 (\"suite\"
  (\"+\"
   (\"only-one\"
    (0.023921592000000002 0 0.0)))))
")
          (result (with-output-to-string
                    (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--full ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "before-mixed")
          (second "after-mixed")
          (expected "((\"before-mixed\" \"after-mixed\")
 (\"suite\"
  (\"+\"
   (\"before-mixed\"
    (0.024720943999999998 0 0.0))
   (\"after-mixed\"
    (0.027677465000000002 0 0.0)
    (:diff
     ((0.0029565210000000036 0 0.0)
      (11.959579698898246 nil nil)))))
  (\"-\"
   (\"before-mixed\"
    (0.010680181 0 0.0)))
  (\"*\"
   (\"after-mixed\"
    (0.011984365 0 0.0)))))
")
          (result (with-output-to-string
                    (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--with-custom-output-function ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (benchwarmer-output-function :compare-reports
    (lambda (report) (princ "dummy")))
  (let* ((first "before")
         (second "after")
         (expected "dummy")
         (result (with-output-to-string
                   (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--with-default-output-function ()
  (setq benchwarmer-report-directory "./test/fixtures")
  (benchwarmer-initialize-output-functions)
  (let* ((first "before")
         (second "after")
          (expected "((\"before\" \"after\")
 (\"suite\"
  (\"+\"
   (\"math\"
    (1.0 2 3.0))
   (\"math\"
    (15.0 21 31.0)
    (:diff
     ((14.0 19 28.0)
      (1400.0 950.0 933.3333333333334)))))
  (\"-\"
   (\"math\"
    (2.0 3 4.0))
   (\"math\"
    (22.0 31 49.0)
    (:diff
     ((20.0 28 45.0)
      (1000.0 933.3333333333334 1125.0)))))
  (\"*\"
   (\"math\"
    (3.0 4 5.0))
   (\"math\"
    (33.0 45 53.0)
    (:diff
     ((30.0 41 48.0)
      (1000.0 1025.0 960.0)))))
  (\"/\"
   (\"math\"
    (4.0 5 6.0))
   (\"math\"
    (48.0 57 62.0)
    (:diff
     ((44.0 52 56.0)
      (1100.0 1040.0 933.3333333333334)))))))
")
          (result (with-output-to-string
                    (benchwarmer-compare-reports first second))))
    (should (string= expected result))))

(ert-deftest test--benchwarmer-compare-reports--invalid-first-name ()
  (benchwarmer-initialize-output-functions)
  (let* ((first 8)
          (second "second")
          (expected '(error "FIRST: Argument value < 8 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-reports first second))))))

(ert-deftest test--benchwarmer-compare-reports--invalid-second-name ()
  (benchwarmer-initialize-output-functions)
  (let* ((first "first")
          (second 8)
          (expected '(error "SECOND: Argument value < 8 > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer-compare-reports first second))))))

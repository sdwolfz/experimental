(ert-deftest test--benchwarmer-report-batch-and-exit--no-suites ()
  (setq benchwarmer--random-seed nil)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-no-suites")
  (when (file-exists-p "./test/tmp/report-no-suites")
    (delete-file "./test/tmp/report-no-suites"))
  (let* ((suite-name "no-suites"))
    (benchwarmer-report-batch-and-exit)
    (let ((expected (list benchwarmer-report-name))
           (result (with-temp-buffer
                     (insert-file-contents (benchwarmer--file-path))
                     (read (current-buffer)))))
      (assert-benchwarmer-report result benchwarmer-report-name)
      (should (equal expected result)))))

(ert-deftest test--benchwarmer-report-batch-and-exit--no-benchmarks ()
  (setq benchwarmer--random-seed nil)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-no-benchmarks")
  (when (file-exists-p "./test/tmp/report-no-benchmarks")
    (delete-file "./test/tmp/report-no-benchmarks"))
  (let* ((suite-name "no-benchmarks"))
    (benchwarmer-suite "no-benchmarks")
    (benchwarmer-report-batch-and-exit)
    (let ((expected (list benchwarmer-report-name (list suite-name)))
          (result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report
        result
        benchwarmer-report-name
        (list (list suite-name)))
      (should (equal expected result)))))

(ert-deftest test--benchwarmer-report-batch-and-exit--one-benchmarks ()
  (setq benchwarmer--random-seed nil)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-one-benchmark")
  (when (file-exists-p "./test/tmp/report-one-benchmark")
    (delete-file "./test/tmp/report-one-benchmark"))
  (let* ((suite-name "one-benchmark")
         (benchmark-name "name"))
    (benchwarmer-suite "one-benchmark"
      (benchwarmer-run "name" 1 (+ 1 2 3)))
    (benchwarmer-report-batch-and-exit)
    (let ((result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report
        result
        benchwarmer-report-name
        (list (list suite-name benchmark-name))))))

(ert-deftest test--benchwarmer-report-batch-and-exit--two-benchmarks ()
  (setq benchwarmer--random-seed nil)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-two-benchmarks")
  (when (file-exists-p "./test/tmp/report-two-benchmarks")
    (delete-file "./test/tmp/report-two-benchmarks"))
  (let ((suite-name "benchmark")
        (first-benchmark-name "first")
        (second-benchmark-name "second"))
    (benchwarmer-suite "benchmark"
      (benchwarmer-run "first" 1 (+ 1 2 3))
      (benchwarmer-run "second" 1 (- 4 5 6)))
    (benchwarmer-report-batch-and-exit)
    (let ((result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report
        result
        benchwarmer-report-name
        (list (list suite-name first-benchmark-name second-benchmark-name))))))

(ert-deftest test--benchwarmer-report-batch-and-exit--two-suites ()
  (setq benchwarmer--random-seed nil)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-two-suites")
  (when (file-exists-p "./test/tmp/report-two-suites")
    (delete-file "./test/tmp/report-two-suites"))
  (let ((first-suite-name "first-suite")
         (first-benchmark-name "first-benchmark")
         (second-suite-name "second-suite")
         (second-benchmark-name "second-benchmark"))
    (benchwarmer-suite "first-suite"
      (benchwarmer-run "first-benchmark" 1 (+ 1 2 3)))
    (benchwarmer-suite "second-suite"
      (benchwarmer-run "second-benchmark" 1 (- 1 2 3)))
    (benchwarmer-report-batch-and-exit)
    (let ((result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report
        result
        benchwarmer-report-name
        (list
          (list first-suite-name first-benchmark-name)
          (list second-suite-name second-benchmark-name))))))

(ert-deftest test--benchwarmer-report-batch-and-exit--random-not-seeded ()
  (setq benchwarmer--random-seed t)
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-two-suites")
  (when (file-exists-p "./test/tmp/report-two-suites")
    (delete-file "./test/tmp/report-two-suites"))
  (let ((first-suite-name "first-suite")
         (first-benchmark-name "first-benchmark")
         (second-suite-name "second-suite")
         (second-benchmark-name "second-benchmark")
         (third-suite-name "third-suite")
         (third-benchmark-name "third-benchmark"))
    (benchwarmer-suite "first-suite"
      (benchwarmer-run "first-benchmark" 1 (+ 1 2 3)))
    (benchwarmer-suite "second-suite"
      (benchwarmer-run "second-benchmark" 1 (- 1 2 3)))
    (benchwarmer-suite "third-suite"
      (benchwarmer-run "third-benchmark" 1 (* 1 2 3)))
    (benchwarmer-report-batch-and-exit)
    (let ((result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report result benchwarmer-report-name))))

(ert-deftest test--benchwarmer-report-batch-and-exit--random-seeded ()
  (setq benchwarmer--random-seed "26058")
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-two-suites")
  (when (file-exists-p "./test/tmp/report-two-suites")
    (delete-file "./test/tmp/report-two-suites"))
  (let ((first-suite-name "first-suite")
         (first-benchmark-name "first-benchmark")
         (second-suite-name "second-suite")
         (second-benchmark-name "second-benchmark")
         (third-suite-name "third-suite")
         (third-benchmark-name "third-benchmark"))
    (benchwarmer-suite "first-suite"
      (benchwarmer-run "first-benchmark" 1 (+ 1 2 3)))
    (benchwarmer-suite "second-suite"
      (benchwarmer-run "second-benchmark" 1 (- 1 2 3)))
    (benchwarmer-suite "third-suite"
      (benchwarmer-run "third-benchmark" 1 (* 1 2 3)))
    (benchwarmer-report-batch-and-exit)
    (let ((result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      (assert-benchwarmer-report result benchwarmer-report-name))))

(ert-deftest test--benchwarmer-report-batch-and-exit--invalid-form ()
  (setq benchwarmer--registered-suites '())
  (setq benchwarmer-report-directory "./test/tmp")
  (setq benchwarmer-report-name "report-error")
  (when (file-exists-p "./test/tmp/report-error")
    (delete-file "./test/tmp/report-error"))
  (let ((first-suite-name "first-suite"))
    (benchwarmer-suite "first-suite" (8))
    (benchwarmer-report-batch-and-exit)
    (let ((expected '("report-error"
                       ("first-suite"
                         (:error "FORM: < (8) > evaluates to error: < Invalid function: 8 >."))))
           (result (with-temp-buffer
                    (insert-file-contents (benchwarmer--file-path))
                    (read (current-buffer)))))
      ;; (assert-benchwarmer-report result benchwarmer-report-name)
      (should (equal expected result)))))

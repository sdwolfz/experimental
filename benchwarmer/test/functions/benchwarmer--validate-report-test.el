(ert-deftest test--benchwarmer--validate-report--no-report ()
  (let ((report 'no-report)
        (path "path/to/file")
        (expected '(error "Report file path/to/file is not a valid report, < no-report > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--empty-report ()
  (let ((report '())
         (path "path/to/file")
         (expected '(error "Report file path/to/file is not a valid report, < nil > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--no-suites ()
  (let* ((report '("name"))
         (path "path/to/file")
         (expected report))
    (should (equal expected (benchwarmer--validate-report report path)))))

(ert-deftest test--benchwarmer--validate-report--wrong-suite ()
  (let* ((report '("name" wrong))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a list.")))
    (should
       (equal expected
         (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-suite-name ()
  (let* ((report '("name" (wrong)))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark ()
  (let* ((report '("report" ("suite" wrong)))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-length ()
  (let* ((report '("report" ("suite" (wrong))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < (wrong) > is not a list of length 2.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-name ()
  (let* ((report '("report" ("suite" (wrong other))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a string.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-body ()
  (let* ((report '("report" ("suite" ("benchmark" other))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < other > is not a list.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-body-length ()
  (let* ((report '("report" ("suite" ("benchmark" (wrong)))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < (wrong) > is not a list of length 3.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-error-symbol ()
  (let* ((report '("report" ("suite" ("benchmark" (wrong "other")))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < (wrong \"other\") > is not an error message.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-error-symbol ()
  (let* ((report '("report" ("suite" ("benchmark" (:error wrong)))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < (:error wrong) > is not an error message.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--benchmark-error-success ()
  (let* ((report '("report" ("suite" ("benchmark" (:error "message")))))
          (path "path/to/file")
          (expected report))
    (should (equal expected (benchwarmer--validate-report report path)))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-time ()
  (let* ((report '("report" ("suite" ("benchmark" (wrong 2 3.3)))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-gc ()
  (let* ((report '("report" ("suite" ("benchmark" (1.1 wrong 3.3)))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a integer.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

(ert-deftest test--benchwarmer--validate-report--wrong-benchmark-gc-time ()
  (let* ((report '("report" ("suite" ("benchmark" (1.1 2 wrong)))))
          (path "path/to/file")
          (expected '(error "Report file path/to/file is not a valid report, < wrong > is not a float.")))
    (should
      (equal expected
        (should-error (benchwarmer--validate-report report path))))))

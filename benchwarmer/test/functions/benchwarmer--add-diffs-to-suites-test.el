(ert-deftest benchwarmer--add-diffs-to-suites--no-reports ()
  (let* ((diff-type :simple)
          (reports '())
          (expected '())
          (result (benchwarmer--add-diffs-to-suites reports diff-type)))
    (should (equal expected result))))

(ert-deftest benchwarmer--add-diffs-to-suites--one-suite-no-groups ()
  (let* ((diff-type :simple)
          (reports '(("one"
                       ("1234" ("first" (1.0 0 0.0)))
                       ("123" ("first" (2.0 0 0.0)))
                       ("124" ("first" (3.0 0 0.0)))
                       ("134" ("first" (4.0 0 0.0)))
                       ("12" ("first" (5.0 0 0.0)))
                       ("13" ("first" (6.0 0 0.0)))
                       ("14" ("first" (7.0 0 0.0)))
                       ("1" ("first" (8.0 0 0.0))))))
          (expected '(("one"
                        ("1234" ("first" (1.0 0 0.0)))
                        ("123" ("first" (2.0 0 0.0)))
                        ("124" ("first" (3.0 0 0.0)))
                        ("134" ("first" (4.0 0 0.0)))
                        ("12" ("first" (5.0 0 0.0)))
                        ("13" ("first" (6.0 0 0.0)))
                        ("14" ("first" (7.0 0 0.0)))
                        ("1" ("first" (8.0 0 0.0))))))
          (result (benchwarmer--add-diffs-to-suites reports diff-type)))
    (should (equal expected result))))

(ert-deftest benchwarmer--add-diffs-to-suites--one-suite-with-groups ()
  (let* ((diff-type :simple)
          (reports '(("one"
                       ("1234"
                         ("first" (1.0 0 0.0))
                         ("second" (11.0 0 0.0)))
                       ("123"
                         ("first" (2.0 0 0.0))
                         ("second" (22.0 0 0.0)))
                       ("124"
                         ("first" (3.0 0 0.0))
                         ("second" (33.0 0 0.0)))
                       ("134" ("first" (4.0 0 0.0)))
                       ("12"
                         ("first" (5.0 0 0.0))
                         ("second" (55.0 0 0.0)))
                       ("13" ("first" (6.0 0 0.0)))
                       ("14" ("first" (7.0 0 0.0)))
                       ("1" ("first" (8.0 0 0.0)))
                       ("234" ("second" (44.0 0 0.0)))
                       ("23" ("second" (66.0 0 0.0)))
                       ("24" ("second" (77.0 0 0.0)))
                       ("2" ("second" (88.0 0 0.0))))))
          (expected '(("one"
                        ("1234"
                          ("first" (1.0 0 0.0))
                          ("second"
                            (11.0 0 0.0)
                            (:diff
                              ((10.0 0 0.0)
                                (1000.0 nil nil)))))
                        ("123"
                          ("first" (2.0 0 0.0))
                          ("second"
                            (22.0 0 0.0)
                            (:diff
                              ((20.0 0 0.0)
                                (1000.0 nil nil)))))
                        ("124"
                          ("first" (3.0 0 0.0))
                          ("second"
                            (33.0 0 0.0)
                            (:diff
                              ((30.0 0 0.0)
                                (1000.0 nil nil)))))
                        ("134" ("first" (4.0 0 0.0)))
                        ("12"
                          ("first" (5.0 0 0.0))
                          ("second"
                            (55.0 0 0.0)
                            (:diff
                              ((50.0 0 0.0)
                                (1000.0 nil nil)))))
                        ("13" ("first" (6.0 0 0.0)))
                        ("14" ("first" (7.0 0 0.0)))
                        ("1" ("first" (8.0 0 0.0)))
                        ("234" ("second" (44.0 0 0.0)))
                        ("23" ("second" (66.0 0 0.0)))
                        ("24" ("second" (77.0 0 0.0)))
                        ("2" ("second" (88.0 0 0.0))))))
          (result (benchwarmer--add-diffs-to-suites reports diff-type)))
    (should (equal expected result))))

(benchwarmer-suite "working"
  (benchwarmer-run "+" 100000 (+ 1 2 3))
  (benchwarmer-run "-" 100000 (- 1 2 3))
  (benchwarmer-run "*" 100000 (* 1 2 3))
  (benchwarmer-run "/" 100000 (/ 1 2 3)))

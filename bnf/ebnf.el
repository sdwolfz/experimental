;; EBNF grammar parser
;;
;; Resources:
;; - https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form

;; Definition:
;; - top-down
;; - C\\\\\an use anything in the second element of the list:
;;    * `::='
;;    * `='
;;    * `asdf'
;; - Since `;' is a comment in lisp it can not be used as a terminal,
;; but since this is lisp we don't need a terminal, we just close the
;; paranthesis (duh)
;; - Some characters need to be escaped inside strings (double duh):
;;    * `"\""' for double quote
;;    * `"\\"' for backslash
;; - Apart from these it's just a matter of copying rules from EBNF and
;; wraping them in paranthesis.

;; Usage

;; The definition for EBNF itself... going full meta here!
(defvar grammar-rules
  '((syntax     ::= { rule })
    (rule       ::= [ whitespace ] , lhs , [ whitespace ]
                  , separator
                  , [ whitespace ] , rhs , [ whitespace ]
                  , finalizer
                  , [ whitespace ])
    (lhs        ::= identifier)
    (rhs        ::= identifier
                  | terminal
                  | "[" , [ whitespace ] , rhs , [ whitespace ] , "]"
                  | "{" , [ whitespace ] , rhs , [ whitespace ] , "}"
                  | "(" , [ whitespace ] , rhs , [ whitespace ] , ")"
                  | rhs , [ whitespace ] , "|" , [ whitespace ] , rhs
                  | rhs , [ whitespace ] , "," , [ whitespace ] , rhs)
    (whitespace ::= { " " | "\n" | "\r" })
    (separator  ::= { ":" } , "=")
    (finalizer  ::= ";")
    (identifier ::= letter , { letter | digit | "_" } )
    (terminal   ::= "'"  , character , { character } , "'"
                  | "\"" , character , { character } , "\"")
    (letter     ::= "A" | "B" | "C" | "D" | "E" | "F" | "G"
                  | "H" | "I" | "J" | "K" | "L" | "M" | "N"
                  | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
                  | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
                  | "c" | "d" | "e" | "f" | "g" | "h" | "i"
                  | "j" | "k" | "l" | "m" | "n" | "o" | "p"
                  | "q" | "r" | "s" | "t" | "u" | "v" | "w"
                  | "x" | "y" | "z" )
    (digit      ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9")
    (symbol     ::= "[" | "]" | "{" | "}" | "(" | ")" | "<" | ">"
                  | "'" | "=" | "|" | "." | "," | ";" | "\"" )))

;; Receives a list of rules and transforms them into a state machine.
(defun ebnf-compile (rules)
  ;; TODO: implement
  (let ((state (make-hash-table :test 'equal)))
    (dolist (rule rules)
      (let ((name (car rule))
            (body (cddr rule)))
        ;; Now transforms body into a tree
        (puthash name (ebnf--rule-list-to-tree body) state)))
    state))

(defun ebnf--rule-list-to-tree (body)
  ())

;; Parses the string using a stat machine and transforms it into an AST.
(defun ebnf-parse (string state-machine)
  ;; TODO: implement
  (let* ((size (length string)))
    (dotimes (index size)
      (let* ((char (aref string index))
             (element (char-to-string char)))
        ;; Do something with each character...
        (message "%S" element)))))

(let ((string "grammar = { rule } ;")
      (state-machine (ebnf-compile grammar-rules))
      (ast (ebnf-parse string state-machine)))
  (message "%S" state-machine)
  (message "%S" ast))

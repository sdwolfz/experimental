;;; actor.el -*- lexical-binding: t -*-
;;
;; Copyright (c) 2018 Codruț Constantin Gușoi
;;
;; Author: Codruț Constantin Gușoi <codrut.gusoi+gitlab.com@gmail.com>
;; URL: https://gitlab.com/sdwolfz/actor.el
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defmacro spawn (body)
  `(funcall
     (lambda ()
       (let* ((actor (list :queue nil :thread nil :mutex nil :context nil :condvar nil))
               (mutex (make-mutex))
               (condvar (make-condition-variable mutex))
               (thread (make-thread (lambda () ,body (thread-yield)))))
         (plist-put actor :thread thread)
         (plist-put actor :mutex mutex)
         (plist-put actor :condvar condvar)
         actor))))

(defun send (actor token)
  (let ((body `(:message . ,token))
         (queue (plist-get actor :queue))
         (mutex (plist-get actor :mutex))
         (condvar (plist-get actor :condvar)))
    (with-mutex mutex
      (push body queue)
      (plist-put actor :queue queue)
      (condition-notify condvar t))))

(defmacro receive (&rest patterns)
  `(let ((condvar (plist-get actor :condvar))
          (head nil))
     (with-mutex mutex
       (while (not (plist-get actor :queue))
         (condition-wait condvar))
       (let ((queue (plist-get actor :queue)))
         (setq head (car queue))
         (plist-put actor :queue (cdr queue))))
     (pcase (cdr head) ,@patterns)))

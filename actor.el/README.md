# `actor.el`

Emacs lisp implementation of the [Actor Model](https://en.wikipedia.org/wiki/Actor_model).

## Usage

```emacs-lisp
(let ((actor
        (spawn (progn
                 (receive (a (message "first message: %S" a)))
                 (receive (a (message "second message: %S" a)))))))
  (send actor :hello)
  (send actor :goodbye))
```

## License

GPLv3

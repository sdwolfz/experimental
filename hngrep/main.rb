#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'nokogiri'

term = Regexp.new(ARGV[0] || '.*?', Regexp::IGNORECASE)

class Finder
  def initialize(term)
    @count = 1
    @term  = term

    @default_headers = {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0'
    }
  end

  def call
    url = 'https://news.ycombinator.com/newest'

    loop do
      body = fetch_page(url)
      more = process_page(body)

      break unless more
      url = "https://news.ycombinator.com/#{more}"

      break if @count > 2000

      sleep 5
    end
  end

  def fetch_page(url)
    use_ssl = true

    result = nil

    uri = URI(url)
    loop do
      Net::HTTP.start(uri.host, uri.port, use_ssl: use_ssl) do |http|
        request = Net::HTTP::Get.new(uri)

        @default_headers.each_pair do |key, value|
          request[key] = value
        end

        print "Visiting: #{url} "
        result = http.request(request)
        puts result.code
      end

      break if result.code == '200'
      puts result.to_hash
      sleep 20
    end

    result.body
  end

  def process_page(body)
    doc = Nokogiri::HTML.parse(body)

    selectors = {
      items: 'table.itemlist tr.athing',
      title: 'td.title a.titlelink',
      more:  'a.morelink'
    }

    items = doc.css(selectors[:items]).to_a
    items.each do |item|
      id       = item.attribute('id').value
      text     = item.css(selectors[:title]).text
      link     = item.css(selectors[:title]).attribute('href')&.value || ''
      comments = "https://news.ycombinator.com/item?id=#{id}"

      if text =~ @term || link =~ @term
        puts ''
        puts "#{@count}: #{text}"
        puts "  Link: #{link}"
        puts "  Comments: #{comments}"
        puts ''

        @count += 1
      end
    end

    more = doc.css(selectors[:more]).attribute('href')&.value
    pp body unless more

    more
  end
end

Finder.new(term).call

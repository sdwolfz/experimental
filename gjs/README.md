# GJS example application

Javascript for both Web and Desktop app with GTK4.

## Local

Install dependencies:

```sh
sudo pacman -S gjs webkit2gtk-5.0
```

Then run with:

```sh
gjs main.js
```

## Package

Fully build and run with:

```sh
make download build clean docker shell/copy package run
```

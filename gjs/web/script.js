document.addEventListener('DOMContentLoaded', function () {
  var result = document.getElementById('result')

  var win = document.getElementById('green')
  win.addEventListener('click', function (event) {
    result.innerText = 'YOU WIN!'
  })

  var lose = document.getElementById('red')
  lose.addEventListener('click', function (event) {
    result.innerText = 'YOU LOSE!'
  })
})

console.log('This is script.js')

#!/usr/bin/env sh

set -ex

. ./scripts/sspm.sh

download_from_github                                               \
  appimagetool                                                     \
  appimagetool-x86_64.AppImage                                     \
  13                                                               \
  df3baf5ca5facbecfc2f3fa6713c29ab9cefa8fd8c1eac5d283b79cab33e4acb \
  https://github.com/AppImage/AppImageKit

#-------------------------------------------------------------------------------
# Settings
#-------------------------------------------------------------------------------

.DEFAULT_GOAL := help

HERE := $(realpath $(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
DOCKER_HERE := docker run --rm -it -u `id -u`:`id -g` -v $(HERE):/work -w /work

REPO_IMAGE := live-coding-javascript
NODE_IMAGE := node:18.11.0-alpine

#-------------------------------------------------------------------------------
# Goals
#-------------------------------------------------------------------------------

.PHONY: help
help:
	@echo 'Usage: make [<GOAL_1>, ...] [<VARIABLE_1>=value_1, ...]'
	@echo ''
	@echo 'Examples:'
	@echo '  make'
	@echo '  make help'
	@echo '  make all'
	@echo ''
	@echo 'Goals:'
	@echo '  - help: Displays this help message.'
	@echo '  - ci:   Runs the entire CI setup using docker.'
	@echo '  - all:  Chains `build`, `npm`, and `shell/fonts`'
	@echo ''
	@echo '  - lint/eclint:   Runs eclint.'
	@echo '  - lint/hadolint: Runs hadolint.'
	@echo '  - lint/yamlliny: Runs yamllint.'
	@echo ''
	@echo 'Default Goal: help'
	@echo ''

.PHONY: npm
npm:
	@$(DOCKER_HERE) $(NODE_IMAGE) npm update

.PHONY: build
build:
	@docker build \
		--build-arg=HOST_USER_UID=`id -u` \
		--build-arg=HOST_USER_GID=`id -g` \
		-t $(REPO_IMAGE) \
		"$(PWD)"

.PHONY: shell
shell:
	@$(DOCKER_HERE) $(REPO_IMAGE) sh

.PHONY: fonts
fonts: clean
	@npm run fonts

.PHONY: clean
clean:
	@rm -rf ./out/

.PHONY: shell/fonts
shell/fonts:
	@$(DOCKER_HERE) $(REPO_IMAGE) make fonts

.PHONY: all
all: build npm shell/fonts

.PHONY: download
download:
	@sh ./scripts/fontawesome.sh

.PHONY: shell/download
shell/download:
	@$(DOCKER_HERE) sdwolfz/csso-cli:latest make download

# # shell/css
#
# Open a shell in the `csso-cli` docker image.
#
# Usage:
#
# ```sh
# make shell/css
# ```
.PHONY: shell/css
shell/css:
	@$(DOCKER_HERE) sdwolfz/sass:latest bash

.PHONY: sass
sass:
	@mkdir -p out/css/
	@sass src/scss/main.scss > out/css/fontawesome-subset.css

#-------------------------------------------------------------------------------
# CI

.PHONY: ci
ci:
	@$(DOCKER_HERE) sdwolfz/eclint:latest make lint/eclint
	@$(DOCKER_HERE) sdwolfz/hadolint:latest make lint/hadolint
	@$(DOCKER_HERE) sdwolfz/yamllint:latest make lint/yamllint

	@$(DOCKER_HERE) $(REPO_IMAGE) make lint/rubocop

.PHONY: lint/eclint
lint/eclint:
	@eclint check $(git ls-files)

.PHONY: lint/hadolint
lint/hadolint:
	@find . -name Dockerfile | xargs hadolint

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .

#!/usr/bin/env sh

set -ex

FA_VERSION=6.1.2
FA_SHA=3f1df9a04745c72a2ea54419c8d84a205d0139e31ea5953b25aeec0cb50725fc
FA_NAME=fontawesome-free-"$FA_VERSION"-web
FA_ZIP="$FA_NAME".zip
FA_URL=https://use.fontawesome.com/releases/v"$FA_VERSION"/"$FA_ZIP"
FA_TMP=./tmp

rm -rf "$FA_TMP"
mkdir -p "$FA_TMP"
wget "$FA_URL" -P "$FA_TMP"
sha256sum "$FA_TMP"/"$FA_ZIP"
echo "$FA_SHA  $FA_TMP/$FA_ZIP" | sha256sum -c -
unzip "$FA_TMP"/"$FA_ZIP" -d "$FA_TMP"

# cp "$FA_TMP"/"$FA_NAME"/webfonts/* static/webfonts
# cp "$FA_TMP"/"$FA_NAME"/css/all.min.css static/css/"$FA_NAME".min.css

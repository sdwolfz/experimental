# Javascript Docker Setup

## Development

Update node with:

```sh
make node
```

Build docker image with:

```sh
make build
```

Start a shell with:

```sh
make shell
```

## CI

Local ci runs with:

```sh
make ci
```

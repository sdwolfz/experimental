const fs = require('fs')
const fontawesomeSubset = require('fontawesome-subset').fontawesomeSubset

// Check `$fa-icons` in `_variables.scss`.
const solids = fs.readFileSync('data/solid').toString().trim().split("\n")

// Check `$fa-brand-icons` in `_variables.scss`.
const brands = fs.readFileSync('data/brands').toString().trim().split("\n")

// -----------------------------------------------------------------------------
// Fonts

const outputDir = 'out/webfonts'
const subset  = { brands: brands, solid: solids }
const options = {
  targetFormats: [
    'woff2',
    'sfnt'
  ]
}

fontawesomeSubset(subset, outputDir, options)

// -----------------------------------------------------------------------------
// CSS

const all = solids.concat(brands)

const template = `
$icons: (
  ${ all.map(icon => `${icon}: $fa-var-${icon},`).join('\n  ').trimEnd().slice(0, -1) }
);

@each $name, $icon in $icons {
  .#{$fa-css-prefix}-#{$name}:before { content: fa-content($icon); }
}
`.trimStart()

fs.writeFileSync('src/scss/_icon_subset.scss', template);


// -----------------------------------------------------------------------------
// HTML

const htmlTemplate = `
<!DOCTYPE html>
<html>
  <head>
    <link rel="shortcut icon" href="#" />

    <link rel="stylesheet" href="../out/css/fontawesome-subset.css">
  </head>
  <body>
    ${ solids.map(icon => `<div><i class="fas fa-${icon}"></i> fa-${icon}</div>`).join('\n    ').trimEnd() }

    <hr/>
    ${ brands.map(icon => `<div><i class="fab fa-${icon}"></i> fa-${icon}</div>`).join('\n    ').trimEnd() }
  </body>
</html>
`.trim()

fs.writeFileSync('out/index.html', htmlTemplate);

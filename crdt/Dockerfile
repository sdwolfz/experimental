#-------------------------------------------------------------------------------
# Base

FROM ruby:3.1.1-alpine

SHELL ["/bin/ash", "-c"]

#-------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail              && \
                                      \
    echo 'Install system packages' && \
    apk add --update --no-cache \
      build-base                \
      shadow

#-------------------------------------------------------------------------------
# Workspace

RUN set -exo pipefail       && \
                               \
    echo 'Create workspace' && \
    mkdir -p /work

WORKDIR /work

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -exo pipefail                                  && \
                                                          \
    echo 'Create the notroot user and group from host' && \
    groupadd -g $HOST_USER_GID notroot                 && \
    useradd -lm -u $HOST_USER_UID -g notroot notroot   && \
                                                          \
    echo 'Set direcotry permissions'                   && \
    chown notroot:notroot /work

#-------------------------------------------------------------------------------
# Gems

COPY --chown=notroot:notroot Gemfile* /work/

USER notroot

RUN set -exo pipefail                 && \
                                         \
    echo 'Install gems'               && \
    cd /work                          && \
    bundle install --jobs 3 --retry 3

#-------------------------------------------------------------------------------
# Command

CMD ["sh"]

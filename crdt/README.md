# Ruby Docker Setup

## Development

Update gems with:

```sh
make gems
```

Build docker image with:

```sh
make build
```

Start a shell with:

```sh
make shell
```

Run guard with:

```sh
make guard
```

Run tests with:

```sh
make test
```

Run rubocop with:

```sh
make rubocop
```

## CI

Local ci runs with:

```sh
make ci
```

# frozen_string_literal: true

require 'pry'

require_relative '../../lib/crdt'

# ------------------------------------------------------------------------------

first  = CRDT::State::Counter.new
second = CRDT::State::Counter.new(first.root)

first.increment
first.increment
first.increment

# second.unlink first

pp 'First'
pp first

pp 'Second'
pp second

# ------------------------------------------------------------------------------

diff = first.diff(second.head)

puts '*' * 80
pp 'First Diff'
pp diff

# second.sync(diff)

# puts '*' * 80
# pp 'First'
# pp first

# pp 'Second'
# pp second

# ------------------------------------------------------------------------------

# second.increment

# diff = second.diff(first.head)

# puts '*' * 80
# pp 'Second Diff'
# pp diff

# puts '*' * 80
# pp 'First'
# pp first

# pp 'Second'
# pp second

# first.sync(diff)

# ------------------------------------------------------------------------------

puts '*' * 80
pp 'Values equal?'
pp "#{first.value} == #{second.value} : #{first.value == second.value}"

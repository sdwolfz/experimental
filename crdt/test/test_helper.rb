# frozen_string_literal: true

require 'pry'

require 'simplecov'
SimpleCov.start do
  enable_coverage :branch
end

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'minitest/autorun'

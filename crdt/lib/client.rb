require 'faye/websocket'
require 'eventmachine'

require 'json'
require_relative 'crdt'
require_relative 'handler'

EM.run do
  ws = Faye::WebSocket::Client.new('ws://0.0.0.0:3000/')

  ws.on :open do |_event|
    pp [:open]

    state    = initialize_state
    response = handle_client_open(state)
    pp '-' * 78
    pp 'Initial message:'
    pp response

    ws.send(JSON.dump(response))
  end

  ws.on :message do |event|
    message = JSON.parse(event.data)
    pp [:message, message]

    sleep 5

    state    = fetch_or_initialize_state(message['id'])
    response = handle_message(state, message)
    pp '-' * 78
    pp 'New response:'
    pp response

    ws.send(JSON.dump(response))
  end

  ws.on :close do |event|
    pp [:close, event.code, event.reason]

    ws = nil
  end
end

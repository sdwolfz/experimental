# frozen_string_literal: true

require 'pry'

require 'faye/websocket'
require 'permessage_deflate'
require 'rack'

require 'json'
require_relative 'crdt'
require_relative 'handler'

App = lambda do |env|
  options = { extensions: [PermessageDeflate], ping: 5 }
  ws      = Faye::WebSocket.new(env, [], options)

  pp [:open, ws.url, ws.version, ws.protocol]

  ws.onmessage = lambda do |event|
    message = JSON.parse(event.data)
    pp [:message, message]

    root     = message['cursor']
    state    = fetch_or_initialize_state(message['id'], root)
    response = handle_message(state, message)
    pp '-' * 78
    pp 'New response:'
    pp response

    ws.send(JSON.dump(response))
  end

  ws.onclose = lambda do |event|
    pp [:close, event.code, event.reason]

    ws = nil
  end

  ws.rack_response
end

Faye::WebSocket.load_adapter('thin')

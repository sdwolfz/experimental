# frozen_string_literal: true
require 'securerandom'

module CRDT
  module State
    # Counter
    class Counter
      attr_reader :id, :root, :head

      def initialize(root = nil)
        @id    = SecureRandom.uuid.freeze
        @root  = root || SecureRandom.uuid.freeze
        @head  = @root
        @links = {}
        @nodes = {
          @root => { 'id' => nil, 'value' => 0, 'next' => nil }
        }
      end

      # def unlink(id)
      #   @links.delete(id) unless @links.key?(id)

      #   self
      # end

      def increment
        node = SecureRandom.uuid.freeze

        @nodes[node] = { 'id' => @id, 'value' => 1, 'next' => @head }
        @head        = node

        self
      end

      def diff(node)
        return false unless @nodes.key?(node)

        result  = []
        current = @head

        while current
          break if current == node

          result << current

          other = @nodes[current]
          break unless other

          current = other['next']
        end

        result << node

        { id: @id, nodes: result }
      end

      # def sync(diff)
      #   return false unless @nodes.key?(diff[:nodes].last)

      #   diff[:nodes].each_cons(2) do |first, second|
      #     @nodes[first] = {} unless @nodes.key?(first)

      #     @nodes[first][diff[:id]] = second
      #   end

      #   @followers[diff[:id]] = diff[:nodes].first

      #   true
      # end

      def value
        total = 0

        @nodes.each_value do |item|
          pp item
          total += item['value']
        end

        total
      end
    end
  end
end

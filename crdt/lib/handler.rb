# frozen_string_literal: true

STORE = {}

def initialize_state(root = nil)
  state = CRDT::State::Counter.new(root)
  STORE[state.id] = state

  state
end

# ------------------------------------------------------------------------------

def handle_client_open(state)
  pp 'Initial state:'
  pp state
  pp "Value #{state.value}"

  {
    cursor: state.root,
    action: 'increment',
    type: 'counter',
    id: state.id
  }
end

def handle_message(state, message)
  id = message['id']
  state.follower(id)

  cursor = message['cursor']
  cursor = state.increment(cursor) if message['action'] == 'increment'
  pp state
  pp "Value #{state.value}"

  { cursor: cursor, action: 'increment', type: 'counter', id: state.id }
end

# ------------------------------------------------------------------------------

def fetch_or_initialize_state(id, root = nil)
  return STORE[id] if STORE.key?(id)

  state     = initialize_state(root)
  STORE[id] = state

  state
end

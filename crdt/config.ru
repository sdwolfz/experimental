# frozen_string_literal: true

require 'bundler/setup'
require File.expand_path('lib/server', __dir__)

run App

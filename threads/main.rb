# frozen_string_literal: true

def main(index)
  work    = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  threads = []

  result = []

  condition = ConditionVariable.new

  condition_lock = Mutex.new
  scheduler_lock = Mutex.new

  puts "Running: #{index}" if index % 1000 == 0

  scheduler = Thread.new do
    condition_lock.synchronize do
      loop do
        stop = false

        scheduler_lock.synchronize do
          work.each do |number|
            thread = Thread.new do
              scheduler_lock.synchronize do
                result << number
                work.push(number * 10) if number < 100000

                threads.delete(Thread.current)
              end

              condition_lock.synchronize do
                condition.signal
              end
            end

            threads << thread
          end

          work.clear
          stop = true if threads.empty?
        end

        break if stop

        condition.wait(condition_lock)
      end
    end
  end

  scheduler.join

  expected = [1, 2, 3, 5, 4, 6, 7, 8, 9,
              20, 200, 2000, 20000, 200000,
              10, 100, 1000, 30, 50, 500,
              5000, 50000, 500000, 60, 600,
              6000, 60000, 600000, 40, 400,
              4000, 40000, 400000, 80, 70,
              10000, 300, 800, 700, 90, 100000,
              3000, 8000, 7000, 900, 9000,
              30000, 80000, 70000, 700000,
              90000, 800000, 300000, 900000]

  raise 'Oopsie!' unless result.sort == expected.sort
end

1_000_000.times { |index| main(index) }

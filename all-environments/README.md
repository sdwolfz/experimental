# All Environments

Deploy an app on all available environments:

- [x] Web (Online)
- [ ] Progressive Web App (Offline)
- [ ] Desktop (Electron)
- [ ] Mobile (React Native)

## Web

A web application.

URL: https://all-environments.herokuapp.com/

```sh
heroku login
heroku container:login
heroku create all-environments
heroku container:push web --recursive -a all-environments
heroku container:release web -a all-environments
```

## Progressive Web App (WIP)

Build and run the docker image:
```sh
docker build . -t all-environments-dev
docker run --rm -it -v "$PWD":/work all-environments-dev sh
```

Install packages:
```sh
npm install
```

Spin up the Docker Compose setup:
```sh
docker-compose up -d
```

Run Lighthouse:
```
docker-compose run lighthouse npx lighthouse-ci --report=./reports http://web:3000
```

## Desktop (TODO)
## Mobile (TODO)
